package aisoai.screens.guidegamescreen;


import android.widget.ImageView;
import android.widget.TextView;

import aisoai.TITApplication;
import aisoai.screens.choicegamescreen.ChoiceGameUIDefine;
import aisoai.config.UIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;
import aisoai.R;
import aisoai.screens.titentities.control.TITNormalControl;

public class GuideGameActivity extends TITNormalActivity
{
    private ImageView ivPlayer1Avatar;
    private ImageView ivPlayer2Avatar;
    private ImageView ivPlayer3Avatar;
    private ImageView ivPlayer4Avatar;

    private TextView tvPlayer1Point;
    private TextView tvPlayer2Point;
    private TextView tvPlayer3Point;
    private TextView tvPlayer4Point;

    private TextView tvTime;

    private ImageView ivGuideGame;
    private TextView tvGuideGame;

    @Override
    protected void linkToLayout()
    {
        setContentView(R.layout.guide_game);
        ivPlayer1Avatar=(ImageView) findViewById(R.id.ivPlayer1Avatar);
        ivPlayer2Avatar=(ImageView) findViewById(R.id.ivPlayer2Avatar);
        ivPlayer3Avatar=(ImageView) findViewById(R.id.ivPlayer3Avatar);
        ivPlayer4Avatar=(ImageView) findViewById(R.id.ivPlayer4Avatar);

        tvPlayer1Point=(TextView) findViewById(R.id.tvPlayer1Point);
        tvPlayer2Point=(TextView) findViewById(R.id.tvPlayer2Point);
        tvPlayer3Point=(TextView) findViewById(R.id.tvPlayer3Point);
        tvPlayer4Point=(TextView) findViewById(R.id.tvPlayer4Point);

        tvTime=(TextView) findViewById(R.id.tvTime);

        ivGuideGame=(ImageView) findViewById(R.id.ivGuideGame);
        tvGuideGame=(TextView) findViewById(R.id.tvGuideGame);
    }

    @Override
    protected void scaleView()
    {
        ivPlayer1Avatar.requestLayout();
        ivPlayer1Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer1Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivPlayer2Avatar.requestLayout();
        ivPlayer2Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer2Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivPlayer3Avatar.requestLayout();
        ivPlayer3Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer3Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivPlayer4Avatar.requestLayout();
        ivPlayer4Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer4Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivGuideGame.requestLayout();
        ivGuideGame.getLayoutParams().width= GuideGameUIDefine.GUIDEGAMEIV_WIDTH();
        ivGuideGame.getLayoutParams().height= GuideGameUIDefine.GUIDEGAMEIV_HEIGHT();

        tvGuideGame.requestLayout();
        tvGuideGame.getLayoutParams().width= GuideGameUIDefine.GUIDEGAMETV_WIDTH();
        tvGuideGame.getLayoutParams().height= GuideGameUIDefine.GUIDEGAMETV_HEIGHT();

        tvPlayer1Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayer2Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayer3Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayer4Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvTime.setTextSize(UIDefine.GIGATICTEXT_SIZE());
        tvGuideGame.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
    }

    public ImageView getIvPlayer1Avatar()
    {
        return ivPlayer1Avatar;
    }

    public ImageView getIvPlayer3Avatar()
    {
        return ivPlayer3Avatar;
    }

    public ImageView getIvPlayer2Avatar()
    {
        return ivPlayer2Avatar;
    }

    public ImageView getIvPlayer4Avatar()
    {
        return ivPlayer4Avatar;
    }

    public TextView getTvPlayer1Point()
    {
        return tvPlayer1Point;
    }

    public TextView getTvPlayer2Point()
    {
        return tvPlayer2Point;
    }

    public TextView getTvPlayer3Point()
    {
        return tvPlayer3Point;
    }

    public TextView getTvPlayer4Point()
    {
        return tvPlayer4Point;
    }

    public TextView getTvTime()
    {
        return tvTime;
    }

    public ImageView getIvGuideGame()
    {
        return ivGuideGame;
    }

    public TextView getTvGuideGame()
    {
        return tvGuideGame;
    }

    @Override
    public GuideGameControl getControl()
    {
        return (GuideGameControl) control;
    }

    @Override
    protected TITNormalControl linkControl()
    {
        return TITApplication.getScreenControlManager().getGuideGameControl();
    }
}
