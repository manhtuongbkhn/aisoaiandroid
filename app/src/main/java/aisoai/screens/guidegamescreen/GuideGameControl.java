package aisoai.screens.guidegamescreen;

import android.graphics.Bitmap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import aisoai.TITApplication;
import aisoai.config.KJS;
import aisoai.loadnetimage.TITRequestImage;
import aisoai.screens.titentities.model.TITModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.titapplib.TITTransferData;
import aisoai.screens.titentities.control.TITNormalControl;
import aisoai.screens.gamescreeens.TITGameRouter;

public class GuideGameControl extends TITNormalControl
{

    @Override
    public void init()
    {
    }

    @Override
    public void finish()
    {

    }

    /////////////////////////////////////////////////////////////////////////////////////////

    public void gameGuideNotify(JsonObject fromServerData)
    {
        int gameId=fromServerData.get(KJS.GAME_ID).getAsInt();
        String gameGuide=fromServerData.get(KJS.GAME_GUIDE).getAsString();
        getActivity().getIvGuideGame().setImageResource(TITGameRouter.routerGameIcon(gameId));
        getActivity().getTvGuideGame().setText(gameGuide);
    }

    public void guideGameUserInfoNotify(JsonObject fromServerData)
    {
        JsonArray userInfoJsonArr=fromServerData.get(KJS.ARRAY).getAsJsonArray();
        for(int i=0;i<userInfoJsonArr.size();i++)
        {
            JsonObject userInfo=userInfoJsonArr.get(i).getAsJsonObject();
            String avatarUrl=userInfo.get(KJS.AVATAR_URL).getAsString();
            Integer totalPoint=userInfo.get(KJS.POINT).getAsInt();
            switch (i)
            {
                case 0:
                    getActivity().getTvPlayer1Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer1Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
                case 1:
                    getActivity().getTvPlayer2Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer2Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
                case 2:
                    getActivity().getTvPlayer3Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer3Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
                case 3:
                    getActivity().getTvPlayer4Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer4Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
            }
        }
    }

    public void guideGameTotalPointNotify(JsonObject fromServerData)
    {
        System.out.println(fromServerData);
        JsonArray jsonArray=fromServerData.get(KJS.POINT_ARR).getAsJsonArray();
        for(int i=0;i<jsonArray.size();i++)
        {
            Integer totalPoint=jsonArray.get(i).getAsInt();
            switch (i)
            {
                case 0:
                    getActivity().getTvPlayer1Point().setText(totalPoint.toString());
                    break;
                case 1:
                    getActivity().getTvPlayer2Point().setText(totalPoint.toString());
                    break;
                case 2:
                    getActivity().getTvPlayer3Point().setText(totalPoint.toString());
                    break;
                case 3:
                    getActivity().getTvPlayer4Point().setText(totalPoint.toString());
                    break;
            }
        }
    }

    public void gameScriptNotify(JsonObject fromServerData)
    {
        JsonArray gameScript=fromServerData.get(KJS.ARRAY).getAsJsonArray();
        TITTransferData.addObject(gameScript);
    }

    public void guideGameTimeDownNotify(JsonObject fromServerData)
    {
        Integer time=fromServerData.get(KJS.DOWN_TIME).getAsInt();
        getActivity().getTvTime().setText(time.toString());
    }

    public void startPlayingGameNotify(JsonObject fromServerData)
    {
        //getRequestFactory().stopKeepConnectionThread();
        int gameId=fromServerData.get(KJS.GAME_ID).getAsInt();
        TITApplication.getScreenControlManager().
                                        changeScreen(TITGameRouter.routerGameControl(gameId));
    }

    @Override
    public Class<GuideGameActivity> initActivity()
    {
        return GuideGameActivity.class;
    }

    @Override
    protected TITModel initModel()
    {
        return new TITModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new GuideGameRF();
    }

    @Override
    public GuideGameActivity getActivity()
    {
        return (GuideGameActivity) activity;
    }

    @Override
    public TITModel getModel()
    {
        return model;
    }

    @Override
    public GuideGameRF getRequestFactory()
    {
        return (GuideGameRF) requestFactory;
    }
}
