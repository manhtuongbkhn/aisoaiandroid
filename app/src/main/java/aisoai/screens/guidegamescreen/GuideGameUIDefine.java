package aisoai.screens.guidegamescreen;

import aisoai.config.ClientConfig;

public class GuideGameUIDefine
{
    public static int GUIDEGAMEIV_WIDTH()
    {
        Float f=150f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int GUIDEGAMEIV_HEIGHT()
    {
        return GUIDEGAMEIV_WIDTH();
    }

    public static int GUIDEGAMETV_WIDTH()
    {
        Float f=280f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int GUIDEGAMETV_HEIGHT()
    {
        Float f=80f*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }
}
