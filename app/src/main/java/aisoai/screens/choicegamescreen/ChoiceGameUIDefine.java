package aisoai.screens.choicegamescreen;

import aisoai.config.ClientConfig;

public class ChoiceGameUIDefine
{
    public static int PLAYERAVATARIV_WIDTH()
    {
        Float f=50f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int PLAYERAVATARIV_HEIGHT()
    {
        return PLAYERAVATARIV_WIDTH();
    }

    public static int CHOICEPLAYERAVATARIV_WIDTH()
    {
        Float f=50f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int CHOICEPLAYERAVATARIV_HEIGHT()
    {
        return CHOICEPLAYERAVATARIV_WIDTH();
    }

    public static int SEARCHGAMEET_WIDTH()
    {
        Float f=200f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int SEARCHGAMEET_HEIGHT()
    {
        Float f=30f*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int WASCHOICEDPLAYERAVATARIV_WIDTH()
    {
        return CHOICEPLAYERAVATARIV_WIDTH();
    }

    public static int WASCHOICEDPLAYERAVATARIV_HEIGHT()
    {
        return WASCHOICEDPLAYERAVATARIV_WIDTH();
    }

    //ITEM GAME
    public static int ITEM_WIDTH()
    {
        Float f=106f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int ITEM_HEIGHT()
    {
        Float f=120f*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int GAMEICONIV_WIDTH()
    {
        Float f=80f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int GAMEICON_HEIGHT()
    {
        return GAMEICONIV_WIDTH();
    }
}
