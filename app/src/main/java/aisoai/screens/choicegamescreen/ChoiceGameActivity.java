package aisoai.screens.choicegamescreen;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import aisoai.TITApplication;
import aisoai.config.UIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;
import aisoai.screens.titentities.control.TITNormalControl;
import aisoai.R;

public class ChoiceGameActivity extends TITNormalActivity
{
    private ImageView ivPlayer1Avatar;
    private ImageView ivPlayer2Avatar;
    private ImageView ivPlayer3Avatar;
    private ImageView ivPlayer4Avatar;

    private TextView tvPlayer1Point;
    private TextView tvPlayer2Point;
    private TextView tvPlayer3Point;
    private TextView tvPlayer4Point;

    private TextView tvTime;

    private ImageView ivChoicePlayerAvatar;
    private TextView tvRound;
    private TextView tvNotification;
    private ImageView ivWasChoicedPlayerAvatar;

    private GridView gvGame;
    private EditText etSearchGame;
    @Override
    protected void linkToLayout()
    {
        setContentView(R.layout.choice_game);
        ivPlayer1Avatar=(ImageView) findViewById(R.id.ivPlayer1Avatar);
        ivPlayer2Avatar=(ImageView) findViewById(R.id.ivPlayer2Avatar);
        ivPlayer3Avatar=(ImageView) findViewById(R.id.ivPlayer3Avatar);
        ivPlayer4Avatar=(ImageView) findViewById(R.id.ivPlayer4Avatar);

        tvPlayer1Point=(TextView) findViewById(R.id.tvPlayer1Point);
        tvPlayer2Point=(TextView) findViewById(R.id.tvPlayer2Point);
        tvPlayer3Point=(TextView) findViewById(R.id.tvPlayer3Point);
        tvPlayer4Point=(TextView) findViewById(R.id.tvPlayer4Point);

        tvTime=(TextView) findViewById(R.id.tvTime);

        ivChoicePlayerAvatar=(ImageView) findViewById(R.id.ivChoicePlayerAvatar);
        tvRound=(TextView) findViewById(R.id.tvRound);
        tvNotification=(TextView) findViewById(R.id.tvNotification);
        ivWasChoicedPlayerAvatar=(ImageView) findViewById(R.id.ivWasChoicedPlayerAvatar);

        gvGame=(GridView)findViewById(R.id.gvGame);
        etSearchGame=(EditText) findViewById(R.id.etSearchGame);

        gvGame.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                getControl().choiceGameEvent(position);
            }
        });
    }

    @Override
    protected void scaleView()
    {
        ivPlayer1Avatar.requestLayout();
        ivPlayer1Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer1Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivPlayer2Avatar.requestLayout();
        ivPlayer2Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer2Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivPlayer3Avatar.requestLayout();
        ivPlayer3Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer3Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivPlayer4Avatar.requestLayout();
        ivPlayer4Avatar.getLayoutParams().width= ChoiceGameUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayer4Avatar.getLayoutParams().height= ChoiceGameUIDefine.PLAYERAVATARIV_HEIGHT();

        ivChoicePlayerAvatar.requestLayout();
        ivChoicePlayerAvatar.getLayoutParams().width=
                                                    ChoiceGameUIDefine.CHOICEPLAYERAVATARIV_WIDTH();
        ivChoicePlayerAvatar.getLayoutParams().height=
                                                    ChoiceGameUIDefine.CHOICEPLAYERAVATARIV_HEIGHT();

        ivWasChoicedPlayerAvatar.requestLayout();
        ivWasChoicedPlayerAvatar.getLayoutParams().width=
                                                ChoiceGameUIDefine.WASCHOICEDPLAYERAVATARIV_WIDTH();
        ivWasChoicedPlayerAvatar.getLayoutParams().height=
                                                ChoiceGameUIDefine.WASCHOICEDPLAYERAVATARIV_HEIGHT();

        etSearchGame.requestLayout();
        etSearchGame.getLayoutParams().width= ChoiceGameUIDefine.SEARCHGAMEET_WIDTH();
        etSearchGame.getLayoutParams().height= ChoiceGameUIDefine.SEARCHGAMEET_HEIGHT();

        tvPlayer1Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayer2Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayer3Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayer4Point.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvRound.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvNotification.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvTime.setTextSize(UIDefine.GIGATICTEXT_SIZE());
        etSearchGame.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
    }

    public ImageView getIvPlayer1Avatar()
    {
        return ivPlayer1Avatar;
    }

    public ImageView getIvPlayer2Avatar()
    {
        return ivPlayer2Avatar;
    }

    public ImageView getIvPlayer3Avatar()
    {
        return ivPlayer3Avatar;
    }

    public ImageView getIvPlayer4Avatar()
    {
        return ivPlayer4Avatar;
    }

    public TextView getTvPlayer1Point()
    {
        return tvPlayer1Point;
    }

    public TextView getTvPlayer2Point()
    {
        return tvPlayer2Point;
    }

    public TextView getTvPlayer3Point() {
        return tvPlayer3Point;
    }

    public TextView getTvPlayer4Point() {
        return tvPlayer4Point;
    }

    public TextView getTvTime() {
        return tvTime;
    }

    public ImageView getIvChoicePlayerAvatar() {
        return ivChoicePlayerAvatar;
    }

    public TextView getTvRound() {
        return tvRound;
    }

    public TextView getTvNotification() {
        return tvNotification;
    }

    public ImageView getIvWasChoicedPlayerAvatar()
    {
        return ivWasChoicedPlayerAvatar;
    }

    public GridView getGvGame() {
        return gvGame;
    }

    public void setTvTime(String time)
    {
        tvTime.setText(time);
    }

    public void setTextTvPlayer1Point(Integer point)
    {
        tvPlayer1Point.setText(point.toString());
    }

    public void setTextTvPlayer2Point(Integer point)
    {
        tvPlayer2Point.setText(point.toString());
    }

    public void setTextTvPlayer3Point(Integer point)
    {
        tvPlayer3Point.setText(point.toString());
    }

    public void setTextTvPlayer4Point(Integer point)
    {
        tvPlayer4Point.setText(point.toString());
    }

    public void setTimeTvTime(String time)
    {
        tvTime.setText(time);
    }

    public void setSourceIvChoicePlayerAvatar(int drawable)
    {
        ivChoicePlayerAvatar.setImageResource(drawable);
    }

    public void setTvRound(Integer round)
    {
        tvRound.setText("Vòng " + round.toString());
    }

    public void setTvNotice(String notice)
    {
        tvNotification.setText(notice);
    }

    public void setGvGame(ArrayList<JsonObject> gameArr)
    {
        GameArrayAdapter gameArrayAdapter=new GameArrayAdapter(this,gameArr);
        gvGame.setAdapter(gameArrayAdapter);
    }

    @Override
    public ChoiceGameControl getControl()
    {
        return (ChoiceGameControl) control;
    }

    @Override
    protected TITNormalControl linkControl()
    {
        return TITApplication.getScreenControlManager().getChoiceGameControl();
    }
}
