package aisoai.screens.choicegamescreen;

import android.graphics.Bitmap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import aisoai.TITApplication;
import aisoai.config.KJS;
import aisoai.config.StrDefine;
import aisoai.screens.guidegamescreen.GuideGameControl;
import aisoai.loadnetimage.TITRequestImage;
import aisoai.R;
import aisoai.screens.titentities.control.TITNormalControl;
import aisoai.screens.titentities.model.TITModel;
import aisoai.screens.titentities.TITRequestFactory;

public class ChoiceGameControl extends TITNormalControl
{

    @Override
    public void init()
    {
        GameArrayAdapter arrayAdapter=new GameArrayAdapter(getActivity(),getModel().getGameInfoArr());
        getActivity().getGvGame().setAdapter(arrayAdapter);
    }

    @Override
    public void finish()
    {

    }

    public void choiceGameEvent(int position)
    {
        JsonObject gameInfo=getModel().getGameInfoArr().get(position);
        int gameId=gameInfo.get(KJS.GAME_ID).getAsInt();
        String gameName=gameInfo.get(KJS.GAME_NAME).getAsString();
        getRequestFactory().sendChoiceGameRequest(gameId, gameName);
    }

    ////////////////////////////////////////////////////////////////////////////////////

    public void choiceGameNotify(JsonObject fromServerData)
    {
        JsonObject choiceGameUserJson=fromServerData.get(KJS.USER1).getAsJsonObject();

        int choiceGameUserSystemUserId=choiceGameUserJson.get(KJS.SYSTEM_USER_ID).getAsInt();
        switch (choiceGameUserSystemUserId)
        {
            case -1:
                getActivity().getIvChoicePlayerAvatar().setImageResource(R.drawable.logo);
                break;
            case -2:
                getActivity().getIvChoicePlayerAvatar().setImageResource(R.drawable.group);
                break;
            default:
                String choiceGameUserAvatarUrl=choiceGameUserJson.get(KJS.AVATAR_URL).getAsString();
                new TITRequestImage(choiceGameUserAvatarUrl,6)
                {
                    @Override
                    protected void setImage(Bitmap bitmap)
                    {
                        getActivity().getIvChoicePlayerAvatar().setImageBitmap(bitmap);
                    }
                };
                break;
        }

        JsonObject wasChoicedGameUserJson=fromServerData.get(KJS.USER2).getAsJsonObject();
        int wasChoicedGameUserSystemUserId=wasChoicedGameUserJson.get(KJS.SYSTEM_USER_ID).getAsInt();

        switch (wasChoicedGameUserSystemUserId)
        {
            case -1:
                getActivity().getIvWasChoicedPlayerAvatar().setImageResource(R.drawable.logo);
                break;
            case -2:
                getActivity().getIvWasChoicedPlayerAvatar().setImageResource(R.drawable.group);
                break;
            default:
                String wasChoicedGameUserAvatarUrl=wasChoicedGameUserJson.
                                                                get(KJS.AVATAR_URL).getAsString();
                new TITRequestImage(wasChoicedGameUserAvatarUrl,6)
                {
                    @Override
                    protected void setImage(Bitmap bitmap)
                    {
                        getActivity().getIvWasChoicedPlayerAvatar().setImageBitmap(bitmap);
                    }
                };
                break;
        }

        String notification=fromServerData.get(KJS.NOTIFICATION).getAsString();
        Integer round=fromServerData.get(KJS.ROOM_ROUND).getAsInt();
        getActivity().getTvRound().setText("Round "+round.toString());
        getActivity().getTvNotification().setText(notification);
        JsonArray gameInfoJsonArr=fromServerData.get(KJS.ARRAY).getAsJsonArray();

        for(int i=0;i<gameInfoJsonArr.size();i++)
        {
            JsonObject gameInfo=gameInfoJsonArr.get(i).getAsJsonObject();
            getModel().getGameInfoArr().add(gameInfo);
        }
        getActivity().getGvGame().invalidateViews();
    }

    public void choiceGameUserInfoNotify(JsonObject fromServerData)
    {
        JsonArray userInfoJsonArr=fromServerData.get(KJS.ARRAY).getAsJsonArray();
        for(int i=0;i<userInfoJsonArr.size();i++)
        {
            JsonObject userInfo=userInfoJsonArr.get(i).getAsJsonObject();
            String avatarUrl=userInfo.get(KJS.AVATAR_URL).getAsString();
            Integer totalPoint=userInfo.get(KJS.POINT).getAsInt();
            switch (i)
            {
                case 0:
                    getActivity().getTvPlayer1Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer1Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
                case 1:
                    getActivity().getTvPlayer2Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer2Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
                case 2:
                    getActivity().getTvPlayer3Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer3Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
                case 3:
                    getActivity().getTvPlayer4Point().setText(totalPoint.toString());
                    new TITRequestImage(avatarUrl,6)
                    {
                        @Override
                        protected void setImage(Bitmap bitmap)
                        {
                            getActivity().getIvPlayer4Avatar().setImageBitmap(bitmap);
                        }
                    };
                    break;
            }
        }
    }

    public void choiceGameDownTimeNotify(JsonObject fromServerData)
    {
        Integer time=fromServerData.get(KJS.DOWN_TIME).getAsInt();
        getActivity().getTvTime().setText(time.toString());
    }

    public void choiceGameResponse(JsonObject fromServerData)
    {
        boolean sucess=fromServerData.get(KJS.SUCESS).getAsBoolean();

        if(sucess)
            getActivity().showMessage(StrDefine.CHOICE_GAME_SUCESS_SHOW,1);
        else
        {
            String systemMessage=fromServerData.get(KJS.SYSTEM_MESSAGE).getAsString();
            if(systemMessage.equals(StrDefine.NOT_PERMISSION_CHOICE_GAME))
                getActivity().showMessage(StrDefine.NOT_PERMISSION_CHOICE_GAME_SHOW, 1);
            if(systemMessage.equals(StrDefine.GAME_NOT_INVALID))
                getActivity().showMessage(StrDefine.GAME_NOT_INVALID_SHOW,1);
            if(systemMessage.equals(StrDefine.CHOICED_GAME))
                getActivity().showMessage(StrDefine.CHOICED_GAME_SHOW,1);
        }
    }

    public void choicedGameNotify(JsonObject fromServerData)
    {
        Integer gameId=fromServerData.get(KJS.GAME_ID).getAsInt();
        String gameName=fromServerData.get(KJS.GAME_NAME).getAsString();
        String notification=fromServerData.get(KJS.NOTIFICATION).getAsString();
        getActivity().getTvNotification().setText(notification);
    }

    public void startGuideGameNotify(JsonObject fromServerData)
    {
        System.out.println(fromServerData.toString());
        TITApplication.getScreenControlManager().changeScreen(new GuideGameControl());
    }

    @Override
    public Class<ChoiceGameActivity> initActivity()
    {
        return ChoiceGameActivity.class;
    }

    @Override
    protected TITModel initModel()
    {
        return new ChoiceGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new ChoiceGameRF();
    }

    @Override
    public ChoiceGameActivity getActivity()
    {
        return (ChoiceGameActivity) activity;
    }

    @Override
    public ChoiceGameModel getModel()
    {
        return (ChoiceGameModel)model;
    }

    @Override
    public ChoiceGameRF getRequestFactory()
    {
        return (ChoiceGameRF) requestFactory;
    }
}
