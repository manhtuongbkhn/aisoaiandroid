package aisoai.screens.choicegamescreen;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.TITGameRouter;
import aisoai.screens.titentities.activity.TITNormalActivity;
import aisoai.R;

public class GameArrayAdapter extends ArrayAdapter<JsonObject>
{
    private TITNormalActivity titActivity;
    private ArrayList<JsonObject> gameArr;

    private View item;
    private ImageView ivGameIcon;
    private TextView tvGameName;
    private TextView tvGameType;

    public GameArrayAdapter(TITNormalActivity titActivity,ArrayList<JsonObject> gameArr)
    {
        super(titActivity, R.layout.game_item,gameArr);
        this.titActivity=titActivity;
        this.gameArr=gameArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        item=inflater.inflate(R.layout.game_item,parent,false);
        linkToLayout();
        scaleView();
        setContentView(position);
        return item;
    }

    public void linkToLayout()
    {
        ivGameIcon=(ImageView) item.findViewById(R.id.ivGameIcon);
        tvGameName=(TextView) item.findViewById(R.id.tvGameName);
        tvGameType=(TextView) item.findViewById(R.id.tvGameType);
    }

    public void scaleView()
    {
        item.requestLayout();
        item.getLayoutParams().width= ChoiceGameUIDefine.ITEM_WIDTH();
        item.getLayoutParams().height= ChoiceGameUIDefine.ITEM_HEIGHT();

        ivGameIcon.requestLayout();
        ivGameIcon.getLayoutParams().width= ChoiceGameUIDefine.GAMEICONIV_WIDTH();
        ivGameIcon.getLayoutParams().height= ChoiceGameUIDefine.GAMEICON_HEIGHT();
    }

    public void setContentView(int postion)
    {
        JsonObject game=gameArr.get(postion);
        String gameName=game.get(KJS.GAME_NAME).getAsString();
        String gameType=game.get(KJS.GAME_MAIN_TYPE).getAsString();
        int gameId=game.get(KJS.GAME_ID).getAsInt();
        tvGameName.setText(gameName);
        tvGameType.setText(gameType);
        ivGameIcon.setImageResource(TITGameRouter.routerGameIcon(gameId));
    }
}
