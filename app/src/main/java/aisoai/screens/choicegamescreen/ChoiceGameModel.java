package aisoai.screens.choicegamescreen;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import aisoai.screens.titentities.model.TITModel;

public class ChoiceGameModel extends TITModel
{
    private ArrayList<JsonObject> gameInfoArr=new ArrayList<JsonObject>();

    public ArrayList<JsonObject> getGameInfoArr()
    {
        return gameInfoArr;
    }

    public void setGameInfoArr(ArrayList<JsonObject> gameInfoArr)
    {
        this.gameInfoArr = gameInfoArr;
    }
}
