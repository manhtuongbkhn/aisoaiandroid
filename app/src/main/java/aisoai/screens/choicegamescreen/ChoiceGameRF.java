package aisoai.screens.choicegamescreen;

import com.smartfoxserver.v2.entities.data.SFSObject;

import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;
import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;

public class ChoiceGameRF extends TITRequestFactory
{
    public void sendChoiceGameRequest(int gameId,String gameName)
    {
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.GAME_ID,gameId);
        toServerData.putUtfString(KJS.GAME_NAME,gameName);
        ExtensionRequest request=new ExtensionRequest(CMDRQ.CHOICEGAME_RQ,toServerData,room);
        sendRequest(request);
    }
}
