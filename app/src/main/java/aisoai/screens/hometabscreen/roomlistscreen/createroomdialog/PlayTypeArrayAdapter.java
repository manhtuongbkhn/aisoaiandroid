package aisoai.screens.hometabscreen.roomlistscreen.createroomdialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import aisoai.R;
import aisoai.config.UIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;

public class PlayTypeArrayAdapter extends ArrayAdapter<String>
{
    private TITNormalActivity titActivity;
    private String[] strArr;

    private TextView tvPlayType;

    public PlayTypeArrayAdapter(TITNormalActivity titActivity, String[] strArr)
    {
        super(titActivity,R.layout.playtype_spiner_item,strArr);
        this.titActivity=titActivity;
        this.strArr=strArr;
    }

    @Override
    public View getView(int position, View convertView,ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        View row=inflater.inflate(R.layout.playtype_spiner_item,parent,false);
        tvPlayType=(TextView) row.findViewById(R.id.tvSPPlayType);

        row.requestLayout();
        row.getLayoutParams().width= CreateRoomUIDefine.ROOMTYPEITEM_WIDTH();
        row.getLayoutParams().height= CreateRoomUIDefine.ROOMTYPEITEM_HEIGHT();

        tvPlayType.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayType.setText(strArr[position]);

        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        View row=inflater.inflate(R.layout.playtype_spiner_item,parent,false);
        tvPlayType=(TextView) row.findViewById(R.id.tvSPPlayType);

        row.requestLayout();
        row.getLayoutParams().width= CreateRoomUIDefine.ROOMTYPEITEM_WIDTH();
        row.getLayoutParams().height= CreateRoomUIDefine.ROOMTYPEITEM_HEIGHT();

        tvPlayType.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvPlayType.setText(strArr[position]);

        return row;
    }
}
