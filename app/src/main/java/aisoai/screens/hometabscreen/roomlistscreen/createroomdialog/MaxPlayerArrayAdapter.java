package aisoai.screens.hometabscreen.roomlistscreen.createroomdialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import aisoai.R;
import aisoai.config.UIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;

public class MaxPlayerArrayAdapter extends ArrayAdapter<Integer>
{
    private TITNormalActivity titActivity;
    private Integer[] integerArr;

    private TextView tvMaxPlayer;
    private ImageView ivPlayerIcon;

    public MaxPlayerArrayAdapter(TITNormalActivity titActivity,Integer[]integerArr)
    {
        super(titActivity,R.layout.maxplayer_spiner_item,integerArr);
        this.titActivity=titActivity;
        this.integerArr=integerArr;
    }

    @Override
    public View getView(int position, View convertView,ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        View row=inflater.inflate(R.layout.maxplayer_spiner_item,parent,false);
        tvMaxPlayer=(TextView) row.findViewById(R.id.tvSPMaxPlayer);
        ivPlayerIcon=(ImageView) row.findViewById(R.id.ivSPMaxPlayer);

        row.requestLayout();
        row.getLayoutParams().width= CreateRoomUIDefine.MAXPLAYERITEM_WIDTH();
        row.getLayoutParams().height= CreateRoomUIDefine.MAXPLAYERITEM_HEIGHT();

        ivPlayerIcon.requestLayout();
        ivPlayerIcon.getLayoutParams().width= CreateRoomUIDefine.PLAYERICONIV_WIDTH();
        ivPlayerIcon.getLayoutParams().height= CreateRoomUIDefine.PLAYERICONIV_HEIGHT();
        tvMaxPlayer.setTextSize(UIDefine.MEDIUMTEXT_SIZE());

        tvMaxPlayer.setText(integerArr[position].toString());

        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        View row=inflater.inflate(R.layout.maxplayer_spiner_item,parent,false);
        tvMaxPlayer=(TextView) row.findViewById(R.id.tvSPMaxPlayer);
        ivPlayerIcon=(ImageView) row.findViewById(R.id.ivSPMaxPlayer);

        row.requestLayout();
        row.getLayoutParams().width= CreateRoomUIDefine.MAXPLAYERITEM_WIDTH();
        row.getLayoutParams().height= CreateRoomUIDefine.MAXPLAYERITEM_HEIGHT();

        ivPlayerIcon.requestLayout();
        ivPlayerIcon.getLayoutParams().width= CreateRoomUIDefine.PLAYERICONIV_WIDTH();
        ivPlayerIcon.getLayoutParams().height= CreateRoomUIDefine.PLAYERICONIV_HEIGHT();
        tvMaxPlayer.setTextSize(UIDefine.MEDIUMTEXT_SIZE());

        tvMaxPlayer.setText(integerArr[position].toString());

        return row;
    }
}