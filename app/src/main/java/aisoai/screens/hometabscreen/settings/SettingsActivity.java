package aisoai.screens.hometabscreen.settings;

import android.widget.ImageButton;

import com.facebook.login.widget.LoginButton;

import aisoai.R;
import aisoai.TITApplication;
import aisoai.screens.titentities.activity.TITSpecActivity;
import aisoai.screens.titentities.control.TITSpecControl;

public class SettingsActivity extends TITSpecActivity
{
    private ImageButton ibtConfig;
    private ImageButton ibtHistory;
    private ImageButton ibtShop;
    private ImageButton ibtProfile;
    private ImageButton ibtComment;
    private LoginButton btFbLogout;

    @Override
    protected void linkToLayout()
    {
        setContentView(R.layout.settings);
        ibtConfig=(ImageButton) findViewById(R.id.ibtConfig);
        ibtHistory=(ImageButton) findViewById(R.id.ibtHistory);
        ibtShop=(ImageButton) findViewById(R.id.ibtShop);
        ibtProfile=(ImageButton) findViewById(R.id.ibtProfile);
        ibtComment=(ImageButton) findViewById(R.id.ibtComment);
        btFbLogout =(LoginButton) findViewById(R.id.btFbLogout);
    }

    @Override
    protected void scaleView()
    {
        ibtConfig.requestLayout();
        ibtConfig.getLayoutParams().width=SettingsUIDefine.IBT_WIDTH();
        ibtConfig.getLayoutParams().height=SettingsUIDefine.IBT_HEIGHT();

        ibtHistory.requestLayout();
        ibtHistory.getLayoutParams().width=SettingsUIDefine.IBT_WIDTH();
        ibtHistory.getLayoutParams().height=SettingsUIDefine.IBT_HEIGHT();

        ibtShop.requestLayout();
        ibtShop.getLayoutParams().width=SettingsUIDefine.IBT_WIDTH();
        ibtShop.getLayoutParams().height=SettingsUIDefine.IBT_HEIGHT();

        ibtProfile.requestLayout();
        ibtProfile.getLayoutParams().width=SettingsUIDefine.IBT_WIDTH();
        ibtProfile.getLayoutParams().height=SettingsUIDefine.IBT_HEIGHT();

        ibtComment.requestLayout();
        ibtComment.getLayoutParams().width=SettingsUIDefine.IBT_WIDTH();
        ibtComment.getLayoutParams().height=SettingsUIDefine.IBT_HEIGHT();

        btFbLogout.requestLayout();
        btFbLogout.getLayoutParams().width=SettingsUIDefine.IBT_WIDTH();
        btFbLogout.getLayoutParams().height=SettingsUIDefine.IBT_HEIGHT();
    }

    @Override
    protected TITSpecControl linkControl()
    {
        return TITApplication.getScreenControlManager().getSettingsControl();
    }

    @Override
    public SettingsControl getControl()
    {
        return (SettingsControl) control;
    }
}
