package aisoai.screens.hometabscreen.homeprofilescreen;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;
import aisoai.R;
import aisoai.screens.titentities.control.TITControl;
import aisoai.screens.titentities.dialog.TITIDialog;

public class ChallengeDialog extends TITIDialog
{
    public ChallengeDialog(Context context, TITControl control)
    {
        super(context,control);
    }

    private Spinner spPlayerMax;
    private Spinner spPlayType;
    private ImageButton ibtConfirm;
    private ImageButton ibtCancel;

    @Override
    protected int getLayout()
    {
        return R.layout.challenge_dialog;
    }

    @Override
    protected void linkToLayout()
    {
        spPlayerMax=(Spinner) findViewById(R.id.spPlayerMax);
        spPlayType=(Spinner) findViewById(R.id.spPlayType);
        ibtConfirm=(ImageButton) findViewById(R.id.ibtConfirm);
        ibtConfirm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
                getControl().challengeConfirmEvent();
            }
        });

        ibtCancel=(ImageButton) findViewById(R.id.ibtCancel);

        ibtCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
    }

    @Override
    protected void scaleView()
    {
        spPlayerMax.requestLayout();
        spPlayerMax.getLayoutParams().width=ProfileUIDefine.PLAYERMAXSP_WIDTH();
        spPlayerMax.getLayoutParams().height=ProfileUIDefine.PLAYERMAXSP_HEIGHT();

        spPlayType.requestLayout();
        spPlayType.getLayoutParams().width=ProfileUIDefine.PLAYTYPESP_WIDTH();
        spPlayType.getLayoutParams().height=ProfileUIDefine.PLAYTYPESP_HEIGHT();

        ibtConfirm.requestLayout();
        ibtConfirm.getLayoutParams().width=ProfileUIDefine.CONFIRMIBT_WIDTH();
        ibtConfirm.getLayoutParams().height=ProfileUIDefine.CONFIRMIBT_HEIGHT();

        ibtCancel.requestLayout();
        ibtCancel.getLayoutParams().width=ProfileUIDefine.CONFIRMIBT_WIDTH();
        ibtCancel.getLayoutParams().height=ProfileUIDefine.CONFIRMIBT_HEIGHT();
    }

    public void setContent()
    {
        Integer[] intArr={1,2,3,4};
        spPlayerMax.setAdapter
                (new MaxPlayerArrayAdapter((ProfileActivity) control.getActivity(), intArr));
        String[] strArr={"Một cho tất cả","Chọn cho đối thủ"};
        spPlayType.setAdapter
                (new PlayTypeArrayAdapter((ProfileActivity) control.getActivity(), strArr));
    }

    @Override
    public int getIcon()
    {
        return R.drawable.top20;
    }

    @Override
    protected void closeEvent()
    {
        dismiss();
    }

    @Override
    public String getTitle()
    {
        return "Tham Gia Thách Đấu";
    }

    @Override
    protected int getWidth()
    {
        return ProfileUIDefine.CHALLENGEDL_WIDTH();
    }

    @Override
    protected int getHeight()
    {
        return ProfileUIDefine.CHALLENGEDL_HEIGHT();
    }

    @Override
    public ProfileControl getControl()
    {
        return (ProfileControl) control;
    }

    public Spinner getSpPlayerMax()
    {
        return spPlayerMax;
    }

    public Spinner getSpPlayType()
    {
        return spPlayType;
    }
}