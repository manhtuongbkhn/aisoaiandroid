package aisoai.screens.hometabscreen.homeprofilescreen;

import aisoai.config.ClientConfig;
import aisoai.config.UIDefine;

public class ProfileUIDefine
{
    public static int USERAVATARIV_WIDTH()
    {
        Float f=60f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int USERAVATARIV_HEIGHT()
    {
        return USERAVATARIV_WIDTH();
    }

    public static int USERRANKIV_WIDTH()
    {
        Float f=50f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int USERRANKIV_HEIGHT()
    {
        return USERRANKIV_WIDTH();
    }

    public static int CHALLENGEIBT_WIDTH()
    {
        Float f=100f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int CHALLENGEIBT_HEIGHT()
    {
        Float f=40f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int TRAININGIBT_WIDTH()
    {
        Float f=100f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int SHAREBT_WIDTH()
    {
        Float f=70f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int SHAREBT_HEIGHT()
    {
        Float f=30f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static float SHARETEXT_SIZE()
    {
        return 10f*ClientConfig.SCREEN_WIDTH_DP/320;
    }

    public static int TRANINGIBT_HEIGHT()
    {
        Float f=40f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static float COMPETENCERADIUS_SCALE()
    {
        return 0.4f;
    }

    public static int COMPETENCETEXT_SIZE()
    {
        Float f= UIDefine.MEDIUMTEXT_SIZE()*ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static int CHALLENGEDL_WIDTH()
    {
        Float f=280*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int CHALLENGEDL_HEIGHT()
    {
        Float f=210*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int PLAYERMAXSP_WIDTH()
    {
        Float f=80*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int PLAYERMAXSP_HEIGHT()
    {
        Float f=40*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int PLAYERICONIV_WIDTH()
    {
        Float f=30f* ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int PLAYERICONIV_HEIGHT()
    {
        return PLAYERICONIV_WIDTH();
    }


    public static int MAXPLAYERITEM_WIDTH()
    {
        Float f=80f* ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int MAXPLAYERITEM_HEIGHT()
    {
        Float f=40f* ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int PLAYTYPESP_WIDTH()
    {
        Float f=150*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int PLAYTYPESP_HEIGHT()
    {
        Float f=40*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int ROOMTYPEITEM_WIDTH()
    {
        return PLAYTYPESP_WIDTH();
    }

    public static int ROOMTYPEITEM_HEIGHT()
    {
        return PLAYTYPESP_HEIGHT();
    }

    public static int CONFIRMIBT_WIDTH()
    {
        Float f=50*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int CONFIRMIBT_HEIGHT()
    {
        return CONFIRMIBT_WIDTH();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static int CHALLENGEWAITINGDL_WIDTH()
    {
        Float f=280*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int CHALLENGEWAITTINGDL_HEIGHT()
    {
        Float f=140*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int WAITINGPB_WIDTH()
    {
        Float f=70*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int WAITINGPB_HEIGHT()
    {
        return WAITINGPB_WIDTH();
    }

    public static int CANCELIBT_WIDTH()
    {
        Float f=50*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int CANCELIBT_HEIGHT()
    {
        return CONFIRMIBT_WIDTH();
    }
}
