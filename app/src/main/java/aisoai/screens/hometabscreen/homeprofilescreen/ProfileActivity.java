package aisoai.screens.hometabscreen.homeprofilescreen;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.widget.ShareButton;

import aisoai.TITApplication;
import aisoai.config.UIDefine;
import aisoai.R;
import aisoai.screens.titentities.activity.TITSpecActivity;
import aisoai.screens.titentities.control.TITSpecControl;

public class ProfileActivity extends TITSpecActivity
{
    private ImageView ivUserAvatar;
    private TextView tvFullName;
    private TextView tvMatchCount;
    private TextView tvWinPercent;
    private ImageView ivUserRank;
    private TextView tvUserRank;
    private ImageView ivCompetence;
    private ImageButton ibtChallenge;
    private ImageButton ibtTraining;
    private ShareButton btFbShare;

    private ChallengeDialog dlchallenge;
    private ChallengeWaitingDialog dlChallengeWaiting;

    @Override
    protected void linkToLayout()
    {
        setContentView(R.layout.profile);
        ivUserAvatar=(ImageView) findViewById(R.id.ivUserAvatar);
        tvFullName=(TextView) findViewById(R.id.tvFullName);
        ivUserRank=(ImageView) findViewById(R.id.ivUserRank);
        tvUserRank=(TextView) findViewById(R.id.tvUserRank);
        tvMatchCount=(TextView) findViewById(R.id.tvMatchCount);
        tvWinPercent=(TextView) findViewById(R.id.tvWinPercent);
        ivCompetence=(ImageView) findViewById(R.id.ivCompetence);
        ibtChallenge =(ImageButton) findViewById(R.id.ibtChallenge);
        ibtTraining =(ImageButton) findViewById(R.id.ibtTraining);
        btFbShare=(ShareButton) findViewById(R.id.btFbShare);

        ibtChallenge.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getControl().challengeEvent();
            }
        });

        ibtTraining.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getControl().trainingEvent();
            }
        });

        //dlchallenge=new ChallengeDialog(this,null);
        //dlChallengeWaiting=new ChallengeWaitingDialog(this,null);
    }

    @Override
    protected void scaleView()
    {
        ivUserAvatar.requestLayout();
        ivUserAvatar.getLayoutParams().width = ProfileUIDefine.USERAVATARIV_WIDTH();
        ivUserAvatar.getLayoutParams().height = ProfileUIDefine.USERAVATARIV_HEIGHT();

        ivUserRank.requestLayout();
        ivUserRank.getLayoutParams().width = ProfileUIDefine.USERRANKIV_WIDTH();
        ivUserRank.getLayoutParams().height = ProfileUIDefine.USERRANKIV_HEIGHT();

        ibtChallenge.requestLayout();
        ibtChallenge.getLayoutParams().width = ProfileUIDefine.CHALLENGEIBT_WIDTH();
        ibtChallenge.getLayoutParams().height = ProfileUIDefine.CHALLENGEIBT_HEIGHT();

        ibtTraining.requestLayout();
        ibtTraining.getLayoutParams().width = ProfileUIDefine.TRAININGIBT_WIDTH();
        ibtTraining.getLayoutParams().height = ProfileUIDefine.TRANINGIBT_HEIGHT();

        btFbShare.requestLayout();
        btFbShare.getLayoutParams().width=ProfileUIDefine.SHAREBT_WIDTH();
        btFbShare.getLayoutParams().height=ProfileUIDefine.SHAREBT_HEIGHT();
        btFbShare.setText("Share");

        tvFullName.setTextSize(UIDefine.BIGTEXT_SIZE());
        tvMatchCount.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvWinPercent.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvUserRank.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        btFbShare.setTextSize(ProfileUIDefine.SHARETEXT_SIZE());
    }

    public ImageView getIvUserAvatar()
    {
        return ivUserAvatar;
    }

    public TextView getTvFullName()
    {
        return tvFullName;
    }

    public ImageView getIvUserRank()
    {
        return ivUserRank;
    }

    public TextView getTvMatchCount()
    {
        return tvMatchCount;
    }

    public TextView getTvWinPercent()
    {
        return tvWinPercent;
    }

    public ImageView getIvCompetence()
    {
        return ivCompetence;
    }

    public ChallengeDialog getDlchallenge()
    {
        return dlchallenge;
    }

    public void setDlchallenge(ChallengeDialog dlchallenge)
    {
        this.dlchallenge = dlchallenge;
    }

    public ChallengeWaitingDialog getDlChallengeWaiting()
    {
        return dlChallengeWaiting;
    }

    public void setDlChallengeWaiting(ChallengeWaitingDialog dlChallengeWaiting)
    {
        this.dlChallengeWaiting = dlChallengeWaiting;
    }

    @Override
    protected TITSpecControl linkControl()
    {
        return TITApplication.getScreenControlManager().getProfileControl();
    }

    @Override
    public ProfileControl getControl()
    {
        return (ProfileControl) control;
    }

}
