package aisoai.screens;

import aisoai.screens.gamescreeens.auditiongame.AuditionGameControl;
import aisoai.screens.gamescreeens.choicemodgame.ChoiceModGameControl;
import aisoai.screens.gamescreeens.collectfruitgame.CollectFruitGameControl;
import aisoai.screens.gamescreeens.dinosaurjumpgame.DinosaurJumpGameControl;
import aisoai.screens.gamescreeens.doublepokemongame.DoublePokemonGameControl;
import aisoai.screens.gamescreeens.findpokemongame.FindPokemonGameControl;
import aisoai.screens.gamescreeens.flappydoraemon.FlappyDoraemonControl;
import aisoai.screens.gamescreeens.operationgame.OperationGameControl;
import aisoai.screens.gamescreeens.pianogame.PianoGameControl;
import aisoai.screens.gamescreeens.snakehuntinggame.SnakeHuntingControl;
import aisoai.screens.gamescreeens.sortrecgame.SortRecGameControl;
import aisoai.screens.gamescreeens.spiderwebgame.SpiderWebControl;
import aisoai.screens.gamescreeens.tetrisgame.TetrisGameControl;
import aisoai.screens.gamescreeens.zombiegame.ZombieGameControl;
import aisoai.screens.hometabscreen.friendtabcreen.FriendTabControl;
import aisoai.screens.hometabscreen.friendtabcreen.chatfriendscreen.ChatFriendControl;
import aisoai.screens.hometabscreen.settings.SettingsControl;
import aisoai.screens.hometabscreen.topuser.TopPlayerControl;
import aisoai.screens.insidetabscreen.chatroomscreen.ChatRoomControl;
import aisoai.screens.choicegamescreen.ChoiceGameControl;
import aisoai.screens.gamescreeens.circlegame.CircleGameControl;
import aisoai.screens.hometabscreen.friendtabcreen.friendlistscreen.FriendListControl;
import aisoai.screens.guidegamescreen.GuideGameControl;
import aisoai.screens.hometabscreen.homeprofilescreen.ProfileControl;
import aisoai.screens.insidetabscreen.insideroomscreen.InsideRoomControl;
import aisoai.screens.hometabscreen.roomlistscreen.RoomListControl;
import aisoai.screens.roundscoringscreen.RoundScoringControl;
import aisoai.screens.titentities.activity.ITITActivity;
import aisoai.screens.signinscreen.SignInControl;
import aisoai.screens.startscreen.StartControl;
import aisoai.screens.hometabscreen.HomeTabControl;
import aisoai.screens.insidetabscreen.InsideTabControl;
import aisoai.screens.titentities.control.TITAppControl;
import aisoai.screens.titentities.control.TITControl;
import aisoai.screens.titentities.control.TITNormalControl;
import aisoai.screens.titentities.control.TITTabControl;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.totalscoringscreen.TotalScoringControl;

public class TITScreenControlManager
{
    private static TITScreenControlManager defaultScreenControlManager;
    private TITControl control;

    private TITScreenControlManager()
    {
        control=new StartControl();
    }

    public static TITScreenControlManager getDefaultScreenControlManager()
    {
        if(defaultScreenControlManager == null)
        {
            defaultScreenControlManager  =new TITScreenControlManager();
        }
        return defaultScreenControlManager;
    }

    public void changeScreen(TITControl nextControl)
    {
        changeControl(nextControl);
        System.gc();
    }

    private void changeControl(TITControl nextControl)
    {
        TITControl backControl=control;
        control=nextControl;
        ITITActivity backActivity=backControl.getActivity();
        backActivity.changeActivity(nextControl.initActivity(), true);
    }

    public void setControl(TITControl control)
    {
        this.control=control;
    }

    public StartControl getStartControl()
    {
        return (StartControl) control;
    }

    public SignInControl getSignInControl()
    {
        return (SignInControl) control;
    }

    public HomeTabControl getHomeTabControl()
    {
        return (HomeTabControl) control;
    }

    public ProfileControl getProfileControl()
    {
        return getHomeTabControl().getProfileControl();
    }

    public RoomListControl getListRoomControl()
    {
        return getHomeTabControl().getRoomListControl();
    }

    public FriendTabControl getFriendTabControl()
    {
        return getHomeTabControl().getFriendTabControl();
    }

    public FriendListControl getFriendListControl()
    {
        return getFriendTabControl().getFriendListControl();
    }

    public ChatFriendControl getChatFriendControl()
    {
        return getFriendTabControl().getChatFriendControl();
    }

    public TopPlayerControl getTopUserControl()
    {
        return getHomeTabControl().getTopUserControl();
    }

    public SettingsControl getSettingsControl()
    {
        return getHomeTabControl().getSettingsControl();
    }

    public InsideTabControl getInsideTabControl()
    {
        return (InsideTabControl)control;
    }

    public InsideRoomControl getInsideRoomControl()
    {
        return getInsideTabControl().getInsideRoomControl();
    }

    public ChatRoomControl getChatRoomControl()
    {
        return getInsideTabControl().getChatRoomControl();
    }

    public ChoiceGameControl getChoiceGameControl()
    {
        return (ChoiceGameControl) control;
    }

    public GuideGameControl getGuideGameControl()
    {
        return (GuideGameControl) control;
    }

    public RoundScoringControl getRoundScoringControl()
    {
        return (RoundScoringControl) control;
    }

    public TotalScoringControl getTotalScoringControl()
    {
        return (TotalScoringControl) control;
    }

    public CircleGameControl getCircleGameControl()
    {
        return (CircleGameControl) control;
    }

    public ZombieGameControl getZombieGameControl()
    {
        return (ZombieGameControl) control;
    }

    public AuditionGameControl getAuditionGameControl()
    {
        return (AuditionGameControl) control;
    }

    public OperationGameControl getOperationGameControl()
    {
        return (OperationGameControl) control;
    }

    public FindPokemonGameControl getFindPokemonGameControl()
    {
        return (FindPokemonGameControl) control;
    }

    public ChoiceModGameControl getChoiceModGameControl()
    {
        return (ChoiceModGameControl) control;
    }

    public CollectFruitGameControl getCollectFruitGameControl()
    {
        return (CollectFruitGameControl) control;
    }

    public PianoGameControl getPianoGameControl()
    {
        return (PianoGameControl) control;
    }

    public DoublePokemonGameControl getDoublePokemonGameControl()
    {
        return (DoublePokemonGameControl) control;
    }

    public FlappyDoraemonControl getFlappyBirdControl()
    {
        return (FlappyDoraemonControl) control;
    }

    public DinosaurJumpGameControl getDinosaurJumpGameControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    public SortRecGameControl getSortRecGameControl()
    {
        return (SortRecGameControl) control;
    }

    public SpiderWebControl getSpiderWebControl()
    {
        return (SpiderWebControl) control;
    }

    public SnakeHuntingControl getSaSnakeHuntingControl()
    {
        return (SnakeHuntingControl) control;
    }

    public TetrisGameControl getTetrisGameControl()
    {
        return (TetrisGameControl) control;
    }

    public TITControl getControl()
    {
        return control;
    }

    public TITAppControl getAppControl()
    {
        return (TITAppControl) control;
    }

    public TITTabControl getTabControl()
    {
        return (TITTabControl) control;
    }

    public TITNormalControl getNormalControl()
    {
        return (TITNormalControl) control;
    }

    public TITGameControl getGameControl()
    {
        return (TITGameControl)control;
    }
}
