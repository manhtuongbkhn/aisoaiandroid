package aisoai.screens.titentities.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;
import aisoai.R;
import aisoai.config.ClientConfig;
import aisoai.config.UIDefine;
import aisoai.screens.titentities.model.TITUserInfo;
import aisoai.titapplib.TITFunction;
import aisoai.screens.titentities.control.TITControl;

abstract public class MoreUserInfoDialog extends TITIDialog
{
    protected ImageView ivAvatar;
    protected TextView tvName;
    protected ImageView ivRank;
    protected TextView tvMatchCount;
    protected TextView tvWinPercent;
    protected ImageView ivCompetence;

    public MoreUserInfoDialog(Context context, TITControl control)
    {
        super(context, control);
    }

    @Override
    public int getIcon()
    {
        return R.drawable.user_profile;
    }

    @Override
    protected void linkToLayout()
    {
        ivAvatar=(ImageView) findViewById(R.id.ivUserAvatar);
        tvName=(TextView) findViewById(R.id.tvFullName);
        ivRank=(ImageView) findViewById(R.id.ivUserRank);
        tvMatchCount=(TextView) findViewById(R.id.tvMatchCount);
        tvWinPercent=(TextView) findViewById(R.id.tvWinPercent);
        ivCompetence=(ImageView) findViewById(R.id.ivCompetence);
    }

    @Override
    protected void scaleView()
    {
        ivAvatar.requestLayout();
        ivAvatar.getLayoutParams().width=MoreUserInfoDialogUIDefine.AVATARIV_WIDTH();
        ivAvatar.getLayoutParams().height=MoreUserInfoDialogUIDefine.AVATARIV_HEIGHT();

        tvName.setTextSize(UIDefine.BIGTEXT_SIZE());

        ivRank.requestLayout();
        ivRank.getLayoutParams().width=MoreUserInfoDialogUIDefine.RANKIV_WIDTH();
        ivRank.getLayoutParams().height=MoreUserInfoDialogUIDefine.RANKIV_HEIGHT();

        tvMatchCount.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
        tvWinPercent.setTextSize(UIDefine.MEDIUMTEXT_SIZE());
    }

    public void setContent(TITUserInfo userInfo)
    {
        ivAvatar.setImageBitmap(userInfo.getAvatarBitmap());
        tvName.setText(userInfo.getFullName());
        ivRank.setImageResource(R.drawable.gold_medal);

        tvMatchCount.setText(userInfo.getMatchCount()+" trận");
        tvWinPercent.setText(userInfo.getWinPercent()+"% thắng");

        int width=MoreUserInfoDialogUIDefine.COMPETENCEIV_WIDTH();
        int height=MoreUserInfoDialogUIDefine.COMPETENCEIV_HEIGHT();
        float radiusScale=MoreUserInfoDialogUIDefine.COMPETENCERADIUS_SCALE();
        int textSize=MoreUserInfoDialogUIDefine.COMPETENCETEXT_SIZE();
        Bitmap competenceBitmap= TITFunction.createCompetenceBitmap(userInfo.getCompetenceArr()
                                                                ,width,height,radiusScale,textSize);
        ivCompetence.setImageBitmap(competenceBitmap);
    }

    @Override
    protected void closeEvent()
    {
        dismiss();
    }

    @Override
    protected int getWidth()
    {
        return MoreUserInfoDialogUIDefine.MOREUSERINFODL_WIDTH();
    }
}

class MoreUserInfoDialogUIDefine
{
    public static int MOREUSERINFODL_WIDTH()
    {
        Float f = 280f * ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int AVATARIV_WIDTH()
    {
        Float f = 50f * ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int AVATARIV_HEIGHT()
    {
        return AVATARIV_WIDTH();
    }

    public static int RANKIV_WIDTH()
    {
        Float f = 30* ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int RANKIV_HEIGHT()
    {
        return RANKIV_WIDTH();
    }

    public static int COMPETENCEIV_WIDTH()
    {
        Float f = 280f * ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int COMPETENCEIV_HEIGHT()
    {
        Float f = 210f * ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static float COMPETENCERADIUS_SCALE()
    {
        return 0.4f;
    }

    public static int COMPETENCETEXT_SIZE()
    {
        Float f= UIDefine.SMALLTEXT_SIZE()*ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }
}