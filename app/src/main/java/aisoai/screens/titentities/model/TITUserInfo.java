package aisoai.screens.titentities.model;

import android.graphics.Bitmap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import aisoai.config.KJS;
import aisoai.titapplib.TITFunction;
import aisoai.loadnetimage.TITRequestImage;

abstract public class TITUserInfo
{
    protected int systemUserId;
    protected String userId;
    protected String fullName;
    protected String facebookId;
    protected String facebookUrl;
    protected String avatarUrl;
    protected String gender;
    protected Bitmap avatarBitmap;

    protected int rank;
    protected int matchCount;
    protected int winPercent;
    protected ArrayList<Integer> competenceArr;

    public TITUserInfo(JsonObject jsonObject)
    {
        systemUserId=jsonObject.get(KJS.SYSTEM_USER_ID).getAsInt();
        userId=jsonObject.get(KJS.USER_ID).getAsString();
        fullName=jsonObject.get(KJS.FULL_NAME).getAsString();
        facebookId=jsonObject.get(KJS.FACEBOOK_ID).getAsString();
        facebookUrl=jsonObject.get(KJS.FACEBOOK_URL).getAsString();
        avatarUrl=jsonObject.get(KJS.AVATAR_URL).getAsString();
        gender=jsonObject.get(KJS.GENDER).getAsString();
        int avatarPriority;
        if(jsonObject.get(KJS.AVATAR_PRIORITY)==null)
        {
            avatarPriority=4;
        }
        else
        {
            avatarPriority=jsonObject.get(KJS.AVATAR_PRIORITY).getAsInt();
        }
        new TITRequestImage(avatarUrl,avatarPriority)
        {
            @Override
            protected void setImage(Bitmap bitmap)
            {
                avatarBitmap=bitmap;
                reloadImage(bitmap);
            }
        };
        rank=jsonObject.get(KJS.RANK).getAsInt();
        matchCount=jsonObject.get(KJS.MATCH_COUNT).getAsInt();
        winPercent=jsonObject.get(KJS.WIN_PERCENT).getAsInt();
        JsonArray jsonArray=jsonObject.get(KJS.COMPETENCE).getAsJsonArray();
        competenceArr= TITFunction.covertToIntArrayList(jsonArray);
    }

    public void reload(JsonObject jsonObject)
    {
        rank=jsonObject.get(KJS.RANK).getAsInt();
        matchCount=jsonObject.get(KJS.MATCH_COUNT).getAsInt();
        winPercent=jsonObject.get(KJS.WIN_PERCENT).getAsInt();
        JsonArray jsonArray=jsonObject.get(KJS.COMPETENCE).getAsJsonArray();
        competenceArr= TITFunction.covertToIntArrayList(jsonArray);
    }

    abstract public void reloadImage(Bitmap bitmap);

    public String getUserId()
    {
        return userId;
    }

    public int getSystemUserId()
    {
        return systemUserId;
    }

    public String getFullName()
    {
        return fullName;
    }

    public String getFacebookId()
    {
        return facebookId;
    }

    public String getFacebookUrl()
    {
        return facebookUrl;
    }

    public String getAvatarUrl()
    {
        return avatarUrl;
    }

    public String getGender()
    {
        return gender;
    }

    public Bitmap getAvatarBitmap()
    {
        return avatarBitmap;
    }

    public int getRank()
    {
        return rank;
    }

    public int getMatchCount()
    {
        return matchCount;
    }

    public int getWinPercent()
    {
        return winPercent;
    }

    public ArrayList<Integer> getCompetenceArr()
    {
        return competenceArr;
    }
}
