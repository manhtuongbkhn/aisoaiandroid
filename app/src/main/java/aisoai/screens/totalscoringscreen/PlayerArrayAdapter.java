package aisoai.screens.totalscoringscreen;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import aisoai.R;
import aisoai.config.UIDefine;
import aisoai.screens.roundscoringscreen.RoundScoringUIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;

public class PlayerArrayAdapter extends ArrayAdapter<PlayerInfo>
{
    private TITNormalActivity titActivity;
    private ArrayList<PlayerInfo> playerArr;

    private View item;
    private ImageView ivPlayerAvatar;
    private TextView tvPlayerName;
    private ArrayList <TextView> tvRoundPointArr;
    private TextView tvTotalPoint;
    private TextView tvPlayerRank;

    public PlayerArrayAdapter(TITNormalActivity titActivity, ArrayList<PlayerInfo> playerArr)
    {
        super(titActivity, R.layout.totalscoring_player_item,playerArr);
        this.titActivity=titActivity;
        this.playerArr=playerArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        item=inflater.inflate(R.layout.totalscoring_player_item,parent,false);
        linkToLayout();
        scaleView();
        setContentView(position);
        return item;
    }

    public void linkToLayout()
    {
        tvRoundPointArr=new ArrayList<TextView>();
        ivPlayerAvatar=(ImageView) item.findViewById(R.id.ivPlayerAvatar);
        tvPlayerName=(TextView) item.findViewById(R.id.tvPlayerName);
        tvRoundPointArr.add((TextView) item.findViewById(R.id.tvRound1Point));
        tvRoundPointArr.add((TextView) item.findViewById(R.id.tvRound2Point));
        tvRoundPointArr.add((TextView) item.findViewById(R.id.tvRound3Point));
        tvRoundPointArr.add((TextView) item.findViewById(R.id.tvRound4Point));
        tvRoundPointArr.add((TextView) item.findViewById(R.id.tvRound5Point));
        tvTotalPoint=(TextView) item.findViewById(R.id.tvTotalPoint);
        tvPlayerRank=(TextView) item.findViewById(R.id.tvPlayerRank);
    }

    public void scaleView()
    {
        item.requestLayout();
        item.getLayoutParams().width= RoundScoringUIDefine.PLAYERITEM_WIDTH();
        item.getLayoutParams().height= RoundScoringUIDefine.PLAYERITEM_HEIGHT();

        ivPlayerAvatar.requestLayout();
        ivPlayerAvatar.getLayoutParams().width= RoundScoringUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayerAvatar.getLayoutParams().height= RoundScoringUIDefine.PLAYERAVATARIV_HEIGHT();

        tvPlayerName.setTextSize(UIDefine.TINYTEXT_SIZE());
        tvTotalPoint.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvPlayerRank.setTextSize(UIDefine.SMALLTEXT_SIZE());

        for(int i=0;i<tvRoundPointArr.size();i++)
        {
            TextView textView=tvRoundPointArr.get(i);
            textView.setTextSize(UIDefine.SMALLTEXT_SIZE());
        }
    }

    public void setContentView(int postion)
    {
        PlayerInfo playerInfo=playerArr.get(postion);
        Bitmap avatarBitmap=playerInfo.getAvatarBitmap();
        String playerName=playerInfo.getFullName();
        ArrayList<Integer> roundPointArr=playerInfo.getRoundPointArr();
        int roundCount=roundPointArr.size();
        rescaleView(roundCount,postion);
        Integer totalPoint=playerInfo.getTotalPoint();
        Integer rank=playerInfo.getRank();

        ivPlayerAvatar.setImageBitmap(avatarBitmap);
        tvPlayerName.setText(playerName);

        for(int i=0;i<roundCount;i++)
        {
            TextView textView=tvRoundPointArr.get(i);
            Integer point=roundPointArr.get(i);
            textView.setText(point.toString());
        }

        tvTotalPoint.setText(totalPoint.toString());
        tvPlayerRank.setText(rank.toString());
    }

    public void rescaleView(int roundCount,int postion)
    {
        int TV_WIDTH= TotalScoringDefine.RIGHTLL_WIDTH()/(roundCount+2);
        for(int i=0;i<roundCount;i++)
        {
            TextView textView=tvRoundPointArr.get(i);
            textView.requestLayout();
            textView.getLayoutParams().width=TV_WIDTH;
        }

        for(int i=roundCount;i<5;i++)
        {
            TextView textView=tvRoundPointArr.get(i);
            textView.requestLayout();
            textView.getLayoutParams().width=0;
        }

        tvTotalPoint.requestLayout();
        tvTotalPoint.getLayoutParams().width=TV_WIDTH;

        tvPlayerRank.requestLayout();
        tvPlayerRank.getLayoutParams().width=TV_WIDTH;
    }
}
