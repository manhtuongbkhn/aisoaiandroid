package aisoai.screens.totalscoringscreen;

import aisoai.config.ClientConfig;

public class TotalScoringDefine
{
    public static int EXITROOMBT_WIDTH()
    {
        Float f=50f* ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int EXITROOMBT_HEIGHT()
    {
        Float f=50f* ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int RIGHTLL_WIDTH()
    {
        Float f=240f* ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int ITEM_HEIGHT()
    {
        Float f=80f*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }
}
