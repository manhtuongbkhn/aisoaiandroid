package aisoai.screens.totalscoringscreen;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import aisoai.R;
import aisoai.TITApplication;
import aisoai.screens.titentities.activity.TITNormalActivity;

public class TotalScoringActivity extends TITNormalActivity
{
    private ArrayList<TextView> tvRoundPointLbArr=new ArrayList<TextView>();
    private TextView tvTotalPointLb;
    private TextView tvPlayerRankLb;
    private ListView lvPlayer;
    private TextView tvNotification;
    private Button btExitRoom;
    private Button btBackRoom;

    @Override
    protected void linkToLayout()
    {
        setContentView(R.layout.total_scoring);
        tvRoundPointLbArr.add((TextView) findViewById(R.id.tvRound1PointLb));
        tvRoundPointLbArr.add((TextView) findViewById(R.id.tvRound2PointLb));
        tvRoundPointLbArr.add((TextView) findViewById(R.id.tvRound3PointLb));
        tvRoundPointLbArr.add((TextView) findViewById(R.id.tvRound4PointLb));
        tvRoundPointLbArr.add((TextView) findViewById(R.id.tvRound5PointLb));
        tvTotalPointLb=(TextView) findViewById(R.id.tvTotalPointLb);
        tvPlayerRankLb=(TextView) findViewById(R.id.tvPlayerRankLb);

        lvPlayer=(ListView) findViewById(R.id.lvPlayer);
        tvNotification=(TextView) findViewById(R.id.tvNotification);
        btExitRoom=(Button) findViewById(R.id.btExitRoom);
        btBackRoom=(Button) findViewById(R.id.btBackRoom);

        btExitRoom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getControl().exitRoomFinishEvent();
            }
        });

        btBackRoom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getControl().backRoomFinishEvent();
            }
        });
    }

    @Override
    protected void scaleView()
    {
        btExitRoom.requestLayout();
        btExitRoom.getLayoutParams().width= TotalScoringDefine.EXITROOMBT_WIDTH();
        btExitRoom.getLayoutParams().height= TotalScoringDefine.EXITROOMBT_HEIGHT();

        btBackRoom.getLayoutParams().width= TotalScoringDefine.EXITROOMBT_WIDTH();
        btBackRoom.getLayoutParams().height= TotalScoringDefine.EXITROOMBT_HEIGHT();
    }

    @Override
    protected TotalScoringControl linkControl()
    {
        return TITApplication.getScreenControlManager().getTotalScoringControl();
    }

    public void reScaleView(int roundCount)
    {
        int TV_WIDTH= TotalScoringDefine.RIGHTLL_WIDTH()/(roundCount+2);
        for(int i=0;i<roundCount;i++)
        {
            TextView textView=tvRoundPointLbArr.get(i);
            textView.requestLayout();
            textView.getLayoutParams().width=TV_WIDTH;
        }
        for(int i=roundCount;i<5;i++)
        {
            TextView textView=tvRoundPointLbArr.get(i);
            textView.requestLayout();
            textView.getLayoutParams().width=0;
        }
        tvTotalPointLb.requestLayout();
        tvTotalPointLb.getLayoutParams().width=TV_WIDTH;
        tvPlayerRankLb.requestLayout();
        tvPlayerRankLb.getLayoutParams().width=TV_WIDTH;

    }

    public ListView getLvPlayer()
    {
        return lvPlayer;
    }

    public TextView getTvNotification()
    {
        return tvNotification;
    }

    public Button getBtExitRoom()
    {
        return btExitRoom;
    }

    public Button getBtBackRoom()
    {
        return btBackRoom;
    }

    @Override
    public TotalScoringControl getControl()
    {
        return (TotalScoringControl) control;
    }
}
