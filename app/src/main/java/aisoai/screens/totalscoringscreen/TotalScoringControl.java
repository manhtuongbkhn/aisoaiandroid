package aisoai.screens.totalscoringscreen;

import android.graphics.Bitmap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import aisoai.TITApplication;
import aisoai.config.KJS;
import aisoai.config.StrDefine;
import aisoai.screens.hometabscreen.HomeTabControl;
import aisoai.screens.insidetabscreen.InsideTabControl;
import aisoai.screens.titentities.activity.TITNormalActivity;
import aisoai.loadnetimage.TITRequestImage;
import aisoai.screens.titentities.control.TITNormalControl;
import aisoai.screens.titentities.model.TITModel;
import aisoai.screens.titentities.TITRequestFactory;

public class TotalScoringControl extends TITNormalControl
{

    @Override
    public void init()
    {
        PlayerArrayAdapter arrayAdapter=
                                    new PlayerArrayAdapter(getActivity(),getModel().getUserArr());
        getActivity().getLvPlayer().setAdapter(arrayAdapter);
    }

    @Override
    public void finish()
    {

    }

    public void exitRoomFinishEvent()
    {
        getRequestFactory().exitRoomWhenFinishRequest();
    }

    public void backRoomFinishEvent()
    {
        getRequestFactory().backRoomFinishRequest();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void roomInfoNotify(JsonObject fromServerData)
    {
        int roomType=fromServerData.get(KJS.ROOM_TYPE).getAsInt();
        switch (roomType)
        {
            case 1:
                break;
            case 2:
                getActivity().getBtBackRoom().setEnabled(false);
                break;
        }
    }

    public void totalScoringUserInfo(JsonObject fromServerData)
    {
        JsonArray jsonArray=fromServerData.get(KJS.ARRAY).getAsJsonArray();
        int roundCount=jsonArray.size()+1;
        getActivity().reScaleView(roundCount);

        for(int i=0;i<jsonArray.size();i++)
        {
            JsonObject jsonObject=jsonArray.get(i).getAsJsonObject();
            PlayerInfo playerInfo=covertToPlayerInfo(jsonObject);
            getModel().getUserArr().add(playerInfo);
        }
        getActivity().getLvPlayer().invalidateViews();
    }

    public void backRoomFinishResponse(JsonObject fromServerData)
    {
        boolean sucess=fromServerData.get(KJS.SUCESS).getAsBoolean();

        if(sucess)
        {
            TITApplication.getScreenControlManager().changeScreen(new InsideTabControl());
        }
        else
            getActivity().showMessage(StrDefine.WAS_KICKED_USER_SHOW,1);
    }

    public void exitRoomFinishResponse(JsonObject fromServerData)
    {
        TITApplication.getScreenControlManager().changeScreen(new HomeTabControl());
    }

    public void totalScoringDownTimeNotify(JsonObject fromServerData)
    {

    }

    public void wasKickRoomBySystem(JsonObject fromServerData)
    {
        getActivity().showMessage(StrDefine.WAS_KICKED_USER_SHOW,1);
        getActivity().getBtBackRoom().setEnabled(false);
    }

    public PlayerInfo covertToPlayerInfo(JsonObject jsonObject)
    {
        final PlayerInfo playerInfo=new PlayerInfo();
        String avatarUrl=jsonObject.get(KJS.AVATAR_URL).getAsString();
        String fullName=jsonObject.get(KJS.FULL_NAME).getAsString();
        JsonArray jsonArray=jsonObject.getAsJsonArray(KJS.POINT_ARR);
        ArrayList<Integer> roundPointArr=new ArrayList<Integer>();

        for(int i=0;i<jsonArray.size();i++)
        {
            roundPointArr.add(jsonArray.get(i).getAsInt());
        }

        int totalPoint=jsonObject.get(KJS.TOTAl_POINT).getAsInt();
        int rank=jsonObject.get(KJS.RANK).getAsInt();

        new TITRequestImage(avatarUrl,6)
        {
            @Override
            protected void setImage(Bitmap bitmap)
            {
                playerInfo.setAvatarBitmap(bitmap);
                getActivity().getLvPlayer().invalidateViews();
            }
        };

        playerInfo.setFullName(fullName);
        playerInfo.setRoundPointArr(roundPointArr);
        playerInfo.setTotalPoint(totalPoint);
        playerInfo.setRank(rank);

        return playerInfo;
    }


    @Override
    public Class<? extends TITNormalActivity> initActivity()
    {
        return TotalScoringActivity.class;
    }

    @Override
    protected TITModel initModel()
    {
        return new TotalScoringModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new TotalScoringRF();
    }

    @Override
    public TotalScoringActivity getActivity()
    {
        return (TotalScoringActivity) activity;
    }

    @Override
    public TotalScoringModel getModel()
    {
        return (TotalScoringModel) model;
    }

    @Override
    public TotalScoringRF getRequestFactory()
    {
        return (TotalScoringRF) requestFactory;
    }
}
