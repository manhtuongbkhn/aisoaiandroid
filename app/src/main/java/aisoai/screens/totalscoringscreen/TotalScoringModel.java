package aisoai.screens.totalscoringscreen;

import java.util.ArrayList;

import aisoai.screens.titentities.model.TITModel;

public class TotalScoringModel extends TITModel
{
    private ArrayList<PlayerInfo> userArr=new ArrayList<PlayerInfo>();

    public ArrayList<PlayerInfo> getUserArr()
    {
        return userArr;
    }

    public void setUserArr(ArrayList<PlayerInfo> userArr)
    {
        this.userArr = userArr;
    }
}
