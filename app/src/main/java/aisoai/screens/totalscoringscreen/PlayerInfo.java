package aisoai.screens.totalscoringscreen;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class PlayerInfo
{
    private String fullName;
    private String avatarUrl;
    private int totalPoint;
    private ArrayList<Integer> roundPointArr;
    private int rank;
    private Bitmap avatarBitmap;

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getAvatarUrl()
    {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl)
    {
        this.avatarUrl = avatarUrl;
    }

    public int getTotalPoint()
    {
        return totalPoint;
    }

    public void setTotalPoint(int totalPoint)
    {
        this.totalPoint = totalPoint;
    }

    public ArrayList<Integer> getRoundPointArr()
    {
        return roundPointArr;
    }

    public void setRoundPointArr(ArrayList<Integer> roundPointArr)
    {
        this.roundPointArr = roundPointArr;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Bitmap getAvatarBitmap()
    {
        return avatarBitmap;
    }

    public void setAvatarBitmap(Bitmap avatarBitmap)
    {
        this.avatarBitmap = avatarBitmap;
    }
}
