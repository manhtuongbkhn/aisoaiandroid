package aisoai.screens.insidetabscreen.insideroomscreen;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import aisoai.config.KJS;
import aisoai.screens.titentities.model.TITUserInfo;
import aisoai.titapplib.TITFunction;

abstract public class PlayerInfo extends TITUserInfo
{
    protected boolean isOwner;

    public PlayerInfo(JsonObject jsonObject)
    {
        super(jsonObject);
        isOwner=jsonObject.get(KJS.IS_OWNER).getAsBoolean();
    }

    public boolean equals(PlayerInfo otherPlayerInfo)
    {
        boolean b1=systemUserId==otherPlayerInfo.getSystemUserId();
        boolean b2=systemUserId==otherPlayerInfo.getSystemUserId();
        return b1&&b2;
    }

    /*
    public JsonObject toJsonObject()
    {
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty(KJS.USER_ID,userId);
        jsonObject.addProperty(KJS.SYSTEM_USER_ID,systemUserId);
        jsonObject.addProperty(KJS.FULL_NAME,fullName);
        jsonObject.addProperty(KJS.FACEBOOK_URL,facebookUrl);
        jsonObject.addProperty(KJS.AVATAR_URL,avatarUrl);
        jsonObject.addProperty(KJS.GENDER,gender);
        jsonObject.addProperty(KJS.RANK,rank);
        jsonObject.addProperty(KJS.MATCH_COUNT,matchCount);
        jsonObject.addProperty(KJS.WIN_PERCENT, winPercent);
        JsonArray jsonArray=TITFunction.covertToFloatJsonArray(competenceArr);
        jsonObject.add(KJS.COMPETENCE,jsonArray);
        jsonObject.addProperty(KJS.IS_OWNER,isOwner);
        return jsonObject;
    }
    */

    public boolean isOwner()
    {
        return isOwner;
    }
}
