package aisoai.screens.gamescreeens.operationgame;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class OperatorButton extends TITSprite
{
    private int type;
    /*
        1:add
        2:minus
        3:multi
        4:divide
     */

    public OperatorButton(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public OperationGameControl getControl()
    {
        return (OperationGameControl) control;
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        if(event.getAction()== TouchEvent.ACTION_UP)
        {
            getControl().operatorButtonTouchEvent(this);
        }
        return true;
    }


    public static OperatorButton createOperatorButton(int type,TITGameControl inputGameControl)
    {
        float x,y,width,height;
        width=OperationGameConfig.BUTTON_WIDTH();
        height=OperationGameConfig.BUTTON_HEIGHT();
        TextureRegion textureRegion;
        switch (type)
        {
            case 1:
                x=OperationGameConfig.ADDBUTTON_X();
                y=OperationGameConfig.ADDBUTTON_Y();
                textureRegion=OperationGameResource.getAdd();
                break;
            case 2:
                x=OperationGameConfig.MINUSBUTTON_X();
                y=OperationGameConfig.MINUSBUTTON_Y();
                textureRegion=OperationGameResource.getMinus();
                break;
            case 3:
                x=OperationGameConfig.MULTIBUTTON_X();
                y=OperationGameConfig.MULTIBUTTON_Y();
                textureRegion=OperationGameResource.getMulti();
                break;
            case 4:
            default:
                x=OperationGameConfig.DIVIDEBUTTON_X();
                y=OperationGameConfig.DIVIDEBUTTON_Y();
                textureRegion=OperationGameResource.getDivide();
                break;
        }
        OperatorButton operatorButton=new OperatorButton(x,y,width,height,textureRegion,
                                                                                inputGameControl);
        operatorButton.setType(type);
        return operatorButton;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
}
