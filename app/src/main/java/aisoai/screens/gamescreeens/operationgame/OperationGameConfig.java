package aisoai.screens.gamescreeens.operationgame;

import android.graphics.Color;

import aisoai.config.ClientConfig;
import aisoai.config.UIDefine;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class OperationGameConfig
{
    public static float BORDER_WIDTH()
    {
        return 320*TITGameConfig.GS_WIDTH()/320;
    }

    public static float BORDER_HEIGHT()
    {
        return 150*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float BORDER_X()
    {
        return 0;
    }

    public static float BORDER_Y()
    {
        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public static float BUTTON_WIDTH()
    {
        return 100*TITGameConfig.GS_WIDTH()/320;
    }

    public static float BUTTON_HEIGHT()
    {
        return BUTTON_WIDTH();
    }

    public static float ADDBUTTON_X()
    {
        return 80*TITGameConfig.GS_WIDTH()/320-BUTTON_WIDTH()/2;
    }

    public static float ADDBUTTON_Y()
    {
        return 210*TITGameConfig.GS_HEIGHT()/390-BUTTON_HEIGHT()/2;
    }

    public static float MINUSBUTTON_X()
    {
        return 240*TITGameConfig.GS_WIDTH()/320-BUTTON_WIDTH()/2;
    }

    public static float MINUSBUTTON_Y()
    {
        return 210*TITGameConfig.GS_HEIGHT()/390-BUTTON_HEIGHT()/2;
    }

    public static float MULTIBUTTON_X()
    {
        return 80*TITGameConfig.GS_WIDTH()/320-BUTTON_WIDTH()/2;
    }

    public static float MULTIBUTTON_Y()
    {
        return 330*TITGameConfig.GS_HEIGHT()/390-BUTTON_HEIGHT()/2;
    }

    public static float DIVIDEBUTTON_X()
    {
        return 240*TITGameConfig.GS_WIDTH()/320-BUTTON_WIDTH()/2;
    }

    public static float DIVIDEBUTTON_Y()
    {
        return 330*TITGameConfig.GS_HEIGHT()/390-BUTTON_HEIGHT()/2;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public static float QUESTIONTEXT_CENTERX()
    {
        return 160*TITGameConfig.GS_WIDTH()/320;
    }

    public static float QUESTIONTEXT_CENTERY()
    {
        return 75*TITGameConfig.GS_HEIGHT()/390;
    }

    public static int QUESTION_FONT_SIZE()
    {
        Float f= UIDefine.GIGATICTEXT_SIZE()* ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    public static int QUESTION_COLOR= Color.BLUE;


}
