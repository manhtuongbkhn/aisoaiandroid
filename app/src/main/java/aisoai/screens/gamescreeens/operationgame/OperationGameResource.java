package aisoai.screens.gamescreeens.operationgame;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class OperationGameResource extends TITGameResource
{
    private static TextureRegion add;
    private static TextureRegion minus;
    private static TextureRegion multi;
    private static TextureRegion divide;
    private static TextureRegion border;
    private static TextureRegion background;
    private static Font questionTextFont;

    public OperationGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {
        questionTextFont=createFontFromAsset(OperationGameConfig.QUESTION_FONT_SIZE(),
                OperationGameConfig.QUESTION_COLOR,"times.ttf");
    }

    @Override
    protected void initPictures()
    {
        add=createTextureRegionFromAsset("operationgame/add.png",2);
        minus=createTextureRegionFromAsset("operationgame/minus.png",2);
        multi=createTextureRegionFromAsset("operationgame/multi.png",2);
        divide=createTextureRegionFromAsset("operationgame/divide.png",2);
        border=createTextureRegionFromAsset("border.png",3);
        background=createTextureRegionFromAsset("operationgame/background.png",4);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    @Override
    public void clearResource()
    {
        add=null;
        minus=null;
        multi=null;
        divide=null;
        background=null;

        questionTextFont=null;
    }

    public static TextureRegion getAdd()
    {
        return add;
    }

    public static TextureRegion getMinus()
    {
        return minus;
    }

    public static TextureRegion getMulti()
    {
        return multi;
    }

    public static TextureRegion getDivide()
    {
        return divide;
    }

    public static TextureRegion getBorder()
    {
        return border;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public static Font getQuestionTextFont()
    {
        return questionTextFont;
    }
}
