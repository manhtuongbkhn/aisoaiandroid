package aisoai.screens.gamescreeens.operationgame;

import org.andengine.opengl.font.Font;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITText;

public class QuestionText extends TITText
{
    public QuestionText(float x, float y, Font font, String text, int textMax,
                                                                    TITGameControl inputControl)
    {
        super(x, y, font, text, textMax, inputControl);
    }

    public static QuestionText createQuestionText(String text,TITGameControl inputControl)
    {
        int textCharMax=14;
        Float x,y,width,height;

        Font font=OperationGameResource.getQuestionTextFont();
        width=(float) font.getStringWidth(text);
        height=(float) font.getLineHeight();
        x=OperationGameConfig.QUESTIONTEXT_CENTERX()-width/2;
        y=OperationGameConfig.QUESTIONTEXT_CENTERY()-height/2;
        QuestionText questionText=new QuestionText(x,y,font,text,textCharMax,inputControl);
        questionText.setHorizontalAlign(TITGameConfig.HORIZONTAL_ALIGN());
        return questionText;
    }
}
