package aisoai.screens.gamescreeens.operationgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class Border extends TITSprite
{

    public Border(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                    TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public TITGameControl getControl()
    {
        return (OperationGameControl) control;
    }

    public static Border createBorder(TITGameControl inputControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=OperationGameResource.getBorder();
        width=OperationGameConfig.BORDER_WIDTH();
        height=OperationGameConfig.BORDER_HEIGHT();
        x=OperationGameConfig.BORDER_X();
        y=OperationGameConfig.BORDER_Y();

        Border border=new Border(x,y,width,height,textureRegion,inputControl);
        return  border;
    }
}
