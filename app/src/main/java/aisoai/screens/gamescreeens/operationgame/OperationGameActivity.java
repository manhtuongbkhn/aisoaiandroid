package aisoai.screens.gamescreeens.operationgame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;
import aisoai.screens.titentities.TITRequestFactory;

public class OperationGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getOperationGameControl();
    }

    @Override
    public OperationGameControl getControl()
    {
        return (OperationGameControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new OperationGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new OperationGameScene();
    }

}
