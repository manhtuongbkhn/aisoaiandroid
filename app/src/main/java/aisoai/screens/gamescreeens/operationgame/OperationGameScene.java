package aisoai.screens.gamescreeens.operationgame;

import org.andengine.entity.IEntity;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class OperationGameScene extends TITScene
{
    private QuestionText questionText;

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();

        if(inputClass.equals(QuestionText.class))
        {
            QuestionText iQuestionText =(QuestionText) iEntity;
            this.questionText=iQuestionText;
        }

        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();
        if(inputClass.equals(QuestionText.class))
        {
            QuestionText iQuestionText =(QuestionText) iEntity;
            this.questionText=null;
        }

        return super.detachChild(iEntity);
    }

    public QuestionText getQuestionText()
    {
        return questionText;
    }

    protected OperationGameControl getControl()
    {
        return (OperationGameControl) control;
    }
}
