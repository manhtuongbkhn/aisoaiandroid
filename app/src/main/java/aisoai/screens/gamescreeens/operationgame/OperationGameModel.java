package aisoai.screens.gamescreeens.operationgame;

import com.google.gson.JsonObject;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class OperationGameModel extends TITGameModel
{
    private JsonObject currentQuestion;
    private GameStatus gameStatus;

    public JsonObject getCurrentQuestion()
    {
        return currentQuestion;
    }

    public void setCurrentQuestion(JsonObject currentQuestion)
    {
        this.currentQuestion = currentQuestion;
    }

    public GameStatus getGameStatus()
    {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus)
    {
        this.gameStatus = gameStatus;
    }
}
