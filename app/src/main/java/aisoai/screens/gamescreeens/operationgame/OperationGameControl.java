package aisoai.screens.gamescreeens.operationgame;

import com.google.gson.JsonObject;

import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class OperationGameControl extends TITGameControl
{

    @Override
    protected TextureRegion getBackground()
    {
        return OperationGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        initOperatorButton();
        initBorder();
        initQuestionText();
    }

    private void initOperatorButton()
    {
        OperatorButton addButton=OperatorButton.createOperatorButton(1,this);
        OperatorButton minusButton=OperatorButton.createOperatorButton(2,this);
        OperatorButton multiButton=OperatorButton.createOperatorButton(3,this);
        OperatorButton divideButton=OperatorButton.createOperatorButton(4,this);

        getScene().attachChild(addButton);
        getScene().attachChild(minusButton);
        getScene().attachChild(multiButton);
        getScene().attachChild(divideButton);

        getScene().registerTouchArea(addButton);
        getScene().registerTouchArea(minusButton);
        getScene().registerTouchArea(multiButton);
        getScene().registerTouchArea(divideButton);
    }

    private void initBorder()
    {
        Border border=Border.createBorder(this);
        getScene().attachChild(border);
    }

    private void initQuestionText()
    {
        QuestionText questionText=QuestionText.createQuestionText("",this);
        getScene().attachChild(questionText);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                getModel().setGameStatus(GameStatus.SLEEPING);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;
            case "createNewQuestion":
                int number1=token.get(KJS.PARAM1).getAsInt();
                int number2=token.get(KJS.PARAM2).getAsInt();
                int result=token.get(KJS.PARAM3).getAsInt();
                showQuestion(number1,number2,result);
                getModel().setCurrentQuestion(token);
                gameScriptReader.stop();
                break;
        }
    }

    private void showQuestion(Integer number1,Integer number2,Integer result)
    {
        String text=number1.toString()+" ▢ "+number2.toString()+" = "+result.toString();
        QuestionText questionText=getScene().getQuestionText();
        questionText.changeText(text);

        float centerX=questionText.getWidth()/2;
        float centerY=questionText.getHeight()/2;
        ScaleAtModifier scaleAtModifier=new ScaleAtModifier(0.5f,0f,1f,centerX,centerY);
        IModifier.IModifierListener modifierListener=new IModifier.IModifierListener()
        {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem)
            {

            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem)
            {
                getModel().setGameStatus(GameStatus.PLAYING);
            }
        };

        scaleAtModifier.addModifierListener(modifierListener);
        scaleAtModifier.setAutoUnregisterWhenFinished(true);
        questionText.registerEntityModifier(scaleAtModifier);
    }

    public void operatorButtonTouchEvent(OperatorButton operatorButton)
    {
        if(getModel().getGameStatus()==GameStatus.PLAYING)
        {
            getModel().setGameStatus(GameStatus.SLEEPING);
            boolean isTrueAnswer = checkAnswer(operatorButton);
            if (isTrueAnswer)
                playTrueSound();
            else
                playTrueSound();
            sendAnswer(operatorButton);
            QuestionText questionText = getScene().getQuestionText();
            float centerX = questionText.getWidth() / 2;
            float centerY = questionText.getHeight() / 2;
            ScaleAtModifier scaleAtModifier = new ScaleAtModifier(0.5f, 1, 0f, centerX, centerY);
            IModifier.IModifierListener modifierListener = new IModifier.IModifierListener() {
                @Override
                public void onModifierStarted(IModifier pModifier, Object pItem)
                {

                }

                @Override
                public void onModifierFinished(IModifier pModifier, Object pItem) {
                    getScene().getQuestionText().changeText("");
                    gameScriptReader.resume();
                }
            };

            scaleAtModifier.addModifierListener(modifierListener);
            scaleAtModifier.setAutoUnregisterWhenFinished(true);
            questionText.registerEntityModifier(scaleAtModifier);
        }
    }

    private void sendAnswer(OperatorButton operatorButton)
    {
        int operator=operatorButton.getType();
        JsonObject currentQuestion=getModel().getCurrentQuestion();
        int index=currentQuestion.get(KJS.INDEX).getAsInt();
        getRequestFactory().sendGameAnswer(index,operator);
    }

    private boolean checkAnswer(OperatorButton operatorButton)
    {
        JsonObject currentQuestion=getModel().getCurrentQuestion();
        int number1=currentQuestion.get(KJS.PARAM1).getAsInt();
        int number2=currentQuestion.get(KJS.PARAM2).getAsInt();
        int result=currentQuestion.get(KJS.PARAM3).getAsInt();

        switch (operatorButton.getType())
        {
            case 1:
                return number1+number2==result;
            case 2:
                return number1-number2==result;
            case 3:
                return number1*number2==result;
            case 4:
            default:
                return number1/number2==result;
        }
    }

    private void playTrueSound()
    {

    }

    private void playFalseSound()
    {

    }

    @Override
    public void finish()
    {
        getModel().setGameStatus(GameStatus.FINISH);
    }

    @Override
    public OperationGameScene getScene()
    {
        return (OperationGameScene) engine.getScene();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return OperationGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new OperationGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new OperationGameRF();
    }

    @Override
    public OperationGameActivity getActivity()
    {
        return (OperationGameActivity) activity;
    }

    @Override
    public OperationGameModel getModel()
    {
        return (OperationGameModel)model;
    }

    @Override
    public OperationGameRF getRequestFactory()
    {
        return (OperationGameRF) requestFactory;
    }
}
