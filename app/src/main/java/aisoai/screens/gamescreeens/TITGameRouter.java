package aisoai.screens.gamescreeens;

import aisoai.R;
import aisoai.screens.gamescreeens.auditiongame.AuditionGameControl;
import aisoai.screens.gamescreeens.choicemodgame.ChoiceModGameControl;
import aisoai.screens.gamescreeens.circlegame.CircleGameControl;
import aisoai.screens.gamescreeens.collectfruitgame.CollectFruitGameControl;
import aisoai.screens.gamescreeens.dinosaurjumpgame.DinosaurJumpGameControl;
import aisoai.screens.gamescreeens.doublepokemongame.DoublePokemonGameControl;
import aisoai.screens.gamescreeens.findpokemongame.FindPokemonGameControl;
import aisoai.screens.gamescreeens.flappydoraemon.FlappyDoraemonControl;
import aisoai.screens.gamescreeens.operationgame.OperationGameControl;
import aisoai.screens.gamescreeens.pianogame.PianoGameControl;
import aisoai.screens.gamescreeens.snakehuntinggame.SnakeHuntingControl;
import aisoai.screens.gamescreeens.sortrecgame.SortRecGameControl;
import aisoai.screens.gamescreeens.spiderwebgame.SpiderWebControl;
import aisoai.screens.gamescreeens.tetrisgame.TetrisGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.zombiegame.ZombieGameControl;

public class TITGameRouter
{
    public static TITGameControl routerGameControl(int gameId)
    {
        switch (gameId)
        {
            case 1:
                return new CircleGameControl();
            case 2:
                return new ZombieGameControl();
            case 3:
                return new AuditionGameControl();
            case 4:
                return new OperationGameControl();
            case 5:
                return new FindPokemonGameControl();
            case 6:
                return new ChoiceModGameControl();
            case 7:
                return new CollectFruitGameControl();
            case 8:
                return new PianoGameControl();
            case 9:
                return new DoublePokemonGameControl();
            case 10:
                return new FlappyDoraemonControl();
            case 11:
                return new DinosaurJumpGameControl();
            case 12:
                return new SortRecGameControl();
            case 13:
                return new SpiderWebControl();
            case 14:
                return new SnakeHuntingControl();
            case 15:
                return new TetrisGameControl();
            default:
                return new CircleGameControl();
        }
    }

    public static int routerGameIcon(int gameId)
    {
        switch (gameId)
        {
            case 1:
                return R.drawable.circle_game_icon;
            case 2:
                return R.drawable.zombie_game_icon;
            case 3:
                return R.drawable.audition_game_icon;
            case 4:
                return R.drawable.operation_game_icon;
            case 5:
                return R.drawable.find_pokemon_game_icon;
            case 6:
                return R.drawable.choice_mod_game_icon;
            case 7:
                return R.drawable.collect_fruit_game_icon;
            case 8:
                return R.drawable.piano_game_icon;
            case 9:
                return R.drawable.double_pokemon_game_icon;
            case 10:
                return R.drawable.flappy_doraemon_game_icon;
            case 11:
                return R.drawable.dinosaur_jump_game_icon;
            case 12:
                return R.drawable.sort_rec_game_icon;
            case 13:
                return R.drawable.spider_web_game_icon;
            case 14:
                return R.drawable.snake_hunting_game_icon;
            case 15:
                return R.drawable.tetris_game_icon;
            default:
                return R.drawable.default_game_icon;
        }
    }
}
