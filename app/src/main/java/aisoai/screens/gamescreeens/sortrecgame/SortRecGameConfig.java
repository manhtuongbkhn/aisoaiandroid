package aisoai.screens.gamescreeens.sortrecgame;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class SortRecGameConfig
{
    public static float REC_BUTTON_WIDTH()
    {
        return 50*TITGameConfig.GS_WIDTH()/320;
    }

    public static float REC_BUTTON_HEIGHT()
    {
        return REC_BUTTON_WIDTH();
    }

    public static float REC_BUTTON_X()
    {
        return 50*TITGameConfig.GS_WIDTH()/320;
    }

    public static float REC_BUTTON_Y()
    {
        return 50*TITGameConfig.GS_WIDTH()/320;
    }

    public static float RECBUTTON_X(int column)
    {
        return (40+(column-1)*60)*TITGameConfig.GS_WIDTH()/320-REC_BUTTON_WIDTH()/2;
    }

    public static float RECBUTTON_Y(int row)
    {
        return (45+(row-1)*60)*TITGameConfig.GS_HEIGHT()/390-REC_BUTTON_HEIGHT()/2;
    }

    public static int SHOWING_REC_TIME_MILLIS=250;
}
