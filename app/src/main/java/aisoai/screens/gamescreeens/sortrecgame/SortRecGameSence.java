package aisoai.screens.gamescreeens.sortrecgame;

import org.andengine.entity.IEntity;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class SortRecGameSence extends TITScene
{
    private ArrayList<RecButton> recButtonArr=new ArrayList<RecButton>();

    @Override
    protected SortRecGameControl getControl()
    {
        return (SortRecGameControl) control;
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        if(iEntity instanceof RecButton)
        {
            RecButton iRecButton=(RecButton) iEntity;
            recButtonArr.add(iRecButton);
        }

        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        if(iEntity instanceof RecButton)
        {
            RecButton iRecButton=(RecButton) iEntity;
            recButtonArr.remove(iRecButton);
        }
        return super.detachChild(iEntity);
    }

    public void clear()
    {
        for(int i=0;i<recButtonArr.size();i++)
        {
            recButtonArr.get(i).setType(2);
        }
    }

    public RecButton getRecButton(int index)
    {
        return recButtonArr.get(index);
    }
}
