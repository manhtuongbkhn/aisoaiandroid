package aisoai.screens.gamescreeens.sortrecgame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import aisoai.screens.gamescreeens.titentities.TITAnimatedSprite;
import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class RecButton extends TITAnimatedSprite
{
    private int row;
    private int colum;

    public RecButton(float x,float y,float width,float height,TiledTextureRegion tiledTextureRegion,
                                                                        TITGameControl iControl)
    {
        super(x, y, width, height,tiledTextureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            getControl().recButtonTouchEvent(RecButton.this);
        }
    }

    @Override
    public SortRecGameControl getControl()
    {
        return (SortRecGameControl) control;
    }

    public static RecButton createRecButton(int row,int colum,TITGameControl iControl)
    {
        float x,y,width,height;
        TiledTextureRegion tiledTextureRegion=SortRecGameResource.getRec();
        width=SortRecGameConfig.REC_BUTTON_WIDTH();
        height=SortRecGameConfig.REC_BUTTON_HEIGHT();
        x=SortRecGameConfig.RECBUTTON_X(colum);
        y=SortRecGameConfig.RECBUTTON_Y(row);
        RecButton recButton=new RecButton(x,y,width,height,tiledTextureRegion,iControl);
        recButton.setType(2);
        recButton.setRow(row);
        recButton.setColum(colum);
        return recButton;
    }

    public int getRow()
    {
        return row;
    }

    public void setRow(int row)
    {
        this.row = row;
    }

    public int getColum()
    {
        return colum;
    }

    public void setColum(int colum)
    {
        this.colum = colum;
    }

    public int getType()
    {
        return getCurrentTileIndex();
    }

    public void setType(int type)
    {
        setCurrentTileIndex(type);
    }

    public int getIndex()
    {
        return SortRecGameFunction.getIndex(row,colum);
    }
}
