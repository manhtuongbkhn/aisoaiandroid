package aisoai.screens.gamescreeens.sortrecgame;

public class SortRecGameFunction
{
    public static int getIndex(int row,int colum)
    {
        return (row-1)*5+(colum-1);
    }

    public static int getRow(int index)
    {
        return (index/5)+1;
    }

    public static int getColum(int index)
    {
        return index%5+1;
    }
}
