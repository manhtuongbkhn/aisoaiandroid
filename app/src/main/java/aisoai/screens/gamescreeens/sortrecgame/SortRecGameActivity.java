package aisoai.screens.gamescreeens.sortrecgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class SortRecGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getSortRecGameControl();
    }

    @Override
    public SortRecGameControl getControl()
    {
        return (SortRecGameControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new SortRecGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new SortRecGameSence();
    }
}
