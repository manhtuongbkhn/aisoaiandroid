package aisoai.screens.gamescreeens.sortrecgame;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class SortRecGameModel extends TITGameModel
{
    private GameStatus gameStatus;

    private int currentQuestionIndex;
    private int currentType;
    private ArrayList<Integer> questionArr;
    private ArrayList<Integer> answerArr;

    public GameStatus getGameStatus()
    {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus)
    {
        this.gameStatus = gameStatus;
    }

    public int getCurrentQuestionIndex()
    {
        return currentQuestionIndex;
    }

    public void setCurrentQuestionIndex(int currentQuestionIndex)
    {
        this.currentQuestionIndex = currentQuestionIndex;
    }

    public int getCurrentType()
    {
        return currentType;
    }

    public void setCurrentType(int currentType)
    {
        this.currentType = currentType;
    }

    public ArrayList<Integer> getAnswerArr()
    {
        return answerArr;
    }

    public void setAnswerArr(ArrayList<Integer> answerArr) {
        this.answerArr = answerArr;
    }

    public ArrayList<Integer> getQuestionArr()
    {
        return questionArr;
    }

    public void setQuestionArr(ArrayList<Integer> questionArr)
    {
        this.questionArr = questionArr;
    }
}
