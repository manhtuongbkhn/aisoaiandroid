package aisoai.screens.gamescreeens.sortrecgame;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.andengine.opengl.texture.region.TextureRegion;

import java.util.ArrayList;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;
import aisoai.titapplib.TITFunction;

public class SortRecGameControl extends TITGameControl
{
    @Override
    protected TextureRegion getBackground()
    {
        return SortRecGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        for(int i=0;i<30;i++)
        {
            int row=SortRecGameFunction.getRow(i);
            int colum=SortRecGameFunction.getColum(i);
            RecButton recButton=RecButton.createRecButton(row,colum,this);
            getScene().attachChild(recButton);
            getScene().registerTouchArea(recButton);
        }
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                getModel().setGameStatus(GameStatus.SLEEPING);
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;
            case "createNewQuestion":
                getModel().setGameStatus(GameStatus.SHOWING);
                int questionIndex=token.get(KJS.INDEX).getAsInt();
                int type=token.get(KJS.PARAM1).getAsInt();
                JsonArray jsonArray=token.get(KJS.PARAM2).getAsJsonArray();
                ArrayList<Integer> questionArr= TITFunction.covertToIntArrayList(jsonArray);
                showQuestion(questionIndex,type,questionArr);
                gameScriptReader.stop();
                getModel().setGameStatus(GameStatus.PLAYING);
                break;
        }
    }

    private void showQuestion(int questionIndex,int type,ArrayList<Integer> questionArr)
    {
        getModel().setCurrentQuestionIndex(questionIndex);
        getModel().setCurrentType(type);
        getModel().setQuestionArr(questionArr);
        getModel().setAnswerArr(new ArrayList<Integer>());

        for(int i=0;i<questionArr.size();i++)
        {
            int index=questionArr.get(i);
            getScene().getRecButton(index).setType(type);
            try {Thread.sleep(SortRecGameConfig.SHOWING_REC_TIME_MILLIS);}
            catch (InterruptedException e) {}
        }
    }

    public void recButtonTouchEvent(RecButton recButton)
    {
        switch (getModel().getGameStatus())
        {
            case PLAYING:
                recButton.setType(2);
                getModel().getAnswerArr().add(recButton.getIndex());
                checkAnswer();
                break;
            default:
                break;
        }
    }

    private void checkAnswer()
    {
        int answerLastPostion=getModel().getAnswerArr().size()-1;
        int answerLastIndex=getModel().getAnswerArr().get(answerLastPostion);
        if(getModel().getCurrentType()==0)
        {
            int questionIndex=getModel().getQuestionArr().get(answerLastPostion);
            if(answerLastIndex==questionIndex)
            {
                if(getModel().getAnswerArr().size()==getModel().getQuestionArr().size())
                    trueAnswer();
            }
            else
            {
                falseAnswer();
            }
        }

        if(getModel().getCurrentType()==1)
        {
            int questionSize=getModel().getQuestionArr().size();
            int questionIndex=getModel().getQuestionArr().get(questionSize-answerLastPostion-1);
            if(answerLastIndex==questionIndex)
            {
                if(getModel().getAnswerArr().size()==getModel().getQuestionArr().size())
                    trueAnswer();
            }
            else
            {
                falseAnswer();
            }
        }
    }

    private void trueAnswer()
    {
        //playTrueSound();
        getScene().clear();
        getRequestFactory().sendGameAnswer(getModel().getCurrentQuestionIndex(),
                                                                        getModel().getAnswerArr());
        try {Thread.sleep(500);} catch (InterruptedException e) {}
        gameScriptReader.resume();
    }

    private void falseAnswer()
    {
        //playFalseSound();
        getScene().clear();
        getRequestFactory().sendGameAnswer(getModel().getCurrentQuestionIndex(),
                                                                        getModel().getAnswerArr());
        try {Thread.sleep(500);} catch (InterruptedException e) {}
        gameScriptReader.resume();
    }

    @Override
    public void finish()
    {

    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return SortRecGameActivity.class;
    }

    @Override
    public SortRecGameSence getScene()
    {
        return (SortRecGameSence) engine.getScene();
    }

    @Override
    protected TITGameModel initModel()
    {
        return new SortRecGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new SortRecGameRF();
    }

    @Override
    public SortRecGameActivity getActivity()
    {
        return (SortRecGameActivity) activity;
    }

    @Override
    public SortRecGameModel getModel()
    {
        return (SortRecGameModel) model;
    }

    @Override
    public SortRecGameRF getRequestFactory()
    {
        return (SortRecGameRF) requestFactory;
    }
}
