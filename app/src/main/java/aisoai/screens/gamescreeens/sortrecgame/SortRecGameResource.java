package aisoai.screens.gamescreeens.sortrecgame;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class SortRecGameResource extends TITGameResource
{
    private static TiledTextureRegion rec;
    private static TextureRegion background;

    public SortRecGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        rec=createTiledTextureRegionFromAsset("sortrecgame/rec.png",3,1,4);
        background=createTextureRegionFromAsset("sortrecgame/background.png",3);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    @Override
    protected void clearResource()
    {
        rec=null;
        background=null;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public static TiledTextureRegion getRec()
    {
        return rec;
    }
}
