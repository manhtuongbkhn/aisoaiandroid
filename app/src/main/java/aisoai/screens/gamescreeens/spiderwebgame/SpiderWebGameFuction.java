package aisoai.screens.gamescreeens.spiderwebgame;

public class SpiderWebGameFuction
{
    public static int getHorLineIndex(int horLineRow,int horLineColumn)
    {
        return (horLineRow-1)*5+(horLineColumn-1);
    }

    public static int getHorLineRow(int horLineIndex)
    {
        return (horLineIndex/5)+1;
    }

    public static int getHorLineColum(int horLineIndex)
    {
        return horLineIndex%5+1;
    }

    public static int getVerLineIndex(int verLineRow, int verLineColumn)
    {
        return (verLineRow-1)*4+(verLineColumn-1);
    }

    public static int getVerLineRow(int verLineIndex)
    {
        return (verLineIndex/4)+1;
    }

    public static int getVerLineColum(int verLineIndex)
    {
        return verLineIndex%4+1;
    }

    public static int getPointIndex(int pointRow,int pointColumn)
    {
        return pointColumn-1+(pointRow-1)*5;
    }

    public static int getPointRow(int pointIndex)
    {
        return pointIndex/5+1;
    }

    public static int getPointColumn(int pointIndex)
    {
        return pointIndex%5+1;
    }
}
