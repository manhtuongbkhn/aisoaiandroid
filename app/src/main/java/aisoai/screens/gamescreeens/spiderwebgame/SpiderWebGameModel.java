package aisoai.screens.gamescreeens.spiderwebgame;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class SpiderWebGameModel extends TITGameModel
{
    private GameStatus gameStatus;

    private int currentQuestionIndex;
    private int currentPostion =0;
    private ArrayList<Integer> currentPointIndexArr;

    public GameStatus getGameStatus()
    {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus)
    {
        this.gameStatus = gameStatus;
    }

    public int getCurrentQuestionIndex()
    {
        return currentQuestionIndex;
    }

    public void setCurrentQuestionIndex(int currentQuestionIndex)
    {
        this.currentQuestionIndex = currentQuestionIndex;
    }

    public int getCurrentPostion()
    {
        return currentPostion;
    }

    public void setCurrentPostion(int currentPostion)
    {
        this.currentPostion = currentPostion;
    }

    public ArrayList<Integer> getCurrentPointIndexArr()
    {
        return currentPointIndexArr;
    }

    public void setCurrentPointIndexArr(ArrayList<Integer> currentPointIndexArr)
    {
        this.currentPointIndexArr = currentPointIndexArr;
    }

}
