package aisoai.screens.gamescreeens.spiderwebgame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class AnswerButton extends TITSprite
{
    private int column;

    public AnswerButton(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            getControl().answerButtonTouchEvent(AnswerButton.this);
        }
    }

    @Override
    public SpiderWebControl getControl()
    {
        return (SpiderWebControl) control;
    }

    public static AnswerButton createAnswerButton(int column,TITGameControl iControl)
    {
        float x,y,width,height;
        width=SpiderWebGameConfig.ANSWER_BUTTON_WIDTH();
        height=SpiderWebGameConfig.ANSWER_BUTTON_HEIGHT();
        x=SpiderWebGameConfig.ANSWER_BUTTON_X(column);
        y=SpiderWebGameConfig.ANSWER_BUTTON_Y();
        TextureRegion textureRegion;
        switch (column)
        {
            case 1:
                textureRegion=SpiderWebGameResource.getButton1();
                break;
            case 2:
                textureRegion=SpiderWebGameResource.getButton2();
                break;
            case 3:
                textureRegion=SpiderWebGameResource.getButton3();
                break;
            case 4:
                textureRegion=SpiderWebGameResource.getButton4();
                break;
            case 5:
            default:
                textureRegion=SpiderWebGameResource.getButton5();
                break;
        }
        AnswerButton answerButton=new AnswerButton(x,y,width,height,textureRegion,iControl);
        answerButton.setColumn(column);
        return answerButton;
    }

    public int getColumn()
    {
        return column;
    }

    public void setColumn(int column)
    {
        this.column = column;
    }
}
