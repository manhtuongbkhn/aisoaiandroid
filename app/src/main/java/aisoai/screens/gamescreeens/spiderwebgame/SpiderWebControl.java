package aisoai.screens.gamescreeens.spiderwebgame;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;
import aisoai.titapplib.TITFunction;

public class SpiderWebControl extends TITGameControl
{
    @Override
    protected TextureRegion getBackground()
    {
        return SpiderWebGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        for(int horLineIndex=0;horLineIndex<45;horLineIndex++)
        {
            int row=SpiderWebGameFuction.getHorLineRow(horLineIndex);
            int column=SpiderWebGameFuction.getHorLineColum(horLineIndex);

            HorizontalLine horizontalLine=HorizontalLine.createHorizontalLine(row, column, this);
            getScene().attachChild(horizontalLine);
        }


        for(int verLineIndex=4;verLineIndex<36;verLineIndex++)
        {
            int verLineRow=SpiderWebGameFuction.getVerLineRow(verLineIndex);
            int verLineColumn=SpiderWebGameFuction.getVerLineColum(verLineIndex);
            VerticalLine verticalLine=VerticalLine.createVerticalLine(verLineRow,verLineColumn,this);
            getScene().attachChild(verticalLine);
        }

        for(int culumn=1;culumn<=5;culumn++)
        {
            AnswerButton answerButton = AnswerButton.createAnswerButton(culumn,this);
            getScene().attachChild(answerButton);
            getScene().registerTouchArea(answerButton);
        }

        Spider spider=Spider.createSpider(3,this);
        getScene().attachChild(spider);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                getModel().setGameStatus(GameStatus.READY);
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;
            case "createNewQuestion":
                int questionIndex=token.get(KJS.INDEX).getAsInt();
                int startPointColumn=token.get(KJS.PARAM1).getAsInt();
                JsonArray verLineIndexJsonArray=token.get(KJS.PARAM2).getAsJsonArray();
                JsonArray pointIndexJsonArray=token.get(KJS.PARAM3).getAsJsonArray();
                ArrayList<Integer> verLineIndexArr=
                                        TITFunction.covertToIntArrayList(verLineIndexJsonArray);
                ArrayList<Integer> pointIndexArr=
                                            TITFunction.covertToIntArrayList(pointIndexJsonArray);
                showQuestion(questionIndex,startPointColumn,verLineIndexArr,pointIndexArr);
                gameScriptReader.stop();
                break;
        }
    }

    private void showQuestion(int questionIndex,int startPointColumn,
                        ArrayList<Integer> verLineIndexJsonArray,ArrayList<Integer> pointIndexArr)
    {
        //System.out.println("-Point Index Arr:"+pointIndexArr.toString());
        getModel().setGameStatus(GameStatus.SHOWING);
        getModel().setCurrentQuestionIndex(questionIndex);
        getModel().setCurrentPointIndexArr(pointIndexArr);
        getModel().setCurrentPostion(0);
        getScene().resetLine();
        getScene().getSpider().reset(startPointColumn);

        for(int i=0;i<verLineIndexJsonArray.size();i++)
        {
            int verLineIndex = verLineIndexJsonArray.get(i);
            VerticalLine verticalLine=getScene().getVerLineByIndex(verLineIndex);
            verticalLine.enble();
        }
        getModel().setGameStatus(GameStatus.PLAYING);
    }

    private void showAnswer()
    {
        getModel().setGameStatus(GameStatus.SLEEPING);
        ArrayList<Integer> pointIndexArr=getModel().getCurrentPointIndexArr();
        for(int i=0;i<pointIndexArr.size()-1;i++)
        {
            int currentPointIndex=pointIndexArr.get(i);
            int currentPointRow=SpiderWebGameFuction.getPointRow(currentPointIndex);
            int currentPointColumn=SpiderWebGameFuction.getPointColumn(currentPointIndex);

            int nextPointIndex=pointIndexArr.get(i + 1);
            int nextPointRow=SpiderWebGameFuction.getPointRow(nextPointIndex);
            int nextPointColumn=SpiderWebGameFuction.getPointColumn(nextPointIndex);

            SpiderWebGameLine spiderWebGameLine;
            //System.out.println("-Current Point Row:"+currentPointRow);
            //System.out.println("-Current Point Culmn:"+currentPointColumn);

            //System.out.println("-Next Point Row:"+nextPointRow);
            //System.out.println("-Next Point Culmn:"+nextPointColumn);
            if(nextPointRow>currentPointRow)
            {
                spiderWebGameLine=getScene().getLine(currentPointRow,currentPointColumn,2);
            }
            else
            {
                if(nextPointColumn>currentPointColumn)
                {
                    spiderWebGameLine=getScene().getLine(currentPointRow,currentPointColumn,1);
                }
                else
                {
                    spiderWebGameLine=getScene().getLine(currentPointRow,currentPointColumn,3);
                }
            }

            spiderWebGameLine.setColor(Color.PINK);
        }
    }

    private void runSpider()
    {
        Spider spider=getScene().getSpider();
        int currentPostion=getModel().getCurrentPostion();
        int toPointIndex=getModel().getCurrentPointIndexArr().get(currentPostion + 1);
        int row=SpiderWebGameFuction.getPointRow(toPointIndex);
        int column=SpiderWebGameFuction.getPointColumn(toPointIndex);
        float toX=SpiderWebGameConfig.SPIDER_X(column);
        float toY=SpiderWebGameConfig.SPIDER_Y(row);

        MoveModifier moveModifier=new MoveModifier(0.1f,spider.getX(),toX,spider.getY(),toY);
        IModifier.IModifierListener<IEntity> modifierListener=
                new IModifier.IModifierListener<IEntity>()
                {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity){}

                    @Override
                    public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
                    {
                        int currentPostion=getModel().getCurrentPostion();
                        getModel().setCurrentPostion(currentPostion + 1);
                        if(getModel().getCurrentPostion()==
                                                    getModel().getCurrentPointIndexArr().size()-1)
                            gameScriptReader.resume();
                        else
                            runSpider();
                    }
                };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        spider.registerEntityModifier(moveModifier);
    }

    public void answerButtonTouchEvent(AnswerButton answerButton)
    {
        switch (getModel().getGameStatus())
        {
            case PLAYING:
                int answerColumn=answerButton.getColumn();
                getRequestFactory().sendGameAnswer
                                                (getModel().getCurrentQuestionIndex(),answerColumn);
                int pointIndex=getModel().getCurrentPointIndexArr().
                                                get(getModel().getCurrentPointIndexArr().size()-1);
                int pointColumn=SpiderWebGameFuction.getPointColumn(pointIndex);
                if(answerColumn==pointColumn)
                {
                    playTrueSound();
                }
                else
                {
                    playFalseSound();
                }

                showAnswer();
                runSpider();
                break;
            default:
                break;
        }
    }

    private void playTrueSound()
    {

    }

    private void playFalseSound()
    {

    }

    @Override
    public void finish()
    {
        getModel().setGameStatus(GameStatus.FINISH);
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return SpiderWebGameActivity.class;
    }

    @Override
    public SpiderWebGameSence getScene()
    {
        return (SpiderWebGameSence) engine.getScene();
    }

    @Override
    protected TITGameModel initModel()
    {
        return new SpiderWebGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new SpiderWebGameRF();
    }

    @Override
    public SpiderWebGameActivity getActivity()
    {
        return (SpiderWebGameActivity) activity;
    }

    @Override
    public SpiderWebGameModel getModel()
    {
        return (SpiderWebGameModel) model;
    }

    @Override
    public SpiderWebGameRF getRequestFactory()
    {
        return (SpiderWebGameRF) requestFactory;
    }
}
