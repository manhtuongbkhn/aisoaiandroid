package aisoai.screens.gamescreeens.spiderwebgame;

import org.andengine.util.color.Color;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITLine;

public class HorizontalLine extends SpiderWebGameLine
{
    public HorizontalLine(float x1, float y1, float x2, float y2, TITGameControl iControl)
    {
        super(x1, y1, x2, y2, iControl);
    }

    @Override
    protected SpiderWebControl getControl()
    {
        return (SpiderWebControl) control;
    }

    public static HorizontalLine createHorizontalLine(int row,int column,TITGameControl iControl)
    {
        float x1,y1,x2,y2;
        y1=SpiderWebGameConfig.HORIZONTALLINE_Y1(row);
        y2=SpiderWebGameConfig.HORIZONTALLINE_Y2(row);
        x1=SpiderWebGameConfig.HORIZONTALLINE_X(column);
        x2=x1;
        HorizontalLine horizontalLine=new HorizontalLine(x1,y1,x2,y2,iControl);
        horizontalLine.setColor(Color.GREEN);
        horizontalLine.setLineWidth(SpiderWebGameConfig.LINE_WIDTH());
        return horizontalLine;
    }
}
