package aisoai.screens.gamescreeens.spiderwebgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class SpiderWebGameResource extends TITGameResource
{
    private static TextureRegion spider;
    private static TextureRegion button1;
    private static TextureRegion button2;
    private static TextureRegion button3;
    private static TextureRegion button4;
    private static TextureRegion button5;
    private static TextureRegion background;

    public SpiderWebGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        spider=createTextureRegionFromAsset("spiderwebgame/spider.png", 2);
        button1=createTextureRegionFromAsset("spiderwebgame/button1.png",2);
        button2=createTextureRegionFromAsset("spiderwebgame/button2.png",2);
        button3=createTextureRegionFromAsset("spiderwebgame/button3.png",2);
        button4=createTextureRegionFromAsset("spiderwebgame/button4.png",2);
        button5=createTextureRegionFromAsset("spiderwebgame/button5.png",2);
        background=createTextureRegionFromAsset("spiderwebgame/background.png",3);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    public static TextureRegion getSpider()
    {
        return spider;
    }

    public static TextureRegion getButton1()
    {
        return button1;
    }

    public static TextureRegion getButton2()
    {
        return button2;
    }

    public static TextureRegion getButton3()
    {
        return button3;
    }

    public static TextureRegion getButton4()
    {
        return button4;
    }

    public static TextureRegion getButton5()
    {
        return button5;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    @Override
    protected void clearResource()
    {
        spider=null;
        button1=null;
        button2=null;
        button3=null;
        button4=null;
        button5=null;
        background=null;
    }
}
