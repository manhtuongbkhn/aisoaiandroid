package aisoai.screens.gamescreeens.spiderwebgame;
import org.andengine.entity.IEntity;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class SpiderWebGameSence extends TITScene
{
    private Spider spider;
    private ArrayList<HorizontalLine> horizontalLineArr=new ArrayList<HorizontalLine>();
    private ArrayList<VerticalLine> verticalLineArr=new ArrayList<VerticalLine>();

    @Override
    protected SpiderWebControl getControl()
    {
        return (SpiderWebControl) control;
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        if(iEntity instanceof HorizontalLine)
        {
            HorizontalLine iHorizontalLine=(HorizontalLine) iEntity;
            horizontalLineArr.add(iHorizontalLine);
        }

        if(iEntity instanceof VerticalLine)
        {
            VerticalLine iVerticalLine=(VerticalLine) iEntity;
            verticalLineArr.add(iVerticalLine);
        }

        if(iEntity instanceof Spider)
        {
            Spider iSpider=(Spider) iEntity;
            this.spider=iSpider;
        }

        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        if(iEntity instanceof HorizontalLine)
        {
            HorizontalLine iHorizontalLine=(HorizontalLine) iEntity;
            horizontalLineArr.remove(iHorizontalLine);
        }

        if(iEntity instanceof VerticalLine)
        {
            VerticalLine iVerticalLine=(VerticalLine) iEntity;
            verticalLineArr.remove(iVerticalLine);
        }

        if(iEntity instanceof Spider)
        {
            Spider iSpider=(Spider) iEntity;
            this.spider=null;
        }

        return super.detachChild(iEntity);
    }

    public void resetLine()
    {
        for(int i=0;i<horizontalLineArr.size();i++)
        {
            HorizontalLine horizontalLine=horizontalLineArr.get(i);
            horizontalLine.setColor(Color.GREEN);
        }

        for(int i=0;i<verticalLineArr.size();i++)
        {
            VerticalLine verticalLine=verticalLineArr.get(i);
            verticalLine.setColor(Color.GREEN);
            verticalLine.disable();
        }
    }

    public SpiderWebGameLine getLine(int pointRow,int pointColumn,int outDirection)
    {
        /*
            1:right
            2:bot
            3:left
            4:top
         */
        int verLineIndex;
        int horLineIndex;
        switch (outDirection)
        {
            case 1:
                verLineIndex=SpiderWebGameFuction.getVerLineIndex(pointRow,pointColumn);
                return getVerLineByIndex(verLineIndex);
            case 2:
                horLineIndex=SpiderWebGameFuction.getHorLineIndex(pointRow,pointColumn);
                return horizontalLineArr.get(horLineIndex);
            case 3:
                verLineIndex=SpiderWebGameFuction.getVerLineIndex(pointRow,pointColumn-1);
                return getVerLineByIndex(verLineIndex);
            case 4:
            default:
                return null;
        }
    }

    public VerticalLine getVerLineByIndex(int verLineIndex)
    {
        return verticalLineArr.get(verLineIndex-4);
    }

    public Spider getSpider()
    {
        return spider;
    }
}
