package aisoai.screens.gamescreeens.spiderwebgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class Spider extends TITSprite
{

    public Spider(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public SpiderWebControl getControl()
    {
        return (SpiderWebControl) control;
    }

    public static Spider createSpider(int startColumn,TITGameControl iControl)
    {
        float x,y,width,height;
        width=SpiderWebGameConfig.SPIDER_WIDTH();
        height=SpiderWebGameConfig.SPIDER_HEIGHT();
        x=SpiderWebGameConfig.SPIDER_X(startColumn);
        y=SpiderWebGameConfig.SPIDER_Y(1);
        TextureRegion textureRegion=SpiderWebGameResource.getSpider();
        Spider spider=new Spider(x,y,width,height,textureRegion,iControl);
        return spider;
    }

    public void reset(int column)
    {
        setX(SpiderWebGameConfig.SPIDER_X(column));
        setY(SpiderWebGameConfig.SPIDER_Y(1));
    }
}
