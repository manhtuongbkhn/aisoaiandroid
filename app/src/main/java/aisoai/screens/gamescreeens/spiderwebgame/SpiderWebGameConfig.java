package aisoai.screens.gamescreeens.spiderwebgame;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class SpiderWebGameConfig
{
    public static float LINE_WIDTH()
    {
        return 4*TITGameConfig.GS_WIDTH()/320;
    }

    public static float HORIZONTALLINE_Y1(int horLineRow)
    {
        return (30+(horLineRow-1)*30)*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float HORIZONTALLINE_Y2(int howLineRow)
    {
        return (30+howLineRow*30)*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float HORIZONTALLINE_X(int horLineColumn)
    {
        return (40+(horLineColumn-1)*60)*TITGameConfig.GS_WIDTH()/320f;
    }

    public static float VERTICALLINE_Y(int verLineRow)
    {
        return (30+(verLineRow-1)*30)*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float VERTICALLINE_X1(int verLineColumn)
    {
        return (40+(verLineColumn-1)*60)*TITGameConfig.GS_WIDTH()/320f;
    }

    public static float VERTICALLINE_DISABLE_Y()
    {
        return (-60)*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float VERTICALLINE_X2(int verLineColumn)
    {
        return (40+verLineColumn*60)*TITGameConfig.GS_WIDTH()/320f;
    }

    public static float ANSWER_BUTTON_WIDTH()
    {
        return 60*TITGameConfig.GS_WIDTH()/320;
    }

    public static float ANSWER_BUTTON_HEIGHT()
    {
        return ANSWER_BUTTON_WIDTH();
    }

    public static float ANSWER_BUTTON_X(int buttonColumn)
    {
        return (40+(buttonColumn-1)*60)*TITGameConfig.GS_WIDTH()/320f-ANSWER_BUTTON_WIDTH()/2;
    }

    public static float ANSWER_BUTTON_Y()
    {
        return 345*TITGameConfig.GS_HEIGHT()/390-ANSWER_BUTTON_HEIGHT()/2;
    }

    public static float SPIDER_WIDTH()
    {
        return 40*TITGameConfig.GS_WIDTH()/320;
    }

    public static float SPIDER_HEIGHT()
    {
        return SPIDER_WIDTH();
    }

    public static float SPIDER_X(int column)
    {
        return (40+(column-1)*60)*TITGameConfig.GS_WIDTH()/320f-SPIDER_WIDTH()/2;
    }

    public static float SPIDER_Y(int row)
    {
        return (30+(row-1)*30)*TITGameConfig.GS_HEIGHT()/390-SPIDER_HEIGHT()/2;
    }
}
