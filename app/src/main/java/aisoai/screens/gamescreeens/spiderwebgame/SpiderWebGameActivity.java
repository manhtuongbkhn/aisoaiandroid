package aisoai.screens.gamescreeens.spiderwebgame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class SpiderWebGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getSpiderWebControl();
    }

    @Override
    public SpiderWebControl getControl()
    {
        return (SpiderWebControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new SpiderWebGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new SpiderWebGameSence();
    }
}
