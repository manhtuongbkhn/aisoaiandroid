package aisoai.screens.gamescreeens.spiderwebgame;

import org.andengine.util.color.Color;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITLine;

public class VerticalLine extends SpiderWebGameLine
{
    private int row;
    private int column;

    public VerticalLine(float x1, float y1, float x2, float y2, TITGameControl iControl)
    {
        super(x1, y1, x2, y2, iControl);
    }

    @Override
    protected SpiderWebControl getControl()
    {
        return (SpiderWebControl) control;
    }

    public static VerticalLine createVerticalLine(int row,int column,TITGameControl iControl)
    {
        float x1,y1,x2,y2;
        x1=SpiderWebGameConfig.VERTICALLINE_X1(column);
        x2=SpiderWebGameConfig.VERTICALLINE_X2(column);
        y1=SpiderWebGameConfig.VERTICALLINE_DISABLE_Y();
        y2=y1;
        VerticalLine verticalLine=new VerticalLine(x1,y1,x2,y2,iControl);
        verticalLine.setColor(Color.GREEN);
        verticalLine.setLineWidth(SpiderWebGameConfig.LINE_WIDTH());
        verticalLine.setRow(row);
        verticalLine.setColumn(column);
        return verticalLine;
    }

    public int getRow()
    {
        return row;
    }

    public void setRow(int row)
    {
        this.row = row;
    }

    public int getColumn()
    {
        return column;
    }

    public void setColumn(int column)
    {
        this.column = column;
    }

    public void disable()
    {
        setY(SpiderWebGameConfig.VERTICALLINE_DISABLE_Y());
    }

    public void enble()
    {
        setY(SpiderWebGameConfig.VERTICALLINE_Y(row));
    }
}
