package aisoai.screens.gamescreeens.spiderwebgame;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITLine;

abstract public class SpiderWebGameLine extends TITLine
{

    public SpiderWebGameLine(float x1, float y1, float x2, float y2, TITGameControl iControl)
    {
        super(x1, y1, x2, y2, iControl);
    }
}
