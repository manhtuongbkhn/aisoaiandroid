package aisoai.screens.gamescreeens.spiderwebgame;

import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

import java.util.ArrayList;

import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.titapplib.TITFunction;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;

public class SpiderWebGameRF extends TITRequestFactory
{
    public void sendGameAnswer(int index,int answerColumn)
    {
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.INDEX,index);
        toServerData.putInt(KJS.PARAM1,answerColumn);
        ExtensionRequest request=new ExtensionRequest(CMDRQ.GAMEANSWER_RQ,toServerData,room);
        sendRequest(request);
    }
}
