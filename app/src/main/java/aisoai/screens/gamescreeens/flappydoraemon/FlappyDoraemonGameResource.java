package aisoai.screens.gamescreeens.flappydoraemon;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class FlappyDoraemonGameResource extends TITGameResource
{
    private static TextureRegion doraemon;
    private static TextureRegion tube;
    private static TextureRegion background;
    private static TextureRegion playButton;
    private static TextureRegion replayButton;
    private static TextureRegion transparent;

    public FlappyDoraemonGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        doraemon=createTextureRegionFromAsset("flappydoraemongame/doraemon.png",2);
        tube=createTextureRegionFromAsset("flappydoraemongame/tube.png",3);
        background =createTextureRegionFromAsset("flappydoraemongame/background.png",4);
        playButton =createTextureRegionFromAsset("play.png",2);
        replayButton =createTextureRegionFromAsset("replay.png",2);
        transparent=createTextureRegionFromAsset("transparent.png",1);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    public static TextureRegion getDoraemon()
    {
        return doraemon;
    }

    public static TextureRegion getTube()
    {
        return tube;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public static TextureRegion getPlayButton()
    {
        return playButton;
    }

    public static TextureRegion getReplayButton()
    {
        return replayButton;
    }

    public static TextureRegion getTransparent()
    {
        return transparent;
    }

    @Override
    protected void clearResource()
    {
        doraemon=null;
        tube=null;
        background=null;
        playButton=null;
        replayButton=null;
        transparent=null;
    }

}
