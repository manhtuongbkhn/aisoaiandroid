package aisoai.screens.gamescreeens.flappydoraemon;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class Tube extends TITSprite
{
    private int type;
    private int index;
    public Tube(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public FlappyDoraemonControl getControl()
    {
        return (FlappyDoraemonControl) control;
    }

    public static Tube createTube(TITGameControl iControl,int type,float heightScale,int index)
    {
        float x,y,width,height;
        TextureRegion textureRegion=FlappyDoraemonGameResource.getTube();
        width=FlappyDoraemonConfig.DORAEMON_WIDTH();
        x=FlappyDoraemonConfig.TUBE_X();
        if(type==1)
        {
            y=0;
            height=heightScale* TITGameConfig.GS_HEIGHT();
        }
        else
        {
            y=(1-heightScale)*TITGameConfig.GS_HEIGHT();
            height=heightScale*TITGameConfig.GS_HEIGHT();
        }
        Tube tube=new Tube(x,y,width,height,textureRegion,iControl);
        if(type==1)
            tube.setRotation(180f);
        tube.setIndex(index);
        tube.setType(type);
        return tube;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
}
