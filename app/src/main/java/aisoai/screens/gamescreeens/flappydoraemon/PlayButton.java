package aisoai.screens.gamescreeens.flappydoraemon;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class PlayButton extends TITSprite
{
    private int type;

    public PlayButton(float x, float y, float width, float height, TextureRegion textureRegion,
                      TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            getControl().playEvent(type);
        }
    }

    @Override
    public FlappyDoraemonControl getControl()
    {
        return (FlappyDoraemonControl) control;
    }

    public static PlayButton createPlayButton(TITGameControl iControl,int type)
    {
        float x,y,width,height;
        TextureRegion textureRegion=null;
        if(type==1)
            textureRegion=FlappyDoraemonGameResource.getPlayButton();
        if(type==2)
            textureRegion=FlappyDoraemonGameResource.getReplayButton();
        width=FlappyDoraemonConfig.PLAY_BUTTON_WIDTH();
        height=FlappyDoraemonConfig.PLAY_BUTTON_HEIGHT();
        x=FlappyDoraemonConfig.PLAY_BUTTON_X();
        y=FlappyDoraemonConfig.PLAY_BUTTON_Y();
        PlayButton playButton=new PlayButton(x,y,width,height,textureRegion,iControl);
        playButton.setType(type);
        return playButton;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
}
