package aisoai.screens.gamescreeens.flappydoraemon;

import org.andengine.entity.IEntity;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class FlappyDoraemonScene extends TITScene
{
    private Doraemon doraemon;
    private ArrayList<Tube> tubeArr=new ArrayList<Tube>();
    private PlayButton playButton;
    private TouchEventListenBoard board;

    @Override
    protected FlappyDoraemonControl getControl()
    {
        return (FlappyDoraemonControl) control;
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        if(iEntity instanceof Doraemon)
        {
            Doraemon iDoraemon =(Doraemon) iEntity;
            this.doraemon=iDoraemon;
        }

        if(iEntity instanceof Tube)
        {
            Tube iTube=(Tube) iEntity;
            tubeArr.add(iTube);
        }

        if(iEntity instanceof PlayButton)
        {
            PlayButton iPlayButton=(PlayButton) iEntity;
            this.playButton=iPlayButton;
        }

        if(iEntity instanceof TouchEventListenBoard)
        {
            TouchEventListenBoard iBoard=(TouchEventListenBoard) iEntity;
            this.board=iBoard;
        }

        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        if (iEntity instanceof Doraemon)
        {
            Doraemon iDoraemon = (Doraemon) iEntity;
            doraemon = null;
        }

        if(iEntity instanceof Tube)
        {
            Tube iTube=(Tube) iEntity;
            tubeArr.remove(iTube);
        }

        if(iEntity instanceof PlayButton)
        {
            PlayButton iPlayButton=(PlayButton) iEntity;
            this.playButton=null;
        }

        if(iEntity instanceof TouchEventListenBoard)
        {
            TouchEventListenBoard iBoard=(TouchEventListenBoard) iEntity;
            this.board=null;
        }

        return super.detachChild(iEntity);
    }

    /*
    public void clearTube()
    {
        while(tubeArr.size()>0)
        {
            Tube tube=tubeArr.get(0);
            tube.clearEntityModifiers();
            detachChild(tube);
        }
        tubeArr.resetVerLine();
    }
    */

    public Doraemon getDoraemon()
    {
        return doraemon;
    }

    public ArrayList<Tube> getTubeArr()
    {
        return tubeArr;
    }

    public PlayButton getPlayButton()
    {
        return playButton;
    }

    public TouchEventListenBoard getBoard()
    {
        return board;
    }
}
