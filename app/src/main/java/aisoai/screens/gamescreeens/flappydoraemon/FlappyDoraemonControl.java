package aisoai.screens.gamescreeens.flappydoraemon;

import com.google.gson.JsonObject;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITCoordinate;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITSprite;
import aisoai.screens.gamescreeens.titentities.physicextension.TITAcceleration;
import aisoai.screens.gamescreeens.titentities.physicextension.TITSpeed;
import aisoai.screens.gamescreeens.titentities.physicextension.TITVector;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class FlappyDoraemonControl extends TITGameControl
{

    @Override
    protected void initBackground()
    {
        AutoParallaxBackground autoParallaxBackground=new AutoParallaxBackground(0,0,0,5);
        TextureRegion backgroundTR=getBackground();
        float postionY=-1.0f* TITGameConfig.CAMERA_Y();
        float width=TITGameConfig.GS_WIDTH()*2;
        float height=TITGameConfig.GS_HEIGHT();
        TITSprite sprite=new TITSprite(0,postionY,width,height,backgroundTR,this)
        {
            @Override
            public TITGameControl getControl()
            {
                return FlappyDoraemonControl.this;
            }
        };
        AutoParallaxBackground.ParallaxEntity parallaxEntity=
                new AutoParallaxBackground.ParallaxEntity(-5f,sprite);
        autoParallaxBackground.attachParallaxEntity(parallaxEntity);
        getScene().setBackground(autoParallaxBackground);
    }

    @Override
    protected TextureRegion getBackground()
    {
        return FlappyDoraemonGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        TouchEventListenBoard board=TouchEventListenBoard.createBoard(this);
        getScene().attachChild(board);
        getScene().registerTouchArea(board);
    }

    private void initDoraemon(int type)
    {
        getScene().unregisterTouchArea(getScene().getBoard());
        PlayButton playButton=PlayButton.createPlayButton(this,type);
        getScene().attachChild(playButton);
        getScene().registerTouchArea(playButton);

        Doraemon doraemon=Doraemon.createDoraemon(this);
        getScene().attachChild(doraemon);

        TITCoordinate coordinate=new TITCoordinate(0,9.8f*FlappyDoraemonConfig.M());
        TITAcceleration acceleration=new TITAcceleration("g",new TITVector(coordinate));
        doraemon.registerAcceleration(acceleration);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();

        switch (methodName)
        {
            case "init":
                getModel().setGameStatus(GameStatus.READY);
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                FlappyDoraemonConfig.mScale=token.get(KJS.PARAM2).getAsFloat();
                getModel().setTime(gameOverTime);
                initDoraemon(1);
                gameScriptReader.stop();
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;
            case "createNewTube":
                int index=token.get(KJS.INDEX).getAsInt();
                float topHeightScale=token.get(KJS.PARAM1).getAsFloat();
                float botHeightScale=token.get(KJS.PARAM2).getAsFloat();
                float runTime=token.get(KJS.PARAM3).getAsFloat();
                createNewTube(topHeightScale,botHeightScale,runTime,index);
                break;
        }
    }

    private void createNewTube(float topHeightScale,float botHeightScale,float runTime,int index)
    {
        Tube tube1=Tube.createTube(FlappyDoraemonControl.this,1,topHeightScale,index);
        Tube tube2=Tube.createTube(FlappyDoraemonControl.this,2,botHeightScale,index);
        float startX=FlappyDoraemonConfig.TUBE_X();
        float finishX=FlappyDoraemonConfig.FINISH_TUBE_X();
        MoveXModifier moveModifier=new MoveXModifier(runTime,startX,finishX);

        IModifier.IModifierListener<IEntity> modifierListener=
                new IModifier.IModifierListener<IEntity>()
                {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity)
                    {

                    }

                    @Override
                    public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
                    {
                        MoveXModifier thisMoveModifier=(MoveXModifier) iModifier;
                        Tube thisTube=(Tube) iEntity;
                        thisTube.unregisterEntityModifier(thisMoveModifier);
                        getScene().detachChild(thisTube);
                        if(thisTube.getType()==1)
                        {
                            getRequestFactory().sendGameAnswer(thisTube.getIndex(),1);
                        }
                    }
                };

        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        tube1.registerEntityModifier(moveModifier);
        tube2.registerEntityModifier(moveModifier);

        getScene().attachChild(tube1);
        getScene().attachChild(tube2);
    }

    public void playEvent(int type)
    {
        System.out.println("-Play Event");
        getScene().unregisterTouchArea(getScene().getPlayButton());
        getScene().detachChild(getScene().getPlayButton());
        getScene().registerTouchArea(getScene().getBoard());
        switch (getModel().getGameStatus())
        {
            case READY:
                getModel().setGameStatus(GameStatus.PLAYING);
                getScene().getDoraemon().start();
                gameScriptReader.resume();
                break;
            case SLEEPING:
                getModel().setGameStatus(GameStatus.PLAYING);
                int oldDieCount = getModel().getDieCount();
                getScene().getDoraemon().start();
                try {Thread.sleep(1500);} catch (InterruptedException e) {}
                int newDieCount=getModel().getDieCount();
                if(newDieCount==oldDieCount)
                    gameScriptReader.resume();
                break;
        }
    }

    public void touchEvent()
    {
        System.out.println("-Touch Event");
        switch (getModel().getGameStatus())
        {
            case PLAYING:
                TITSpeed thisSpeed = getScene().getDoraemon().getSpeed();
                float y = -thisSpeed.getVector().getY()-2.25f* FlappyDoraemonConfig.M();
                TITSpeed newSpeed = new TITSpeed("g", new TITVector(new TITCoordinate(0,y)));
                getScene().getDoraemon().addSpeed(newSpeed);
                break;
            case READY:
                break;
            case SLEEPING:
                break;
        }
    }

    public void doraemonCollidesWidthEvent(int type)
    {
        getModel().increaseDieCount();
        getModel().setGameStatus(GameStatus.SLEEPING);
        gameScriptReader.stop();

        Doraemon doraemon=getScene().getDoraemon();
        doraemon.stop();
        getScene().detachChild(doraemon);

        clearTube();
        initDoraemon(2);

        if(type==1)
        {
        }

        if(type==2)
        {

        }
    }

    private void clearTube()
    {
        ArrayList<Tube> tubeArr=getScene().getTubeArr();
        while (tubeArr.size()>0)
        {
            Tube tube=tubeArr.get(0);
            tube.clearEntityModifiers();
            getScene().detachChild(tube);
            if(tube.getType()==1)
            {
                getRequestFactory().sendGameAnswer(tube.getIndex(),2);
            }
        }
        tubeArr.clear();
    }

    @Override
    public void finish()
    {
        getScene().getDoraemon().stop();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return FlappyDoraemonActivity.class;
    }

    @Override
    public FlappyDoraemonScene getScene()
    {
        return (FlappyDoraemonScene)engine.getScene();
    }

    @Override
    protected TITGameModel initModel()
    {
        return new FlappyDoraemonModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new FlappyDoraemonRF();
    }

    @Override
    public FlappyDoraemonActivity getActivity()
    {
        return (FlappyDoraemonActivity) activity;
    }

    @Override
    public FlappyDoraemonModel getModel()
    {
        return (FlappyDoraemonModel) model;
    }

    @Override
    public FlappyDoraemonRF getRequestFactory()
    {
        return (FlappyDoraemonRF) requestFactory;
    }
}
