package aisoai.screens.gamescreeens.flappydoraemon;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class FlappyDoraemonConfig
{
    public static float mScale;

    public static float G()
    {
        return 9.8f*M();
    }

    public static float M()
    {
        return mScale*TITGameConfig.GS_HEIGHT();
    }

    public static float DORAEMON_WIDTH()
    {
        return 40*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DORAEMON_HEIGHT()
    {
        return DORAEMON_WIDTH();
    }

    public static float DORAEMON_X()
    {
        return TITGameConfig.GS_WIDTH()/2-DORAEMON_WIDTH()/2;
    }

    public static float DORAEMON_Y()
    {
        return TITGameConfig.GS_HEIGHT()/2-DORAEMON_HEIGHT()/2;
    }

    public static float TUBE_WIDTH()
    {
        return 60*TITGameConfig.GS_WIDTH()/320;
    }

    public static float TUBE_X()
    {
        return TITGameConfig.GS_WIDTH();
    }

    public static float FINISH_TUBE_X()
    {
        return -TUBE_WIDTH();
    }

    public static float LANE_Y()
    {
        return TITGameConfig.GS_HEIGHT();
    }

    public static float PLAY_BUTTON_WIDTH()
    {
        return 60*TITGameConfig.GS_WIDTH()/320;
    }

    public static float PLAY_BUTTON_HEIGHT()
    {
        return PLAY_BUTTON_WIDTH();
    }

    public static float PLAY_BUTTON_X()
    {
        return TITGameConfig.GS_WIDTH()/2-PLAY_BUTTON_WIDTH()/2;
    }

    public static float PLAY_BUTTON_Y()
    {
        return 4*TITGameConfig.GS_HEIGHT()/5-PLAY_BUTTON_HEIGHT()/2;
    }
}
