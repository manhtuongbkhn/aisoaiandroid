package aisoai.screens.gamescreeens.flappydoraemon;

import org.andengine.opengl.texture.region.TextureRegion;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.physicextension.TITPhysicSprite;

public class Doraemon extends TITPhysicSprite
{
    public Doraemon(float x,float y,float width,float height,TextureRegion textureRegion,
                                                            TITGameControl iControl,float weight)
    {
        super(x, y, width, height, textureRegion, iControl, weight);
    }

    @Override
    public void setCoordinate(float newX, float newY)
    {
        setX(newX);
        setY(newY);
    }

    @Override
    public void afterSetCoordinate()
    {
        ArrayList<Tube> tubeArr=getControl().getScene().getTubeArr();
        for(int i=0;i<tubeArr.size();i++)
        {
            Tube tube=tubeArr.get(i);
            if(collidesWith(tube))
            {
                getControl().doraemonCollidesWidthEvent(1);
                break;
            }
        }

        if(getY()>=FlappyDoraemonConfig.LANE_Y()-getHeight())
            getControl().doraemonCollidesWidthEvent(2);
    }

    @Override
    public FlappyDoraemonControl getControl()
    {
        return (FlappyDoraemonControl) control;
    }

    public static Doraemon createDoraemon(TITGameControl iControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=FlappyDoraemonGameResource.getDoraemon();
        float weight=1f;
        width=FlappyDoraemonConfig.DORAEMON_WIDTH();
        height=FlappyDoraemonConfig.DORAEMON_HEIGHT();
        x=FlappyDoraemonConfig.DORAEMON_X();
        y=FlappyDoraemonConfig.DORAEMON_Y();
        Doraemon doraemon=new Doraemon(x,y,width,height,textureRegion,iControl,weight);
        return doraemon;
    }
}
