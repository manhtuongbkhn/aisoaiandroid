package aisoai.screens.gamescreeens.flappydoraemon;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class FlappyDoraemonActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getFlappyBirdControl();
    }

    @Override
    public FlappyDoraemonControl getControl()
    {
        return (FlappyDoraemonControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new FlappyDoraemonGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new FlappyDoraemonScene();
    }
}
