package aisoai.screens.gamescreeens.zombiegame;

import org.andengine.entity.IEntity;
import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class ZombieGameScene extends TITScene
{
    private ArrayList<RunSprite> runSpriteArr;

    public ZombieGameScene()
    {
        runSpriteArr=new ArrayList<RunSprite>();
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        if(iEntity instanceof RunSprite)
        {
            RunSprite runSprite=(RunSprite) iEntity;
            runSpriteArr.add(runSprite);
        }
        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        if(iEntity instanceof RunSprite)
        {
            RunSprite runSprite=(RunSprite) iEntity;
            runSpriteArr.remove(runSprite);
        }
        return super.detachChild(iEntity);
    }

    public ArrayList<RunSprite> getRunSpriteArr()
    {
        return runSpriteArr;
    }

    protected ZombieGameControl getControl()
    {
        return (ZombieGameControl) control;
    }
}
