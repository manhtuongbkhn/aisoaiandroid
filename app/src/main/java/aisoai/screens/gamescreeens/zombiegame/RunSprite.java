package aisoai.screens.gamescreeens.zombiegame;

import org.andengine.opengl.texture.region.TiledTextureRegion;
import aisoai.screens.gamescreeens.titentities.TITAnimatedSprite;
import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class RunSprite extends TITAnimatedSprite
{
    protected int index;
    protected boolean run;

    public RunSprite(float x,float y,float width,float height,TiledTextureRegion tiledTextureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x,y,width,height,tiledTextureRegion,inputControl);
        setRun(true);
        long arr[]={500,500,500};
        animate(arr,true);
    }

    /*
    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        if(event.getAction()==TouchEvent.ACTION_DOWN)
        {
            getControl().runSpriteTouchEvent(this);
        }
        return true;
    }
    */

    public boolean isRun()
    {
        return run;
    }

    public void setRun(boolean run)
    {
        this.run = run;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public ZombieGameControl getControl()
    {
        return (ZombieGameControl) control;
    }
}
