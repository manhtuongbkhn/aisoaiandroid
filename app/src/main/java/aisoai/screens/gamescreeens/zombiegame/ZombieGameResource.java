package aisoai.screens.gamescreeens.zombiegame;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class ZombieGameResource extends TITGameResource
{
    private static TiledTextureRegion zombie1;
    private static TiledTextureRegion zombie2;
    private static TiledTextureRegion zombie3;
    private static TiledTextureRegion zombie4;
    private static TiledTextureRegion zombie5;
    private static TiledTextureRegion zombie6;
    private static TiledTextureRegion zombie7;
    private static TiledTextureRegion zombie8;
    private static TiledTextureRegion girl;
    private static TextureRegion transparent;
    private static TextureRegion background;

    public ZombieGameResource(TITGameActivity titGameActivity) {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        zombie1 = createTiledTextureRegionFromAsset("zombiegame/zombie1.png", 3, 1,1);
        zombie2 = createTiledTextureRegionFromAsset("zombiegame/zombie2.png", 3, 1,1);
        zombie3 = createTiledTextureRegionFromAsset("zombiegame/zombie3.png", 3, 1,1);
        zombie4 = createTiledTextureRegionFromAsset("zombiegame/zombie4.png", 3, 1,1);
        zombie5 = createTiledTextureRegionFromAsset("zombiegame/zombie5.png", 3, 1,1);
        zombie6 = createTiledTextureRegionFromAsset("zombiegame/zombie6.png", 3, 1,1);
        zombie7 = createTiledTextureRegionFromAsset("zombiegame/zombie7.png", 3, 1,1);
        zombie8 = createTiledTextureRegionFromAsset("zombiegame/zombie8.png", 3, 1,1);
        girl = createTiledTextureRegionFromAsset("zombiegame/girl.png", 3, 1,1);
        transparent=createTextureRegionFromAsset("transparent.png",1);
        background =createTextureRegionFromAsset("zombiegame/zombie_game_background.png",3);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    public static TiledTextureRegion getZombie1()
    {
        return zombie1;
    }

    public static TiledTextureRegion getZombie2()
    {
        return zombie2;
    }

    public static TiledTextureRegion getZombie3()
    {
        return zombie3;
    }

    public static TiledTextureRegion getZombie4()
    {
        return zombie4;
    }

    public static TiledTextureRegion getZombie5()
    {
        return zombie5;
    }

    public static TiledTextureRegion getZombie6()
    {
        return zombie6;
    }

    public static TiledTextureRegion getZombie7()
    {
        return zombie7;
    }

    public static TiledTextureRegion getZombie8()
    {
        return zombie8;
    }

    public static TiledTextureRegion getGirl()
    {
        return girl;
    }

    public static TextureRegion getTransparent()
    {
        return transparent;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    @Override
    public void clearResource()
    {
        zombie1=null;
        zombie2=null;
        zombie3=null;
        zombie4=null;
        zombie5=null;
        zombie6=null;
        zombie7=null;
        zombie8=null;
        girl=null;
        background =null;
    }
}
