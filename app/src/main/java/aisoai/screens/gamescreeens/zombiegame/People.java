package aisoai.screens.gamescreeens.zombiegame;

import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class People extends RunSprite
{
    public People(float x,float y,float width,float height,TiledTextureRegion tiledTextureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x,y,width,height,tiledTextureRegion,inputControl);
    }

    public static People creatPeople(int postion,int index,TITGameControl inputControl)
    {
        float width= ZombieGameConfig.PEOPLE_WIDTH();
        float height= ZombieGameConfig.PEOPLE_HEIGHT();
        TiledTextureRegion tiledTextureRegion=ZombieGameResource.getGirl();

        float x,y;
        switch (postion)
        {
            case 1:
                x= ZombieGameConfig.START1_X()-width/2;
                break;
            case 2:
                x= ZombieGameConfig.START2_X()-width/2;
                break;
            case 3:
            default:
                x= ZombieGameConfig.START3_X()-width/2;
                break;
        }
        y= ZombieGameConfig.START_Y()-height/2;

        People people= new People(x,y,width,height,tiledTextureRegion,inputControl);
        people.setIndex(index);
        return people;
    }
}
