package aisoai.screens.gamescreeens.zombiegame;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class ZombieGameConfig
{
    public static float CLEAR_HANDLER_TIME=0.5f;

    public static float PEOPLE_WIDTH()
    {
        return 65* TITGameConfig.GS_WIDTH()/320;
    }

    public static float PEOPLE_HEIGHT()
    {
        return PEOPLE_WIDTH();
    }

    public static float SMALLZOMBIE_WIDTH()
    {
        return 60* TITGameConfig.GS_WIDTH()/320;
    }

    public static float SMALLZOMBIE_HEIGHT()
    {
        return SMALLZOMBIE_WIDTH();
    }

    public static float MEDIUMZOMBIE_WIDTH()
    {
        return 85* TITGameConfig.GS_WIDTH()/320;
    }

    public static float MEDIUMZOMBIE_HEIGHT()
    {
        return MEDIUMZOMBIE_WIDTH();
    }

    public static float BIGZOMBIE_WIDTH()
    {
        return 110* TITGameConfig.GS_WIDTH()/320;
    }

    public static float BIGZOMBIE_HEIGHT()
    {
        return BIGZOMBIE_WIDTH();
    }

    public static float START1_X()
    {
        return 60* TITGameConfig.GS_WIDTH()/320;
    }

    public static float START2_X()
    {
        return 160* TITGameConfig.GS_WIDTH()/320;
    }

    public static float START3_X()
    {
        return 260* TITGameConfig.GS_WIDTH()/320;
    }

    public static float START_Y()
    {
        return 0;
    }

    public static float FINISH_Y()
    {
        return TITGameConfig.GS_HEIGHT();
    }
}
