package aisoai.screens.gamescreeens.zombiegame;

import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class Zombie extends RunSprite
{
    private int typeZombie;
    private int life;
    public Zombie(float x, float y, float width,float height,TiledTextureRegion tiledTextureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x,y,width,height,tiledTextureRegion,inputControl);
    }

    public static Zombie createZombie(int zombieType,int postion,int index,
                                                                        TITGameControl inputControl)
    {
        int life;
        float x,y,width,height;
        TiledTextureRegion tiledTextureRegion;

        switch (zombieType)
        {
            case 1:
                width= ZombieGameConfig.SMALLZOMBIE_WIDTH();
                height= ZombieGameConfig.SMALLZOMBIE_HEIGHT();
                tiledTextureRegion=ZombieGameResource.getZombie1();
                life=1;
                break;
            case 2:
                width= ZombieGameConfig.SMALLZOMBIE_WIDTH();
                height= ZombieGameConfig.SMALLZOMBIE_HEIGHT();
                tiledTextureRegion=ZombieGameResource.getZombie2();
                life=1;
                break;
            case 3:
                width= ZombieGameConfig.MEDIUMZOMBIE_WIDTH();
                height= ZombieGameConfig.MEDIUMZOMBIE_HEIGHT();
                tiledTextureRegion=ZombieGameResource.getZombie3();
                life=2;
                break;
            case 4:
                width= ZombieGameConfig.MEDIUMZOMBIE_WIDTH();
                height= ZombieGameConfig.MEDIUMZOMBIE_HEIGHT();
                tiledTextureRegion=ZombieGameResource.getZombie4();
                life=2;
                break;
            case 5:
                width= ZombieGameConfig.BIGZOMBIE_WIDTH();
                height= ZombieGameConfig.BIGZOMBIE_HEIGHT();
                tiledTextureRegion=ZombieGameResource.getZombie5();
                life=3;
                break;
            case 6:
            default:
                width= ZombieGameConfig.BIGZOMBIE_WIDTH();
                height= ZombieGameConfig.BIGZOMBIE_HEIGHT();
                tiledTextureRegion=ZombieGameResource.getZombie6();
                life=3;
                break;
        }

        switch (postion)
        {
            case 1:
                x= ZombieGameConfig.START1_X()-width/2;
                break;
            case 2:
                x= ZombieGameConfig.START2_X()-width/2;
                break;
            case 3:
            default:
                x= ZombieGameConfig.START3_X()-width/2;
                break;
        }
        y= ZombieGameConfig.START_Y()-height/2;

        Zombie zombie= new Zombie(x,y,width,height,tiledTextureRegion,inputControl);
        zombie.setIndex(index);
        zombie.setTypeZombie(zombieType);
        zombie.setLife(life);
        return zombie;
    }

    public int getLife()
    {
        return life;
    }

    public void setLife(int life)
    {
        this.life = life;
    }

    public int getTypeZombie()
    {
        return typeZombie;
    }

    public void setTypeZombie(int typeZombie)
    {
        this.typeZombie = typeZombie;
    }
}
