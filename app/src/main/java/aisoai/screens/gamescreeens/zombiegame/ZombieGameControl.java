package aisoai.screens.gamescreeens.zombiegame;

import com.google.gson.JsonObject;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;
import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class ZombieGameControl extends TITGameControl
{

    @Override
    protected TextureRegion getBackground()
    {
        return ZombieGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        TouchEventListenBoard board=TouchEventListenBoard.createBoard(this);
        getScene().attachChild(board);
        getScene().registerTouchArea(board);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        int index,postion,zombieType;
        float runTime;
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                break;
            case "createNewZombie":
                index=token.get(KJS.INDEX).getAsInt();
                postion=token.get(KJS.PARAM1).getAsInt();
                zombieType=token.get(KJS.PARAM3).getAsInt();
                runTime=token.get(KJS.PARAM2).getAsFloat();
                Zombie zombie=Zombie.createZombie(zombieType,postion,index,this);
                zombie.setControl(this);
                setFinishMoveEvent(zombie, runTime);
                getScene().attachChild(zombie);
                //getScene().registerTouchArea(zombie);
                break;
            case "createNewPeople":
                index=token.get(KJS.INDEX).getAsInt();
                postion=token.get(KJS.PARAM1).getAsInt();
                runTime=token.get(KJS.PARAM2).getAsFloat();
                People people=People.creatPeople(postion,index,this);
                people.setControl(this);
                setFinishMoveEvent(people, runTime);
                getScene().attachChild(people);
                getScene().registerTouchArea(people);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;
            case "finish":
                try {
                    Thread.sleep(500);} catch (InterruptedException e) {}
                finish();
                break;

        }
    }

    public void setFinishMoveEvent(RunSprite runSprite,float runTime)
    {
        float finishY=ZombieGameConfig.FINISH_Y();
        MoveYModifier moveModifier=new MoveYModifier(runTime,runSprite.getY(),finishY);

        IModifier.IModifierListener<IEntity> modifierListener=new IModifier.IModifierListener<IEntity>()
        {
            @Override
            public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity)
            {

            }

            @Override
            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
            {
                MoveYModifier thisMoveYModifier=(MoveYModifier) iModifier;
                RunSprite thisRunSprite =(RunSprite) iEntity;
                thisRunSprite.setRun(false);
                getRequestFactory().sendGameAnswer
                        (thisRunSprite.getIndex(),thisRunSprite.getY(),TITGameConfig.GS_HEIGHT());

                getScene().detachChild(thisRunSprite);
            }
        };

        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        runSprite.registerEntityModifier(moveModifier);
    }

    public void runSpriteTouchEvent(RunSprite runSprite)
    {
        if(runSprite.getClass().equals(People.class))
        {
            runSprite.clearEntityModifiers();
            runSprite.clearUpdateHandlers();
            getScene().unregisterTouchArea(runSprite);
            getScene().detachChild(runSprite);

            getRequestFactory().sendGameAnswer
                    (runSprite.getIndex(), runSprite.getY(), TITGameConfig.GS_HEIGHT());
        }

        if(runSprite.getClass().equals(Zombie.class))
        {
            Zombie zombie=(Zombie) runSprite;
            int life=zombie.getLife();
            life=life-1;
            zombie.setLife(life);

            if(life==0)
            {
                runSprite.clearEntityModifiers();
                runSprite.clearUpdateHandlers();
                getScene().unregisterTouchArea(runSprite);
                getScene().detachChild(runSprite);

                getRequestFactory().sendGameAnswer
                        (runSprite.getIndex(), runSprite.getY(), TITGameConfig.GS_HEIGHT());
            }
        }

    }

    @Override
    public void finish()
    {
    }

    @Override
    public ZombieGameScene getScene()
    {
        return (ZombieGameScene) engine.getScene();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return ZombieGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new TITGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory() {
        return new ZombieGameRF();
    }

    @Override
    public ZombieGameActivity getActivity()
    {
        return (ZombieGameActivity) activity;
    }

    @Override
    public TITGameModel getModel()
    {
        return (TITGameModel) model;
    }

    @Override
    public ZombieGameRF getRequestFactory()
    {
        return (ZombieGameRF) requestFactory;
    }
}
