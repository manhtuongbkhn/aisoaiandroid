package aisoai.screens.gamescreeens.zombiegame;

import aisoai.TITApplication;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class ZombieGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getZombieGameControl();
    }

    @Override
    public TITGameControl getControl()
    {
        return (ZombieGameControl) control;
    }


    @Override
    public TITGameResource initGameResource()
    {
        return new ZombieGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new ZombieGameScene();
    }
}
