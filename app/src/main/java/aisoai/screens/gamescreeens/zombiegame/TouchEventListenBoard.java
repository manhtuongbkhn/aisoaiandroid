package aisoai.screens.gamescreeens.zombiegame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import java.util.ArrayList;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class TouchEventListenBoard extends TITSprite
{
    public TouchEventListenBoard(float x, float y, float width, float height, TextureRegion textureRegion,
                                 TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            checkRunSpriteTouchEvent(x,y);
        }
    }

    private void checkRunSpriteTouchEvent(float x,float y)
    {
        ArrayList<RunSprite> runSpriteArr=getControl().getScene().getRunSpriteArr();
        for(int i=0;i<runSpriteArr.size();i++)
        {
            RunSprite runSprite=runSpriteArr.get(i);
            CheckPianoButtonThread thread=new CheckPianoButtonThread(runSprite,x,y);
            thread.start();
        }
    }

    class CheckPianoButtonThread extends Thread
    {
        private RunSprite runSprite;
        private float x;
        private float y;

        public CheckPianoButtonThread(RunSprite iRunSprite,float iX,float iY)
        {
            runSprite=iRunSprite;
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            float delayY=runSprite.getHeight()/2;
            y=y+delayY;
            if(runSprite.contains(x,y))
            {
                getControl().runSpriteTouchEvent(runSprite);
            }
        }
    }

    @Override
    public ZombieGameControl getControl()
    {
        return (ZombieGameControl) control;
    }

    public static TouchEventListenBoard createBoard(TITGameControl iControl)
    {
        TextureRegion textureRegion=(TextureRegion)ZombieGameResource.getTransparent();
        float x,y,width,height;
        x=0;
        y=0;
        width= TITGameConfig.GS_WIDTH();
        height=TITGameConfig.GS_HEIGHT();
        TouchEventListenBoard board =new TouchEventListenBoard(x,y,width,height,textureRegion,
                                                                                        iControl);
        return board;
    }
}
