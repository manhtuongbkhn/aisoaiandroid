package aisoai.screens.gamescreeens.zombiegame;

import com.smartfoxserver.v2.entities.data.SFSObject;

import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;

public class ZombieGameRF extends TITRequestFactory
{
    public void sendGameAnswer(int index,float finishPostion,float gcHeight)
    {
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.INDEX,index);
        toServerData.putFloat(KJS.PARAM1,finishPostion);
        toServerData.putFloat(KJS.PARAM2,gcHeight);
        ExtensionRequest request=new ExtensionRequest(CMDRQ.GAMEANSWER_RQ,toServerData,room);
        sendRequest(request);
    }
}
