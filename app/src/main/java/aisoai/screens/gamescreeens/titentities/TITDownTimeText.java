package aisoai.screens.gamescreeens.titentities;

import org.andengine.opengl.font.Font;

public class TITDownTimeText extends TITText
{

    public TITDownTimeText(float x, float y, Font font, String text, int textMax,
                           TITGameControl inputControl)
    {
        super(x, y, font, text, textMax, inputControl);
    }

    public static TITDownTimeText createDownTime(TITGameControl inputControl)
    {
        Font timeFont=inputControl.gameResource.getTimeFont();
        float width=(float) timeFont.getStringWidth("");
        float height=(float) timeFont.getLineHeight();
        float postionX=TITGameConfig.SCREEN_WIDTH/2-width/2;
        float postionY=TITGameConfig.HUD_HEIGHT()/2-height/2;
        TITDownTimeText timeText =
                new TITDownTimeText(postionX,postionY,timeFont,"",TITGameConfig.TIME_MAX_CHAR,inputControl);
        timeText.setZIndex(TITGameConfig.TIME_TEXT_LAYER);
        return timeText;
    }
}
