package aisoai.screens.gamescreeens.titentities;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.IEntity;

import java.util.ArrayList;

public class TITHUD extends HUD
{
    private TITDownTimeText downTimeText;
    private ArrayList<TITPointText> pointTextArr;
    private ArrayList<TITSprite> listAvarta;

    public TITHUD()
    {
        super();
        pointTextArr=new ArrayList<TITPointText>();
        listAvarta=new ArrayList<TITSprite>();
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();
        if(inputClass.equals(TITDownTimeText.class))
        {
            TITDownTimeText iDownTimeText =(TITDownTimeText) iEntity;
            this.downTimeText=iDownTimeText;
        }

        if(inputClass.equals(TITPointText.class))
        {
            TITPointText iPointText=(TITPointText) iEntity;
            pointTextArr.add(iPointText);
        }
        super.attachChild(iEntity);
        sortChildren();
    }

    public TITPointText getPointText1()
    {
        return pointTextArr.get(0);
    }

    public TITPointText getPointText2()
    {
        return pointTextArr.get(1);
    }

    public TITPointText getPointText3()
    {
        return pointTextArr.get(2);
    }

    public TITPointText getPointText4()
    {
        return pointTextArr.get(3);
    }

    public TITDownTimeText getDownTimeText()
    {
        return downTimeText;
    }
}
