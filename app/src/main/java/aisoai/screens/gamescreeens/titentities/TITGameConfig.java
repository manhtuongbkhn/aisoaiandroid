package aisoai.screens.gamescreeens.titentities;

import android.graphics.Color;
import org.andengine.util.HorizontalAlign;

import aisoai.config.ClientConfig;
import aisoai.config.UIDefine;

public class TITGameConfig
{
    public static float SCREEN_WIDTH= ClientConfig.SCREEN_WIDTH_PX;
    public static float SCREEN_HEIGHT=ClientConfig.SCREEN_HEIGHT_PX;

    public final static int PHYSICENTITYRUN_DELAYTIMEMILLIS=25;

    public static float CAMERA_WIDTH()
    {
        return SCREEN_WIDTH;
    }

    public static float CAMERA_HEIGHT()
    {
        return SCREEN_HEIGHT;
    }

    public static float AVATAR_WIDTH()
    {
        return 50f*SCREEN_WIDTH/320f;
    }

    public static float AVATAR_HEIGHT()
    {
        return AVATAR_WIDTH();
    }

    public static float HUD_WIDTH()
    {
        return SCREEN_WIDTH;
    }

    public static float HUD_HEIGHT()
    {
        return (90f/320f)*SCREEN_WIDTH;
    }

    public static float GS_WIDTH()
    {
        return SCREEN_WIDTH;
    }

    public static float GS_HEIGHT()
    {
        return SCREEN_HEIGHT-HUD_HEIGHT();
    }

    public static float CAMERA_X()
    {
        return 0.0f*SCREEN_WIDTH;
    }

    public static float CAMERA_Y()
    {
        return -HUD_HEIGHT();
    }

    public static float CENTER_AVATAR1_X()
    {
        return (90f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_AVATAR2_X()
    {
        return (230f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_AVATAR3_X()
    {
        return (30f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_AVATAR4_X()
    {
        return (290f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_AVATAR_Y()
    {
        return (30f/320f)*SCREEN_WIDTH;
    }

    public static HorizontalAlign HORIZONTAL_ALIGN()
    {
        return HorizontalAlign.CENTER;
    }

    public static int TIME_FONT_SIZE()
    {
        Float f=UIDefine.GIGATICTEXT_SIZE()*ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    public static int POINT_FONT_SIZE()
    {
        Float f=UIDefine.BIGTEXT_SIZE()*ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    public static float CENTER_POINT1_X()
    {
        return (90f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_POINT2_X()
    {
        return (230f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_POINT3_X()
    {
        return (30f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_POINT4_X()
    {
        return (290f/320f)*SCREEN_WIDTH;
    }

    public static float CENTER_POINT_Y()
    {
        return (75f/320)*SCREEN_WIDTH;
    }

    public static int TIME_COLOR= Color.argb(255,7,42,255);
    public static int POINT_COLOR= Color.BLUE;

    public static int TIME_MAX_CHAR=3;
    public static int POINT_MAX_CHAR=4;
    public static boolean MULTITOUCHSUPPORT;
    public static boolean MULTITOUCHSUPPORTDISTINCT;

    public static String GAME_IMAGE_FOLDER="gfx/";
    public static String FONT_FOLDER="fonts/";
    public static String MUSIC_FOLDER="mfx/";

    public static int TINY_TEXTURE_WIDTH=64;
    public static int TINY_TEXTURE_HEIGHT=64;

    public static int SMALL_TEXTURE_WIDTH=128;
    public static int SMALL_TEXTURE_HEIGHT=128;

    public static int MEDIUM_TEXTURE_WIDTH=256;
    public static int MEDIUM_TEXTURE_HEIGHT=256;

    public static int BIG_TEXTURE_WIDTH=512;
    public static int BIG_TEXTURE_HEIGHT=512;

    public static int HUGE_TEXTURE_WIDTH=1024;
    public static int HUGE_TEXTURE_HEIGHT=1024;

    public static int GIGATIC_TEXTURE_WIDTH=2048;
    public static int GIGATIC_TEXTURE_HEIGHT=2048;

    public static int CHILD_COUNT=0;
    public static float getRatio()
    {
        return CAMERA_WIDTH()/CAMERA_HEIGHT();
    }

    public static int HUD_LAYER=12;
    public static int TIME_TEXT_LAYER=20;
    public static int POINT_TEXT_LAYER=20;
    public static int AVARTA_LAYER=20;
}