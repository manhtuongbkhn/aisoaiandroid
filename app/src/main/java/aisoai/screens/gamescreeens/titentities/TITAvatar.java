package aisoai.screens.gamescreeens.titentities;

import android.graphics.Bitmap;

import org.andengine.opengl.texture.region.TextureRegion;

public class TITAvatar extends TITSprite
{

    public TITAvatar(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public TITGameControl getControl()
    {
        return null;
    }

    public static TITAvatar createAvatar(Bitmap bitmap,int player,TITGameControl inputControl)
    {
        TextureRegion avataTR=inputControl.gameResource.createTextureRegionFromBitmap(bitmap,1);
        float x,y,width,height;
        width=TITGameConfig.AVATAR_WIDTH();
        height=TITGameConfig.AVATAR_HEIGHT();

        switch (player)
        {
            case 1:
                x=TITGameConfig.CENTER_AVATAR1_X()-TITGameConfig.AVATAR_WIDTH()/2;
                break;
            case 2:
                x=TITGameConfig.CENTER_AVATAR2_X()-TITGameConfig.AVATAR_WIDTH()/2;
                break;
            case 3:
                x=TITGameConfig.CENTER_AVATAR3_X()-TITGameConfig.AVATAR_WIDTH()/2;
                break;
            case 4:
            default:
                x=TITGameConfig.CENTER_AVATAR4_X()-TITGameConfig.AVATAR_WIDTH()/2;
                break;
        }

        y=TITGameConfig.CENTER_AVATAR_Y()-TITGameConfig.AVATAR_WIDTH()/2;
        TITAvatar avatar=new TITAvatar(x,y,width,height,avataTR,inputControl);
        avatar.setZIndex(TITGameConfig.AVARTA_LAYER);
        return avatar;
    }
}
