package aisoai.screens.gamescreeens.pianogame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class BlackButton extends PianoButton
{
    public BlackButton(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl iControl)
    {
        super(x,y,width,height,textureRegion,iControl);
    }

    public static BlackButton createBlackButton(int postion,TITGameControl iControl)
    {
        TextureRegion textureRegion=(TextureRegion)PianoGameResource.getBlackButton();
        float x,y,width,height;
        x=PianoGameConfig.BUTTON_X(postion);
        y=PianoGameConfig.BLACKBUTTON_Y();
        width=PianoGameConfig.BUTTON_WIDTH();
        height=PianoGameConfig.BLACKBUTTON_HEIGHT();

        BlackButton blackButton =new BlackButton(x,y,width,height,textureRegion,iControl);
        blackButton.setStatus(1);
        return blackButton;
    }

    public int getDeltaYPercent()
    {
        float centerY=getCenterY();
        OrangeLine orangeLine=getControl().getScene().getOrangeLine();
        float deltaY=Math.abs(centerY-orangeLine.getY());
        Float deltaYPercent=deltaY*100/PianoGameConfig.BLACKBUTTON_HEIGHT();
        return deltaYPercent.intValue();
    }
}
