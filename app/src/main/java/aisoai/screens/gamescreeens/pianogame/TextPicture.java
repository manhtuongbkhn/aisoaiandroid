package aisoai.screens.gamescreeens.pianogame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.auditiongame.AuditionGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class TextPicture extends TITSprite
{
    private String content;

    public TextPicture(float x, float y, float width, float height, TextureRegion textureRegion,
                       TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static TextPicture createTextPicture(String content,TITGameControl inputControl)
    {
        float x,y,widht,height;
        widht=PianoGameConfig.TEXTPICTURE_WIDTH();
        height=PianoGameConfig.TEXTPICTURE_HEIGHT();
        x=PianoGameConfig.TEXTPICTURE_X();
        y=PianoGameConfig.TEXTPICTURE_Y();

        TextureRegion textureRegion;
        switch (content)
        {
            case "miss":
                textureRegion=PianoGameResource.getMiss();
                break;
            case "cool":
                textureRegion=PianoGameResource.getCool();
                break;
            case "creat":
                textureRegion=PianoGameResource.getCreat();
                break;
            case "perfect":
            default:
                textureRegion=PianoGameResource.getPerfect();
                break;
        }
        TextPicture textPicture=new TextPicture(x,y,widht,height,textureRegion,inputControl);
        textPicture.setContent(content);
        return textPicture;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
