package aisoai.screens.gamescreeens.pianogame;

import android.media.MediaPlayer;

import com.google.gson.JsonObject;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITLine;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class PianoGameControl extends TITGameControl
{
    @Override
    protected TextureRegion getBackground()
    {
        return PianoGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        initEventListenBoard();
        initLine();
    }

    private void initEventListenBoard()
    {
        TouchEventListenBoard board=TouchEventListenBoard.createBoard(this);
        getScene().attachChild(board);
        getScene().registerTouchArea(board);
    }

    private void initLine()
    {
        GreenLine greenLine=GreenLine.createGreenLine(this);
        getScene().attachChild(greenLine);

        OrangeLine orangeLine= OrangeLine.createOrangeLine(this);
        getScene().attachChild(orangeLine);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        float postionY;
        switch (methodName)
        {
            case "init":
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                PianoGameResource.getMusic().play();
                PianoGameResource.getMusic().pause();
                break;
            case "createNewButton":
                int index=token.get(KJS.INDEX).getAsInt();
                int postion=token.get(KJS.PARAM1).getAsInt();
                float runTime=token.get(KJS.PARAM2).getAsFloat();
                int type=token.get(KJS.PARAM3).getAsInt();
                PianoButton pianoButton = PianoButton.createButton(type,postion,this);
                pianoButton.setIndex(index);
                setMovePianoButton(pianoButton,runTime);
                getScene().attachChild(pianoButton);
                break;
            case "changeGreenLine":
                postionY=token.get(KJS.PARAM1).getAsFloat();
                getScene().getGreenLine().setY(postionY * TITGameConfig.GS_HEIGHT()/100);
                break;
            case "changeOrangeLine":
                postionY=token.get(KJS.PARAM1).getAsFloat();
                getScene().getOrangeLine().setY(postionY*TITGameConfig.GS_HEIGHT()/100);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;

        }
    }

    private void setMovePianoButton(PianoButton pianoButton,float runTime)
    {
        float toY=0f;
        if(pianoButton.getClass().equals(BlackButton.class))
            toY=PianoGameConfig.BLACKBUTTONTO_Y();
        if(pianoButton.getClass().equals(WhiteButton.class))
            toY=PianoGameConfig.WHITEBUTTONTO_Y();
        MoveYModifier moveModifier=new MoveYModifier(runTime,pianoButton.getY(),toY);

        IModifier.IModifierListener<IEntity> modifierListener=
                new IModifier.IModifierListener<IEntity>()
                {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity){}

                    @Override
                    public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
                    {
                        PianoButton thisPianoButton=(PianoButton) iEntity;
                        movePianoButtonFinishEvent(thisPianoButton);
                    }
                };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        pianoButton.registerEntityModifier(moveModifier);
    }

    public void pianoButtonEvent(PianoButton pianoButton)
    {
        if(pianoButton.getStatus()==1)
        {
            float deltaYpercent=pianoButton.getDeltaYPercent();
            if(deltaYpercent<40)
            {
                if (0 <= deltaYpercent && deltaYpercent < 8)
                {
                    showTextPicture("perfect");
                }

                if (8 <= deltaYpercent && deltaYpercent < 20)
                {
                    showTextPicture("creat");
                }

                if (20 <= deltaYpercent && deltaYpercent < 40)
                {
                    showTextPicture("cool");
                }

                pianoButton.setStatus(2);
                MediaPlayer mediaPlayer = PianoGameResource.getMusic().getMediaPlayer();
                if (!mediaPlayer.isPlaying())
                {
                    mediaPlayer.start();
                }
            }

            if(deltaYpercent>=40)
            {
                showTextPicture("miss");
                pianoButton.setStatus(3);
            }
            int index=pianoButton.getIndex();
            getRequestFactory().sendGameAnswer(index, deltaYpercent);
        }
    }

    private void movePianoButtonFinishEvent(PianoButton pianoButton)
    {
        if(pianoButton.getStatus()==1||pianoButton.getStatus()==3)
        {
            if(pianoButton.getStatus()==1)
            {
                int index=pianoButton.getIndex();
                getRequestFactory().sendGameAnswer(index,-1f);
            }

            MediaPlayer mediaPlayer=PianoGameResource.getMusic().getMediaPlayer();
            int currentPostion=mediaPlayer.getCurrentPosition();
            mediaPlayer.seekTo(currentPostion + 1000);
            mediaPlayer.pause();
        }
        getScene().detachChild(pianoButton);
    }

    private void showTextPicture(String content)
    {
        TextPicture textPicture=TextPicture.createTextPicture(content,this);
        float width,height;
        width=PianoGameConfig.TEXTPICTURE_WIDTH();
        height=PianoGameConfig.TEXTPICTURE_HEIGHT();
        ScaleAtModifier scaleAtModifier=new ScaleAtModifier(1f,1.0f,1.2f,width/2,height/2);
        IModifier.IModifierListener modifierListener=new IModifier.IModifierListener()
        {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem)
            {

            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem)
            {
                TextPicture thisTextPicture=(TextPicture) pItem;
                getScene().detachChild(thisTextPicture);
            }
        };

        scaleAtModifier.addModifierListener(modifierListener);
        scaleAtModifier.setAutoUnregisterWhenFinished(true);
        textPicture.registerEntityModifier(scaleAtModifier);
        getScene().attachChild(textPicture);
    }

    @Override
    public void finish()
    {

    }

    @Override
    public PianoGameScene getScene()
    {
        return (PianoGameScene) engine.getScene();
    }


    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return PianoGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new PianoGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new PianoGameRF();
    }

    @Override
    public PianoGameActivity getActivity()
    {
        return (PianoGameActivity) activity;
    }

    @Override
    public PianoGameModel getModel()
    {
        return (PianoGameModel) model;
    }

    @Override
    public PianoGameRF getRequestFactory()
    {
        return (PianoGameRF) requestFactory;
    }
}
