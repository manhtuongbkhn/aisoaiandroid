package aisoai.screens.gamescreeens.pianogame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;
import aisoai.screens.titentities.TITRequestFactory;

public class PianoGameActivity extends TITGameActivity
{

    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getPianoGameControl();
    }

    @Override
    public PianoGameControl getControl()
    {
        return (PianoGameControl) control;
    }


    @Override
    public TITGameResource initGameResource()
    {
        return new PianoGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new PianoGameScene();
    }

}
