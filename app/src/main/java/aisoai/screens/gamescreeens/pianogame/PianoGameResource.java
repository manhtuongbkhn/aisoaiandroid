package aisoai.screens.gamescreeens.pianogame;

import org.andengine.audio.music.Music;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class PianoGameResource extends TITGameResource
{
    private static TextureRegion blackButton;
    private static TextureRegion whiteButton;
    private static TextureRegion miss;
    private static TextureRegion cool;
    private static TextureRegion creat;
    private static TextureRegion perfect;
    private static TextureRegion transparent;
    private static TextureRegion background;
    private static Music music;

    public PianoGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        blackButton=createTextureRegionFromAsset("pianogame/black_button.png",3);
        whiteButton=createTextureRegionFromAsset("pianogame/white_button.png",3);
        miss=createTextureRegionFromAsset("auditiongame/miss.png",3);
        cool=createTextureRegionFromAsset("auditiongame/cool.png",3);
        creat=createTextureRegionFromAsset("auditiongame/creat.png",3);
        perfect=createTextureRegionFromAsset("auditiongame/perfect.png",3);
        transparent=createTextureRegionFromAsset("transparent.png",1);
        background=createTextureRegionFromAsset("pianogame/background.png",4);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {
        music=createMusicFromAsset("pianogame/dau_mua.mp3");
    }

    @Override
    protected void clearResource()
    {
        blackButton=null;
        whiteButton=null;
        miss=null;
        cool=null;
        creat=null;
        perfect=null;
        background=null;
    }

    public static TextureRegion getBlackButton()
    {
        return blackButton;
    }

    public static TextureRegion getWhiteButton()
    {
        return whiteButton;
    }

    public static TextureRegion getTransparent()
    {
        return transparent;
    }

    public static TextureRegion getMiss()
    {
        return miss;
    }

    public static TextureRegion getCool()
    {
        return cool;
    }

    public static TextureRegion getCreat()
    {
        return creat;
    }

    public static TextureRegion getPerfect()
    {
        return perfect;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public static Music getMusic()
    {
        return music;
    }
}
