package aisoai.screens.gamescreeens.pianogame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class WhiteButton extends PianoButton
{
    public WhiteButton(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                        TITGameControl iControl)
    {
        super(x,y,width,height,textureRegion,iControl);
    }

    public static WhiteButton createWhiteButton(int postion,TITGameControl iControl)
    {
        TextureRegion textureRegion=(TextureRegion)PianoGameResource.getWhiteButton();
        float x,y,width,height;
        x=PianoGameConfig.BUTTON_X(postion);
        y=PianoGameConfig.WHITEBUTTON_Y();
        width=PianoGameConfig.BUTTON_WIDTH();
        height=PianoGameConfig.WHITEBUTTON_HEIGHT();

        WhiteButton whiteButton =new WhiteButton(x,y,width,height,textureRegion,iControl);
        whiteButton.setStatus(1);
        return whiteButton;
    }

    public int getDeltaYPercent()
    {
        float centerY=getCenterY();
        GreenLine greenLine=getControl().getScene().getGreenLine();
        float deltaY=Math.abs(centerY-greenLine.getY());
        Float deltaYPercent=deltaY*100/PianoGameConfig.WHITEBUTTON_HEIGHT();
        return deltaYPercent.intValue();
    }
}
