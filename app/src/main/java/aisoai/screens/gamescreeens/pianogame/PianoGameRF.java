package aisoai.screens.gamescreeens.pianogame;

import com.smartfoxserver.v2.entities.data.SFSObject;
import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;

public class PianoGameRF extends TITRequestFactory
{
    public void sendGameAnswer(int index,float deltaYPercent)
    {
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.INDEX,index);
        toServerData.putFloat(KJS.PARAM1,deltaYPercent);
        ExtensionRequest request=new ExtensionRequest(CMDRQ.GAMEANSWER_RQ,toServerData,room);
        sendRequest(request);
    }
}

