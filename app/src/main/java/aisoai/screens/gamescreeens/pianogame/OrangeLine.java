package aisoai.screens.gamescreeens.pianogame;

import org.andengine.util.color.Color;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITLine;

public class OrangeLine extends TITLine
{
    public OrangeLine(float x1, float y1, float x2, float y2, TITGameControl iControl)
    {
        super(x1, y1, x2, y2, iControl);
    }

    @Override
    protected TITGameControl getControl()
    {
        return (PianoGameControl) control;
    }

    public static OrangeLine createOrangeLine(TITGameControl iControl)
    {
        float width= TITGameConfig.GS_WIDTH();
        float orangelineY=PianoGameConfig.ORANGELINE_Y();
        OrangeLine orangeLine =new OrangeLine(0,orangelineY,width,orangelineY,iControl);
        orangeLine.setColor(Color.ORANGE);
        return orangeLine;
    }
}
