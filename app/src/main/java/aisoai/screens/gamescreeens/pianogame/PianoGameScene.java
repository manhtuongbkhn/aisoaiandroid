package aisoai.screens.gamescreeens.pianogame;

import org.andengine.entity.IEntity;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class PianoGameScene extends TITScene
{
    private ArrayList<PianoButton> pianoButtonArr;
    private OrangeLine orangeLine;
    private GreenLine greenLine;

    public PianoGameScene()
    {
        super();
        pianoButtonArr=new ArrayList<PianoButton>();
    }

    @Override
    protected PianoGameControl getControl()
    {
        return (PianoGameControl) control;
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        if(iEntity instanceof PianoButton)
        {
            PianoButton iPianoButton=(PianoButton) iEntity;
            this.pianoButtonArr.add(iPianoButton);
        }

        if(iEntity instanceof OrangeLine)
        {
            OrangeLine iOrangeLine =(OrangeLine) iEntity;
            this.orangeLine = iOrangeLine;
        }

        if(iEntity instanceof GreenLine)
        {
            GreenLine iGreenLine =(GreenLine) iEntity;
            this.greenLine=iGreenLine;
        }

        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        if(iEntity instanceof PianoButton)
        {
            PianoButton iPianoButton =(PianoButton) iEntity;
            this.pianoButtonArr.remove(iPianoButton);
        }

        if(iEntity instanceof OrangeLine)
        {
            OrangeLine iOrangeLine =(OrangeLine) iEntity;
            this.orangeLine = null;
        }

        if(iEntity instanceof GreenLine)
        {
            GreenLine iGreenLine =(GreenLine) iEntity;
            this.greenLine=null;
        }

        return super.detachChild(iEntity);
    }

    public ArrayList<PianoButton> getPianoButtonArr()
    {
        return pianoButtonArr;
    }

    public void removePianoButton(PianoButton pianoButton)
    {
        pianoButtonArr.remove(pianoButton);
    }

    public OrangeLine getOrangeLine()
    {
        return orangeLine;
    }

    public GreenLine getGreenLine()
    {
        return greenLine;
    }
}
