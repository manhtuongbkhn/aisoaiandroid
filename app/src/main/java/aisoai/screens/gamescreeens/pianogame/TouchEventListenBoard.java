package aisoai.screens.gamescreeens.pianogame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class TouchEventListenBoard extends TITSprite
{
    public TouchEventListenBoard(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                            TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            checkPianoButtonTouchEvent(x,y);
        }
    }

    private void checkPianoButtonTouchEvent(float x,float y)
    {
        ArrayList<PianoButton> pianoButtonArr=getControl().getScene().getPianoButtonArr();
        for(int i=0;i<pianoButtonArr.size();i++)
        {
            PianoButton pianoButton=pianoButtonArr.get(i);
            CheckPianoButtonThread thread=new CheckPianoButtonThread(pianoButton,x,y);
            thread.start();
        }
    }

    class CheckPianoButtonThread extends Thread
    {
        private PianoButton pianoButton;
        private float x;
        private float y;

        public CheckPianoButtonThread(PianoButton iPianoButton,float iX,float iY)
        {
            pianoButton=iPianoButton;
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            if(pianoButton.contains(x,y))
            {
                getControl().pianoButtonEvent(pianoButton);
            }
        }
    }

    @Override
    public PianoGameControl getControl()
    {
        return (PianoGameControl) control;
    }

    public static TouchEventListenBoard createBoard(TITGameControl iControl)
    {
        TextureRegion textureRegion=(TextureRegion)PianoGameResource.getTransparent();
        float x,y,width,height;
        x=0;
        y=0;
        width= TITGameConfig.GS_WIDTH();
        height=TITGameConfig.GS_HEIGHT();
        TouchEventListenBoard board =new TouchEventListenBoard(x,y,width,height,textureRegion,
                                                                                        iControl);
        return board;
    }
}
