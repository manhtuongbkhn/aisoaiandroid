package aisoai.screens.gamescreeens.pianogame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

abstract public class PianoButton extends TITSprite
{
    private int index;
    private int status;

    /*
        1:begin
        2:sucess
        3:disable
     */

    public PianoButton(float x,float y,float width,float height,TextureRegion textureRegion,
                       TITGameControl iControl)
    {
        super(x,y,width,height,textureRegion,iControl);
    }

    @Override
    public PianoGameControl getControl()
    {
        return (PianoGameControl) control;
    }

    public static PianoButton createButton(int type,int postion,TITGameControl iControl)
    {
        if(type==2)
        {
            return BlackButton.createBlackButton(postion, iControl);
        }
        else
        {
            return WhiteButton.createWhiteButton(postion, iControl);
        }
    }

    protected float getCenterY()
    {
        float height=getHeight();
        return getY()+height/2;
    }

    abstract public int getDeltaYPercent();

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int iStatus)
    {
        this.status = iStatus;
    }
}
