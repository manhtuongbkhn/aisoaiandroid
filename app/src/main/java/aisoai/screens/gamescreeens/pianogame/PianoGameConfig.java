package aisoai.screens.gamescreeens.pianogame;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class PianoGameConfig
{
    public static float BUTTON_WIDTH()
    {
        return 80*TITGameConfig.GS_WIDTH()/320;
    }

    public static float BLACKBUTTON_HEIGHT()
    {
        return 200*TITGameConfig.GS_WIDTH()/320;
    }

    public static float WHITEBUTTON_HEIGHT()
    {
        return 200*TITGameConfig.GS_WIDTH()/320;
    }

    public static float BUTTON_X(int postion)
    {
        return (postion*2-1)*TITGameConfig.GS_WIDTH()/8-BUTTON_WIDTH()/2;
    }

    public static float BLACKBUTTON_Y()
    {
        return -BLACKBUTTON_HEIGHT();
    }

    public static float WHITEBUTTON_Y()
    {
        return -WHITEBUTTON_HEIGHT();
    }

    public static float BLACKBUTTONTO_Y()
    {
        return TITGameConfig.GS_HEIGHT()+BLACKBUTTON_HEIGHT();
    }

    public static float WHITEBUTTONTO_Y()
    {
        return TITGameConfig.GS_HEIGHT()+WHITEBUTTON_HEIGHT();
    }


    public static float ORANGELINE_Y()
    {
        return 5*TITGameConfig.GS_HEIGHT()/6;
    }

    public static float GREENLINE_Y()
    {
        return 4*TITGameConfig.GS_HEIGHT()/6;
    }

    public static float TEXTPICTURE_WIDTH()
    {
        return 150*TITGameConfig.GS_WIDTH()/320;
    }

    public static float TEXTPICTURE_HEIGHT()
    {
        return TEXTPICTURE_WIDTH();
    }

    public static float TEXTPICTURE_X()
    {
        return TITGameConfig.GS_WIDTH()/2-TEXTPICTURE_WIDTH()/2;
    }

    public static float TEXTPICTURE_Y()
    {
        return TITGameConfig.GS_HEIGHT()/2-TEXTPICTURE_HEIGHT()/2;
    }
}
