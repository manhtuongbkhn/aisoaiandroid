package aisoai.screens.gamescreeens.pianogame;

import org.andengine.util.color.Color;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITLine;

public class GreenLine extends TITLine
{

    public GreenLine(float x1,float y1,float x2,float y2,TITGameControl iControl)
    {
        super(x1, y1, x2, y2, iControl);
    }

    @Override
    protected PianoGameControl getControl()
    {
        return (PianoGameControl) control;
    }

    public static GreenLine createGreenLine(TITGameControl iControl)
    {
        float width= TITGameConfig.GS_WIDTH();
        float greenLineY=PianoGameConfig.GREENLINE_Y();
        GreenLine greenLine =new GreenLine(0,greenLineY,width,greenLineY,iControl);
        greenLine.setColor(Color.GREEN);
        return greenLine;
    }
}
