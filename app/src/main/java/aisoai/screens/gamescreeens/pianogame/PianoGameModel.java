package aisoai.screens.gamescreeens.pianogame;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class PianoGameModel extends TITGameModel
{
    private GameStatus gameStatus;

    public GameStatus getGameStatus()
    {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus)
    {
        this.gameStatus = gameStatus;
    }
}
