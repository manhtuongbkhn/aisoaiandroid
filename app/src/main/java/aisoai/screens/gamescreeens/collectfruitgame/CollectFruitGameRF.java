package aisoai.screens.gamescreeens.collectfruitgame;

import com.smartfoxserver.v2.entities.data.SFSObject;

import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;

public class CollectFruitGameRF extends TITRequestFactory
{
    public void sendGameAnswer(int index,int typeFinish)
    {
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.INDEX,index);
        toServerData.putInt(KJS.PARAM1,typeFinish);
        ExtensionRequest request=new ExtensionRequest(CMDRQ.GAMEANSWER_RQ,toServerData,room);
        sendRequest(request);
    }
}
