package aisoai.screens.gamescreeens.collectfruitgame;

import com.google.gson.JsonObject;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;
import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class CollectFruitGameControl extends TITGameControl
{

    @Override
    protected TextureRegion getBackground()
    {
        return CollectFruitGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        Monkey monkey=Monkey.createMonkey(this);
        getScene().attachChild(monkey);
        //getScene().registerTouchArea(monkey);

        MoveEventListenerBoard board=MoveEventListenerBoard.createMoveEventListenerBoard(this);
        getScene().attachChild(board);
        getScene().registerTouchArea(board);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                break;
            case "createNewRunSprite":
                int index=token.get(KJS.INDEX).getAsInt();
                int type=token.get(KJS.PARAM1).getAsInt();
                float runTime=token.get(KJS.PARAM2).getAsFloat();
                int postion=token.get(KJS.PARAM3).getAsInt();
                RunSprite runSprite =RunSprite.createRunSprite(type,postion,this);
                runSprite.setIndex(index);
                setMove1RunSprite(runSprite,runTime);
                getScene().attachChild(runSprite);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;

        }
    }

    private void setMove1RunSprite(RunSprite runSprite, final float runTime)
    {
        float toY=CollectFruitGameConfig.FRUITTO_Y1();
        float subRunTime=CollectFruitGameConfig.SUBRUN_TIME1(runTime);
        MoveYModifier moveModifier=new MoveYModifier(subRunTime,runSprite.getY(),toY);

        IModifier.IModifierListener<IEntity> modifierListener=
                new IModifier.IModifierListener<IEntity>()
                {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity){}

                    @Override
                    public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
                    {
                        RunSprite thisRunSprite=(RunSprite) iEntity;
                        if(checkCollideMonkey(thisRunSprite))
                            collideEvent(thisRunSprite);
                        else
                            setMove2RunSprite(thisRunSprite,runTime);
                    }
                };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        runSprite.registerEntityModifier(moveModifier);
    }

    private void setMove2RunSprite(RunSprite runSprite,final float runTime)
    {
        float toY=CollectFruitGameConfig.FRUITTO_Y2();
        float subRunTime=CollectFruitGameConfig.SUBRUN_TIME2(runTime);
        MoveYModifier moveModifier=new MoveYModifier(subRunTime,runSprite.getY(),toY);

        IModifier.IModifierListener<IEntity> modifierListener=
                new IModifier.IModifierListener<IEntity>()
                {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity){}

                    @Override
                    public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
                    {
                        RunSprite thisRunSprite=(RunSprite) iEntity;
                        if(checkCollideMonkey(thisRunSprite))
                            collideEvent(thisRunSprite);
                        else
                            setMove3RunSprite(thisRunSprite, runTime);
                    }
                };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        runSprite.registerEntityModifier(moveModifier);
    }

    private void setMove3RunSprite(RunSprite runSprite,final float runTime)
    {
        float toY=CollectFruitGameConfig.FRUITTO_Y3();
        float subRunTime=CollectFruitGameConfig.SUBRUN_TIME3(runTime);
        MoveYModifier moveModifier=new MoveYModifier(subRunTime,runSprite.getY(),toY);
        IModifier.IModifierListener<IEntity> modifierListener=
                new IModifier.IModifierListener<IEntity>()
                {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity){}

                    @Override
                    public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
                    {
                        RunSprite thisRunSprite=(RunSprite) iEntity;
                        if(checkCollideMonkey(thisRunSprite))
                            collideEvent(thisRunSprite);
                        else
                            runSpriteMoveFinishEvent(thisRunSprite);
                    }
                };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        runSprite.registerEntityModifier(moveModifier);
    }

    private boolean checkCollideMonkey(RunSprite runSprite)
    {
        Monkey monkey=getScene().getMonkey();
        return runSprite.collidesWith(monkey);
    }

    private void collideEvent(RunSprite runSprite)
    {
        if(runSprite.getClass().equals(Bomb.class))
        {
            playeFalseSound();
            getScene().getMonkey().reduce();
        }
        else
        {
            playTrueSound();
            getScene().getMonkey().increase();
        }
        getScene().detachChild(runSprite);
        int index=runSprite.getIndex();
        getRequestFactory().sendGameAnswer(index,1);
    }

    private void runSpriteMoveFinishEvent(RunSprite runSprite)
    {
        if(runSprite.getClass().equals(Bomb.class))
        {
            playeFalseSound();
        }
        else
        {
            playTrueSound();
            getScene().getMonkey().reduce();
        }
        getScene().detachChild(runSprite);
        int index=runSprite.getIndex();
        getRequestFactory().sendGameAnswer(index,2);
    }

    private void playTrueSound()
    {

    }

    private void playeFalseSound()
    {

    }

    public void monkeyMoveEvent(float newX)
    {
        float width=getScene().getMonkey().getWidth();
        getScene().getMonkey().setX(newX-width/2);
    }

    @Override
    public void finish()
    {

    }

    @Override
    public CollectFruitGameScene getScene()
    {
        return (CollectFruitGameScene) engine.getScene();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return CollectFruitGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new CollectFruitGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new CollectFruitGameRF();
    }

    @Override
    public CollectFruitGameActivity getActivity()
    {
        return (CollectFruitGameActivity) activity;
    }

    @Override
    public CollectFruitGameModel getModel()
    {
        return (CollectFruitGameModel) model;
    }

    @Override
    public CollectFruitGameRF getRequestFactory()
    {
        return (CollectFruitGameRF) requestFactory;
    }
}
