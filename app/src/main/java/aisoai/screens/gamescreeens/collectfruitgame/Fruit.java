package aisoai.screens.gamescreeens.collectfruitgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class Fruit extends RunSprite
{
    private int type;

    /*
        1:apple
        2:banana
        3:lemon
        4:orange
        5:pear
        6:strawberry
     */

    public Fruit(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                            TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    public static Fruit createFruit(int type,int postion,TITGameControl iControl)
    {
        TextureRegion textureRegion;
        switch (type)
        {
            case 1:
                textureRegion=CollectFruitGameResource.getApple();
                break;
            case 2:
                textureRegion=CollectFruitGameResource.getBanana();
                break;
            case 3:
                textureRegion=CollectFruitGameResource.getLemon();
                break;
            case 4:
                textureRegion=CollectFruitGameResource.getOrange();
                break;
            case 5:
                textureRegion=CollectFruitGameResource.getPear();
                break;
            case 6:
            default:
                textureRegion=CollectFruitGameResource.getStrawberry();
                break;
        }

        float x,y,width,height;
        width=CollectFruitGameConfig.FRUIT_WIDTH();
        height=CollectFruitGameConfig.FRUIT_HEIGHT();
        x=CollectFruitGameConfig.FRUIT_X(postion);
        y=CollectFruitGameConfig.FRUIT_Y();

        Fruit fruit=new Fruit(x,y,width,height,textureRegion,iControl);
        fruit.setType(type);
        return fruit;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }
}
