package aisoai.screens.gamescreeens.collectfruitgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class CollectFruitGameResource extends TITGameResource
{
    private static TextureRegion apple;
    private static TextureRegion banana;
    private static TextureRegion lemon;
    private static TextureRegion orange;
    private static TextureRegion pear;
    private static TextureRegion strawberry;
    private static TextureRegion monkey;
    private static TextureRegion bomb;
    private static TextureRegion transparent;
    private static TextureRegion background;

    public CollectFruitGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        apple=createTextureRegionFromAsset("collectfruitgame/apple.png",2);
        banana=createTextureRegionFromAsset("collectfruitgame/banana.png",2);
        lemon=createTextureRegionFromAsset("collectfruitgame/lemon.png",2);
        orange=createTextureRegionFromAsset("collectfruitgame/orange.png",2);
        pear=createTextureRegionFromAsset("collectfruitgame/pear.png",2);
        strawberry=createTextureRegionFromAsset("collectfruitgame/strawberry.png",2);
        monkey=createTextureRegionFromAsset("collectfruitgame/monkey.png",3);
        bomb=createTextureRegionFromAsset("collectfruitgame/bomb.png",2);
        transparent=createTextureRegionFromAsset("transparent.png",1);
        background=createTextureRegionFromAsset("collectfruitgame/background.png",3);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    @Override
    protected void clearResource()
    {
        apple=null;
        banana=null;
        orange=null;
        lemon=null;
        pear=null;
        strawberry=null;
        bomb=null;
        monkey=null;
        background=null;
    }

    public static TextureRegion getApple()
    {
        return apple;
    }

    public static TextureRegion getBanana()
    {
        return banana;
    }

    public static TextureRegion getOrange()
    {
        return orange;
    }

    public static TextureRegion getLemon()
    {
        return lemon;
    }

    public static TextureRegion getPear()
    {
        return pear;
    }

    public static TextureRegion getMonkey()
    {
        return monkey;
    }

    public static TextureRegion getStrawberry()
    {
        return strawberry;
    }

    public static TextureRegion getBomb()
    {
        return bomb;
    }

    public static TextureRegion getTransparent()
    {
        return transparent;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }
}
