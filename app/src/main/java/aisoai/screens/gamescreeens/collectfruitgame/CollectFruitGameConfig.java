package aisoai.screens.gamescreeens.collectfruitgame;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class CollectFruitGameConfig
{
    public static float MONKEY_WIDTH()
    {
        return 60* TITGameConfig.GS_WIDTH()/320;
    }

    public static float MONKEY_HEIGHT()
    {
        return 40*TITGameConfig.GS_WIDTH()/320;
    }

    public static float MONKEY_X()
    {
        return TITGameConfig.GS_WIDTH()/2-MONKEY_WIDTH()/2;
    }

    public static float MONKEY_Y()
    {
        return TITGameConfig.GS_HEIGHT()-MONKEY_HEIGHT();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float FRUIT_WIDTH()
    {
        return 40* TITGameConfig.GS_WIDTH()/320;
    }

    public static float FRUIT_HEIGHT()
    {
        return 40*TITGameConfig.GS_WIDTH()/320;
    }

    public static float FRUIT_X(int postion)
    {
        return (postion-0.5f)*TITGameConfig.GS_WIDTH()/8-FRUIT_WIDTH()/2;
    }

    public static float FRUIT_Y()
    {
        return -MONKEY_HEIGHT()/2;
    }

    public static float FRUITTO_Y1()
    {
        return TITGameConfig.GS_HEIGHT()-(2*MONKEY_HEIGHT()+FRUIT_HEIGHT());
    }

    public static float FRUITTO_Y2()
    {
        return TITGameConfig.GS_HEIGHT()-(2*MONKEY_HEIGHT()+FRUIT_HEIGHT())/2;
    }

    public static float FRUITTO_Y3()
    {
        return TITGameConfig.GS_HEIGHT();
    }

    public static float SUBRUN_TIME1(float runTime)
    {
        return runTime*FRUITTO_Y1()/TITGameConfig.GS_HEIGHT();
    }

    public static float SUBRUN_TIME2(float runTime)
    {
        return runTime*(FRUITTO_Y2()-FRUITTO_Y1())/TITGameConfig.GS_HEIGHT();
    }
    public static float SUBRUN_TIME3(float runTime)
    {
        return runTime*(FRUITTO_Y3()-FRUITTO_Y2())/TITGameConfig.GS_HEIGHT();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float BOARD_WIDTH()
    {
        return TITGameConfig.GS_WIDTH();
    }

    public static float BOARD_HEIGHT()
    {
        return 150*TITGameConfig.GS_WIDTH()/320;
    }

    public static float BOARD_X()
    {
        return 0;
    }

    public static float BOARD_Y()
    {
        return TITGameConfig.GS_HEIGHT()-BOARD_HEIGHT();
    }
}
