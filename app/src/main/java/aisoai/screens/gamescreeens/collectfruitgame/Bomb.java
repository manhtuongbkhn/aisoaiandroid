package aisoai.screens.gamescreeens.collectfruitgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class Bomb extends RunSprite
{

    public Bomb(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                            TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    public static Bomb createBomb(int postion,TITGameControl iControl)
    {
        TextureRegion textureRegion=CollectFruitGameResource.getBomb();
        float x,y,width,height;
        width=CollectFruitGameConfig.FRUIT_WIDTH();
        height=CollectFruitGameConfig.FRUIT_HEIGHT();
        x=CollectFruitGameConfig.FRUIT_X(postion);
        y=CollectFruitGameConfig.FRUIT_Y();

        Bomb bomb=new Bomb(x,y,width,height,textureRegion,iControl);
        return bomb;
    }
}
