package aisoai.screens.gamescreeens.collectfruitgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITCoordinate;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class Monkey extends TITSprite
{

    public Monkey(float x, float y, float width, float height,TextureRegion textureRegion,
                                                                            TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public CollectFruitGameControl getControl()
    {
        return (CollectFruitGameControl) control;
    }

    public static Monkey createMonkey(TITGameControl iControl)
    {
        float x,y,width,height;
        width=CollectFruitGameConfig.MONKEY_WIDTH();
        height=CollectFruitGameConfig.MONKEY_HEIGHT();
        x=CollectFruitGameConfig.MONKEY_X();
        y=CollectFruitGameConfig.MONKEY_Y();

        TextureRegion textureRegion=CollectFruitGameResource.getMonkey();

        Monkey monkey=new Monkey(x,y,width,height,textureRegion,iControl);
        return monkey;
    }

    public TITCoordinate getScaleCenter()
    {
        float scaleX=getWidth()/2;
        float scaleY=getHeight();
        return new TITCoordinate(scaleX,scaleY);
    }

    public void reduce()
    {
        TITCoordinate scaleCenter=getScaleCenter();
        setScaleCenter(scaleCenter.getX(),scaleCenter.getY());
        float currentScale=getScaleX();
        if(currentScale>0.7f)
            setScale(currentScale*0.9f);
    }

    public void increase()
    {
        TITCoordinate scaleCenter=getScaleCenter();
        setScaleCenter(scaleCenter.getX(),scaleCenter.getY());
        float currentScale=getScaleX();
        if(currentScale<2.0f)
            setScale(currentScale*1.1f);
    }
}
