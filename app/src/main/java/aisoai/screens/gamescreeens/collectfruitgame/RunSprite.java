package aisoai.screens.gamescreeens.collectfruitgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class RunSprite extends TITSprite
{
    protected int index;

    public RunSprite(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                            TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public CollectFruitGameControl getControl()
    {
        return (CollectFruitGameControl) control;
    }

    public static RunSprite createRunSprite(int type,int postion,TITGameControl iControl)
    {
        if(type==7)
            return Bomb.createBomb(postion,iControl);
        else
            return Fruit.createFruit(type,postion,iControl);
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
