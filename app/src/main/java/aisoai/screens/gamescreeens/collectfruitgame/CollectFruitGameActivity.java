package aisoai.screens.gamescreeens.collectfruitgame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;
import aisoai.screens.titentities.TITRequestFactory;

public class CollectFruitGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getCollectFruitGameControl();
    }

    @Override
    public CollectFruitGameControl getControl()
    {
        return (CollectFruitGameControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new CollectFruitGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new CollectFruitGameScene();
    }
}
