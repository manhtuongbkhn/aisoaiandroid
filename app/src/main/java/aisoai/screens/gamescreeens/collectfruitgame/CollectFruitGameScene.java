package aisoai.screens.gamescreeens.collectfruitgame;

import org.andengine.entity.IEntity;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class CollectFruitGameScene extends TITScene
{
    private Monkey monkey;

    @Override
    protected CollectFruitGameControl getControl()
    {
        return (CollectFruitGameControl) control ;
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();

        if(inputClass.equals(Monkey.class))
        {
            Monkey iMonkey =(Monkey) iEntity;
            monkey=iMonkey;
        }
        super.attachChild(iEntity);
    }

    public Monkey getMonkey()
    {
        return monkey;
    }
}
