package aisoai.screens.gamescreeens.collectfruitgame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class MoveEventListenerBoard extends TITSprite
{

    public MoveEventListenerBoard(float x, float y,float width,float height,TextureRegion
                                                            textureRegion,TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_MOVE)
        {
            float postionX=motionEvent.getX();
            getControl().monkeyMoveEvent(postionX);
        }
        return true;
    }

    public static MoveEventListenerBoard createMoveEventListenerBoard(TITGameControl iControl)
    {
        TextureRegion textureRegion=CollectFruitGameResource.getTransparent();
        float x,y,width,height;
        x=CollectFruitGameConfig.BOARD_X();
        y=CollectFruitGameConfig.BOARD_Y();
        width=CollectFruitGameConfig.BOARD_WIDTH();
        height=CollectFruitGameConfig.BOARD_HEIGHT();

        MoveEventListenerBoard board=new MoveEventListenerBoard
                                                        (x,y,width,height,textureRegion,iControl);
        return board;
    }

    @Override
    public CollectFruitGameControl getControl()
    {
        return (CollectFruitGameControl) control;
    }
}
