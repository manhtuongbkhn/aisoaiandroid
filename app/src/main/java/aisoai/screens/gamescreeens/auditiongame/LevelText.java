package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.opengl.font.Font;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITText;

public class LevelText extends TITText
{

    public LevelText(float x,float y,Font font,String text,int textMax,TITGameControl inputControl)
    {
        super(x,y,font,text,textMax,inputControl);
    }

    public static LevelText createLevelText(String text,TITGameControl inputControl)
    {
        int textCharMax=7;
        Float x,y,width,height;

        Font font=AuditionGameResource.getLevelTextFont();
        width=(float) font.getStringWidth(text);
        height=(float) font.getLineHeight();
        x=AuditionGameConfig.LEVELTEXT_CENTERX()-width/2;
        y=AuditionGameConfig.LEVELTEXT_CENTERY()-height/2;
        LevelText levelText=new LevelText(x,y,font,text,textCharMax,inputControl);
        levelText.setHorizontalAlign(TITGameConfig.HORIZONTAL_ALIGN());
        return levelText;
    }
}
