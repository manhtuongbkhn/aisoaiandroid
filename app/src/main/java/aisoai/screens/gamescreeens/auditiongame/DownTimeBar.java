package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class DownTimeBar extends TITSprite
{

    public DownTimeBar(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static DownTimeBar createDownTimeBar(TITGameControl inputControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=AuditionGameResource.getDownTimeBar();
        width=AuditionGameConfig.DOWNTIMEBAR_WIDTH();
        height=AuditionGameConfig.DOWNTIMEBAR_HEIGHT();
        x=AuditionGameConfig.DOWNTIMEBAR_X();
        y=AuditionGameConfig.DOWNTIMEBAR_Y();

        DownTimeBar downTimeBar=new DownTimeBar(x,y,width,height,textureRegion,inputControl);
        return downTimeBar;
    }
}
