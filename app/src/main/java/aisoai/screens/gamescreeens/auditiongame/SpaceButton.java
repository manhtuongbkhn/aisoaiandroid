package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class SpaceButton extends TITSprite
{

    public SpaceButton(float x, float y, float width, float height, TextureRegion textureRegion,
                       TITGameControl inputControl)
    {
        super(x,y,width,height,textureRegion,inputControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        if(event.getAction()== TouchEvent.ACTION_UP)
        {
            getControl().spaceButtonTouchEvent(this);
        }
        return true;
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static SpaceButton createTouchButton(TITGameControl inputControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=AuditionGameResource.getTouchButton();
        width=AuditionGameConfig.TOUCHBT_WIDTH();
        height=AuditionGameConfig.TOUCHBT_HEIGHT();
        x=AuditionGameConfig.TOUCHBT_X();
        y=AuditionGameConfig.TOUCHBT_Y();
        SpaceButton touchButton=new SpaceButton(x,y,width,height,textureRegion,inputControl);
        return touchButton;
    }
}
