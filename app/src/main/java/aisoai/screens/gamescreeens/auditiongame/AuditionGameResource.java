package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.audio.music.Music;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class AuditionGameResource extends TITGameResource
{
    private static TiledTextureRegion direction;
    private static TextureRegion directionButton;
    private static TextureRegion touchButton;
    private static TextureRegion downTimeBar;
    private static TextureRegion greenCircle;
    private static TextureRegion miss;
    private static TextureRegion cool;
    private static TextureRegion creat;
    private static TextureRegion perfect;
    private static TextureRegion border;
    private static TextureRegion background;

    private static Music music;
    private static Font levelTextFont;
    public AuditionGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {
        levelTextFont=createFontFromAsset(AuditionGameConfig.LEVELTEXT_FONT_SIZE(),
                AuditionGameConfig.LEVELTEXT_COLOR,"times.ttf");
    }

    @Override
    protected void initPictures()
    {
        direction=createTiledTextureRegionFromAsset("auditiongame/direction.png",5,1,3);
        directionButton=createTextureRegionFromAsset("direction_button.png",2);
        touchButton=createTextureRegionFromAsset("touch_button.png",2);
        downTimeBar=createTextureRegionFromAsset("auditiongame/down_time_bar.png",3);
        greenCircle=createTextureRegionFromAsset("auditiongame/green_circle.png",2);
        miss=createTextureRegionFromAsset("auditiongame/miss.png",3);
        cool=createTextureRegionFromAsset("auditiongame/cool.png",3);
        creat=createTextureRegionFromAsset("auditiongame/creat.png",3);
        perfect=createTextureRegionFromAsset("auditiongame/perfect.png",3);
        border=createTextureRegionFromAsset("border.png",3);
        background=createTextureRegionFromAsset("auditiongame/background.png",4);
    }


    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {
        music=createMusicFromAsset("auditiongame/em_cua_ngay_hom_qua.mp3");
    }

    @Override
    public void clearResource()
    {
        direction=null;
        directionButton=null;
        touchButton=null;
        downTimeBar=null;
        greenCircle=null;
        miss=null;
        cool=null;
        creat=null;
        perfect=null;
        border=null;
        background=null;

        music=null;

        levelTextFont=null;
    }

    public static TiledTextureRegion getDirection()
    {
        return direction.deepCopy();
    }

    public static TextureRegion getDirectionButton()
    {
        return directionButton.deepCopy();
    }

    public static TextureRegion getTouchButton()
    {
        return touchButton;
    }

    public static TextureRegion getDownTimeBar()
    {
        return downTimeBar;
    }

    public static TextureRegion getGreenCircle()
    {
        return greenCircle;
    }

    public static TextureRegion getMiss()
    {
        return miss;
    }

    public static TextureRegion getCool()
    {
        return cool;
    }

    public static TextureRegion getCreat()
    {
        return creat;
    }

    public static TextureRegion getPerfect()
    {
        return perfect;
    }

    public static TextureRegion getBorder()
    {
        return border;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public static Music getMusic()
    {
        return music;
    }

    public static Font getLevelTextFont()
    {
        return levelTextFont;
    }
}
