package aisoai.screens.gamescreeens.auditiongame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class AuditionGameActivity extends TITGameActivity
{

    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getAuditionGameControl();
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new AuditionGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new AuditionGameScene();
    }
}
