package aisoai.screens.gamescreeens.auditiongame;

import com.google.gson.JsonArray;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;

public class AuditionGameRF extends TITRequestFactory
{
    public void sendGameAnswer(int index,JsonArray answerJsonArr,float postionPercent)
    {
        SFSArray answerSFSArr=covert(answerJsonArr);
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.INDEX,index);
        toServerData.putSFSArray(KJS.PARAM1,answerSFSArr);
        toServerData.putFloat(KJS.PARAM2,postionPercent);
        ExtensionRequest request=new ExtensionRequest(CMDRQ.GAMEANSWER_RQ,toServerData,room);
        sendRequest(request);
    }

    public SFSArray covert(JsonArray jsonArray)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<jsonArray.size();i++)
        {
            int answer=jsonArray.get(i).getAsInt();
            sfsArray.addInt(answer);
        }
        return sfsArray;
    }
}
