package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class Border extends TITSprite
{

    public Border(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public TITGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static Border createBorder(TITGameControl inputControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=AuditionGameResource.getBorder();
        width=AuditionGameConfig.BORDER_WIDTH();
        height=AuditionGameConfig.BORDER_HEIGHT();
        x=AuditionGameConfig.BORDER_X();
        y=AuditionGameConfig.BORDER_Y();

        Border border=new Border(x,y,width,height,textureRegion,inputControl);
        return  border;
    }
}
