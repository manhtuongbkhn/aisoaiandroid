package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITAnimatedSprite;
import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class DirectionQuestion extends TITAnimatedSprite
{
    private int directionQuestion;

    public DirectionQuestion(float x, float y, float width, float height, TiledTextureRegion tiledTextureRegion,
                             TITGameControl inputControl)
    {
        super(x,y,width,height,tiledTextureRegion,inputControl);
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static DirectionQuestion createDirectionQuestion(int postion,TITGameControl inputControl)
    {
        float x,y,width,height,rotation;
        TiledTextureRegion tiledTextureRegion=AuditionGameResource.getDirection();

        width=AuditionGameConfig.DIRECTION_WIDTH();
        height=AuditionGameConfig.DIRECTION_HEIGHT();

        switch(postion)
        {
            case 1:
                x=AuditionGameConfig.DIRECTION1_X();
                break;
            case 2:
                x=AuditionGameConfig.DIRECTION2_X();
                break;
            case 3:
                x=AuditionGameConfig.DIRECTION3_X();
                break;
            case 4:
                x=AuditionGameConfig.DIRECTION4_X();
                break;
            case 5:
                x=AuditionGameConfig.DIRECTION5_X();
                break;
            case 6:
                x=AuditionGameConfig.DIRECTION6_X();
                break;
            case 7:
                x=AuditionGameConfig.DIRECTION7_X();
                break;
            case 8:
            default:
                x=AuditionGameConfig.DIRECTION8_X();
                break;
        }

        y=AuditionGameConfig.DIRECTION_Y();
        DirectionQuestion directionQuestion=new DirectionQuestion(x,y,width,height,
                                                                    tiledTextureRegion,inputControl);
        directionQuestion.setDirectionQuestion(0);
        return directionQuestion;
    }

    public void setDirectionQuestion(int inputDirectionQuestion)
    {
        switch (inputDirectionQuestion)
        {
            case 0:
                setCurrentTileIndex(0);
                setRotation(0);
                break;
            case 1:
                setCurrentTileIndex(1);
                setRotation(0);
                break;
            case 2:
                setCurrentTileIndex(1);
                setRotation(90);
                break;
            case 3:
                setCurrentTileIndex(1);
                setRotation(180);
                break;
            case 4:
                setCurrentTileIndex(1);
                setRotation(270);
                break;
            case 5:
                setCurrentTileIndex(2);
                setRotation(0);
                break;
            case 6:
                setCurrentTileIndex(2);
                setRotation(90);
                break;
            case 7:
                setCurrentTileIndex(2);
                setRotation(180);
                break;
            case 8:
                setCurrentTileIndex(2);
                setRotation(270);
                break;
            default:
                break;
        }
        this.directionQuestion=inputDirectionQuestion;
    }

    public void changeToAnswerDirection(boolean answer)
    {
        if(directionQuestion!=0)
        {
            switch (directionQuestion)
            {
                case 0:
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                case 5:
                    setRotation(180);
                    break;
                case 6:
                    setRotation(270);
                    break;
                case 7:
                    setRotation(0);
                    break;
                case 8:
                    setRotation(90);
                    break;
            }

            if (answer)
                setCurrentTileIndex(3);
            else
                setCurrentTileIndex(4);
            directionQuestion = 0;
        }
    }
}
