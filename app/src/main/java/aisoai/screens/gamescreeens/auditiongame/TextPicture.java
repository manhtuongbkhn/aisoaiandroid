package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class TextPicture extends TITSprite
{
    private String content;

    public TextPicture(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static TextPicture createTextPicture(String content,TITGameControl inputControl)
    {
        float x,y,widht,height;
        widht=AuditionGameConfig.TEXTPICTURE_WIDTH();
        height=AuditionGameConfig.TEXTPICTURE_HEIGHT();
        x=AuditionGameConfig.TEXTPICTURE_X();
        y=AuditionGameConfig.TEXTPICTURE_Y();

        TextureRegion textureRegion;
        switch (content)
        {
            case "miss":
                textureRegion=AuditionGameResource.getMiss();
                break;
            case "cool":
                textureRegion=AuditionGameResource.getCool();
                break;
            case "creat":
                textureRegion=AuditionGameResource.getCreat();
                break;
            case "perfect":
            default:
                textureRegion=AuditionGameResource.getPerfect();
                break;
        }
        TextPicture textPicture=new TextPicture(x,y,widht,height,textureRegion,inputControl);
        textPicture.setContent(content);
        return textPicture;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
