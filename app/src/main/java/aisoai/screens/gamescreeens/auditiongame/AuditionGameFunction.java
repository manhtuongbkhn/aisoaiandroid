package aisoai.screens.gamescreeens.auditiongame;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;

public class AuditionGameFunction
{
    public static float directionToRotation(int direction)
    {
        switch (direction)
        {
            case 1:
                return 0;
            case 2:
                return 90;
            case 3:
                return 180;
            case 4:
            default:
                return 270;
        }
    }

    public static int getAnswerDirection(int questionDirection)
    {
        switch (questionDirection)
        {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 1;
            case 8:
            default:
                return 2;
        }
    }

    public static boolean checkAnswerVsQuestion(JsonArray answerArr,JsonArray questionArr)
    {
        JsonArray newQuestionArr=new JsonArray();
        for(int i=0;i<questionArr.size();i++)
        {
            int question=questionArr.get(i).getAsInt();
            int answer= getAnswerDirection(question);
            newQuestionArr.add(new JsonPrimitive(answer));
        }
        return answerArr.equals(newQuestionArr);
    }

    public static int getBeginIndex(int size)
    {
        int begin;
        switch(size)
        {
            case 1:
            case 2:
                begin = 4;
                break;
            case 3:
            case 4:
                begin = 3;
                break;
            case 5:
            case 6:
                begin = 2;
                break;
            case 7:
            case 8:
                begin = 1;
                break;
            default:
                begin=-1;
                break;
        }
        return begin;
    }

    public static String getLevelText(Integer level)
    {
        String text;
        switch (level)
        {
            case 0:
                text="";
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                text="Level "+level.toString();
                break;
            case 8:
            default:
                text="finish";
                break;
        }
        return text;
    }
}
