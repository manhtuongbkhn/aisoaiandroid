package aisoai.screens.gamescreeens.auditiongame;

import com.google.gson.JsonArray;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class AuditionGameModel extends TITGameModel
{
    private int currentIndex;
    private GameStatus gameStatus;
    private JsonArray questionJsonArray;
    private JsonArray answerJsonArray;

    public int getCurrentIndex()
    {
        return currentIndex;
    }

    public void setCurrentIndex(int inputCurrentIndex)
    {
        this.currentIndex =inputCurrentIndex;
    }

    public AuditionGameModel()
    {
        gameStatus=GameStatus.SLEEPING;
    }

    public GameStatus getGameStatus()
    {
        return gameStatus;
    }

    public void setGameStatus(GameStatus inputGameStatus)
    {
        this.gameStatus = inputGameStatus;
    }

    public JsonArray getQuestionJsonArray()
    {
        return questionJsonArray;
    }

    public void setQuestionJsonArray(JsonArray inputQuestionJsonArray)
    {
        this.questionJsonArray = inputQuestionJsonArray;
    }

    public JsonArray getAnswerJsonArray()
    {
        return answerJsonArray;
    }

    public void setAnswerJsonArray(JsonArray inputAnswerJsonArray)
    {
        this.answerJsonArray=inputAnswerJsonArray;
    }

    public boolean checkAnswerMax()
    {
        if(answerJsonArray.size()>=questionJsonArray.size())
            return true;
        else
            return false;
    }
}
