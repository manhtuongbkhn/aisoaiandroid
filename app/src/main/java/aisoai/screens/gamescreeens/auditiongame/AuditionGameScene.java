package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.entity.IEntity;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class AuditionGameScene extends TITScene
{
    private ArrayList<DirectionQuestion> directionQuestionArr;
    private RunCircle runCircle;
    private LevelText levelText;
    public AuditionGameScene()
    {
        super();
        directionQuestionArr=new ArrayList<DirectionQuestion>();
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();

        if(inputClass.equals(DirectionQuestion.class))
        {
            DirectionQuestion directionQuestion =(DirectionQuestion) iEntity;
            directionQuestionArr.add(directionQuestion);
        }

        if(inputClass.equals(RunCircle.class))
        {
            RunCircle inputRunCircle=(RunCircle) iEntity;
            this.runCircle=inputRunCircle;
        }

        if(inputClass.equals(LevelText.class))
        {
            LevelText inputLevelText=(LevelText) iEntity;
            this.levelText=inputLevelText;
        }

        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();
        if(inputClass.equals(DirectionQuestion.class))
        {
            DirectionQuestion directionQuestion =(DirectionQuestion) iEntity;
            directionQuestionArr.remove(directionQuestion);
        }

        if(inputClass.equals(RunCircle.class))
        {
            RunCircle inputRunCircle=(RunCircle) iEntity;
        }

        if(inputClass.equals(LevelText.class))
        {
            LevelText inputLevelText=(LevelText) iEntity;
            this.levelText=null;
        }

        return super.detachChild(iEntity);

    }

    public ArrayList<DirectionQuestion> getDirectionQuestionArr()
    {
        return directionQuestionArr;
    }

    public DirectionQuestion getDirectionQuestion(int index)
    {
        if(index>directionQuestionArr.size())
            return null;
        else
            return directionQuestionArr.get(index-1);
    }

    public void clearDirectionQuestion()
    {
        for(int i=0;i<directionQuestionArr.size();i++)
        {
            DirectionQuestion directionQuestion=directionQuestionArr.get(i);
            directionQuestion.setDirectionQuestion(0);
        }
    }

    public RunCircle getRunCircle()
    {
        return runCircle;
    }

    public LevelText getLevelText()
    {
        return levelText;
    }

    protected AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }
}
