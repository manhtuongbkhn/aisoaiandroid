package aisoai.screens.gamescreeens.auditiongame;

import android.graphics.Color;

import aisoai.config.ClientConfig;
import aisoai.config.UIDefine;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class AuditionGameConfig
{
    public static float BORDER_WIDTH()
    {
        return 320*TITGameConfig.GS_WIDTH()/320;
    }

    public static float BORDER_HEIGHT()
    {
        return 60*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float BORDER_X()
    {
        return 0;
    }

    public static float BORDER_Y()
    {
        return 0;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////
    public static float DIRECTIONBT_WIDTH()
    {
        return 60*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DIRECTIONBT_HEIGHT()
    {
        return DIRECTIONBT_WIDTH();
    }

    public static float DIRECTIONBT1_X()
    {
        return 215*TITGameConfig.GS_WIDTH()/320-DIRECTIONBT_WIDTH()/2;
    }

    public static float DIRECTIONBT1_Y()
    {
        return 285*TITGameConfig.GS_HEIGHT()/390-DIRECTIONBT_HEIGHT()/2;
    }

    public static float DIRECTIONBT2_X()
    {
        return 285*TITGameConfig.GS_WIDTH()/320-DIRECTIONBT_WIDTH()/2;
    }

    public static float DIRECTIONBT2_Y()
    {
        return 355*TITGameConfig.GS_HEIGHT()/390-DIRECTIONBT_HEIGHT()/2;
    }

    public static float DIRECTIONBT3_X()
    {
        return 215*TITGameConfig.GS_WIDTH()/320-DIRECTIONBT_WIDTH()/2;
    }

    public static float DIRECTIONBT3_Y()
    {
        return 355*TITGameConfig.GS_HEIGHT()/390-DIRECTIONBT_HEIGHT()/2;
    }

    public static float DIRECTIONBT4_X()
    {
        return 145*TITGameConfig.GS_WIDTH()/320-DIRECTIONBT_WIDTH()/2;
    }

    public static float DIRECTIONBT4_Y()
    {
        return 355*TITGameConfig.GS_HEIGHT()/390-DIRECTIONBT_HEIGHT()/2;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float DIRECTION_WIDTH()
    {
        return 30*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DIRECTION_HEIGHT()
    {
        return DIRECTION_WIDTH();
    }

    public static float DIRECTION1_X()
    {
        return 20*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION2_X()
    {
        return 60*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION3_X()
    {
        return 100*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION4_X()
    {
        return 140*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION5_X()
    {
        return 180*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION6_X()
    {
        return 220*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION7_X()
    {
        return 260*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION8_X()
    {
        return 300*TITGameConfig.GS_WIDTH()/320-DIRECTION_WIDTH()/2;
    }

    public static float DIRECTION_Y()
    {
        return 30*TITGameConfig.GS_HEIGHT()/390-DIRECTION_HEIGHT()/2;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float TOUCHBT_WIDTH()
    {
        return 90*TITGameConfig.GS_WIDTH()/320;
    }

    public static float TOUCHBT_HEIGHT()
    {
        return TOUCHBT_WIDTH();
    }

    public static float TOUCHBT_X()
    {
        return 55*TITGameConfig.GS_WIDTH()/320-TOUCHBT_WIDTH()/2;
    }

    public static float TOUCHBT_Y()
    {
        return 320*TITGameConfig.GS_HEIGHT()/390-TOUCHBT_WIDTH()/2;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float DOWNTIMEBAR_WIDTH()
    {
        return 200*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DOWNTIMEBAR_HEIGHT()
    {
        return 30*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DOWNTIMEBAR_X()
    {
        return 160*TITGameConfig.GS_WIDTH()/320-DOWNTIMEBAR_WIDTH()/2;
    }

    public static float DOWNTIMEBAR_Y()
    {
        return 230*TITGameConfig.GS_HEIGHT()/390-DOWNTIMEBAR_HEIGHT()/2;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float CIRCLERUN_TIME=2f;

    public static float RUNCIRCLE_R()
    {
        return 15*TITGameConfig.GS_WIDTH()/320;
    }

    public static float RUNCIRCLE_STARTX()
    {
        return DOWNTIMEBAR_X()-RUNCIRCLE_R();
    }

    public static float RUNCIRCLE_ENDX()
    {
        return RUNCIRCLE_STARTX()+DOWNTIMEBAR_WIDTH();
    }

    public static float RUNCIRCLE_Y()
    {
        return 230*TITGameConfig.GS_HEIGHT()/390-RUNCIRCLE_R();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public static float TEXTPICTURE_WIDTH()
    {
        return 150*TITGameConfig.GS_WIDTH()/320;
    }

    public static float TEXTPICTURE_HEIGHT()
    {
        return TEXTPICTURE_WIDTH();
    }

    public static float TEXTPICTURE_X()
    {
        return 160*TITGameConfig.GS_WIDTH()/320-TEXTPICTURE_WIDTH()/2;
    }

    public static float TEXTPICTURE_Y()
    {
        return 180*TITGameConfig.GS_HEIGHT()/390-TEXTPICTURE_HEIGHT()/2;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    public static float LEVELTEXT_CENTERX()
    {
        return 160*TITGameConfig.GS_WIDTH()/320;
    }

    public static float LEVELTEXT_CENTERY()
    {
        return 85*TITGameConfig.GS_HEIGHT()/390;
    }

    public static int LEVELTEXT_FONT_SIZE()
    {
        Float f= UIDefine.GIGATICTEXT_SIZE()* ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    public static int LEVELTEXT_COLOR= Color.BLUE;
}
