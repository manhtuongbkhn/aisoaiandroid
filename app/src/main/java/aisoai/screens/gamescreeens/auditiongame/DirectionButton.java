package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class DirectionButton extends TITSprite
{
    private int direction;
    public DirectionButton(float x,float y,float width,float height,TextureRegion textureRegion,
                           TITGameControl inputControl)
    {
        super(x,y,width,height,textureRegion,inputControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        if(event.getAction()== TouchEvent.ACTION_UP)
        {
            getControl().directionButtonTouchEvent(this);
        }
        return true;
    }

    @Override
    public AuditionGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static DirectionButton createDirectionButton(int direction,TITGameControl inputControl)
    {
        float x,y,width,height,rotation;
        TextureRegion textureRegion=AuditionGameResource.getDirectionButton();
        width=AuditionGameConfig.DIRECTIONBT_WIDTH();
        height=AuditionGameConfig.DIRECTIONBT_HEIGHT();
        switch (direction)
        {
            case 1://up
                x=AuditionGameConfig.DIRECTIONBT1_X();
                y=AuditionGameConfig.DIRECTIONBT1_Y();
                System.out.println(y);
                rotation=0;
                break;
            case 2://right
                x=AuditionGameConfig.DIRECTIONBT2_X();
                y=AuditionGameConfig.DIRECTIONBT2_Y();
                rotation=90;
                break;
            case 3://down
                x=AuditionGameConfig.DIRECTIONBT3_X();
                y=AuditionGameConfig.DIRECTIONBT3_Y();
                rotation=180;
                break;
            case 4://left
            default:
                x=AuditionGameConfig.DIRECTIONBT4_X();
                y=AuditionGameConfig.DIRECTIONBT4_Y();
                rotation=270;
                break;
        }
        DirectionButton directionButton=new DirectionButton
                                                    (x,y,width,height,textureRegion,inputControl);
        directionButton.setRotation(rotation);
        directionButton.setDirection(direction);
        return directionButton;
    }

    public int getDirection()
    {
        return direction;
    }

    public void setDirection(int direction)
    {
        this.direction = direction;
    }
}
