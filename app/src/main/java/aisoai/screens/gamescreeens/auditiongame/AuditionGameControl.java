package aisoai.screens.gamescreeens.auditiongame;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;
import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class AuditionGameControl extends TITGameControl
{

    @Override
    protected TextureRegion getBackground()
    {
        return AuditionGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        initButton();
        initDirectionQuestion();
        initBorder();
        initLevelText();
        initDownTimeBar();
    }

    private void initButton()
    {
        DirectionButton button1=DirectionButton.createDirectionButton(1,this);
        DirectionButton button2=DirectionButton.createDirectionButton(2,this);
        DirectionButton button3=DirectionButton.createDirectionButton(3,this);
        DirectionButton button4=DirectionButton.createDirectionButton(4,this);

        getScene().attachChild(button1);
        getScene().attachChild(button2);
        getScene().attachChild(button3);
        getScene().attachChild(button4);

        SpaceButton touchButton= SpaceButton.createTouchButton(this);

        getScene().attachChild(touchButton);

        getScene().registerTouchArea(button1);
        getScene().registerTouchArea(button2);
        getScene().registerTouchArea(button3);
        getScene().registerTouchArea(button4);
        getScene().registerTouchArea(touchButton);
    }

    private void initDirectionQuestion()
    {
        for(int i=0;i<8;i++)
        {
            DirectionQuestion directionQuestion=DirectionQuestion.createDirectionQuestion(i+1,this);
            getScene().attachChild(directionQuestion);
        }
    }

    private void initBorder()
    {
        Border border=Border.createBorder(this);
        getScene().attachChild(border);
    }

    private void initLevelText()
    {
        LevelText levelText=LevelText.createLevelText("",this);
        getScene().attachChild(levelText);
    }

    private void initDownTimeBar()
    {
        DownTimeBar downTimeBar=DownTimeBar.createDownTimeBar(this);
        getScene().attachChild(downTimeBar);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                AuditionGameResource.getMusic().play();
                break;
            case "createNewDirectionQuestion":
                int index=token.get(KJS.INDEX).getAsInt();
                int level=token.get(KJS.PARAM1).getAsInt();
                JsonArray questionJsonArray=token.get(KJS.PARAM2).getAsJsonArray();
                getModel().setCurrentIndex(index);
                getModel().setQuestionJsonArray(questionJsonArray);
                getModel().setAnswerJsonArray(new JsonArray());
                showQuestionJsonArray(questionJsonArray);
                showLevelText(level);
                break;
            case "createNewRunCircle":
                float runTime=token.get(KJS.PARAM1).getAsInt();
                RunCircle runCircle=RunCircle.createRunCircle(this);
                setRunCircleMoveModifier(runCircle,runTime);
                getScene().attachChild(runCircle);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                boolean playing=token.get(KJS.PARAM2).getAsBoolean();
                if(playing)
                    getModel().setGameStatus(GameStatus.PLAYING);
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}

                if(getModel().getGameStatus()==GameStatus.FINISH)
                    break;

                if(getModel().getGameStatus()==GameStatus.PLAYING)
                {
                    showTextPicture("miss");
                    sendAnswer(-1f);
                }

                getModel().setGameStatus(GameStatus.SLEEPING);
                getScene().clearDirectionQuestion();
                showLevelText(0);
                break;
        }
    }

    private void showQuestionJsonArray(JsonArray questionJsonArray)
    {
        int size=questionJsonArray.size();
        int beginIndex=AuditionGameFunction.getBeginIndex(size);

        for(int i=0;i<size;i++)
        {
            int intDirectionQuestion=questionJsonArray.get(i).getAsInt();
            DirectionQuestion directionQuestion=getScene().getDirectionQuestion(beginIndex);
            directionQuestion.setDirectionQuestion(intDirectionQuestion);
            beginIndex++;
        }
    }

    private void setRunCircleMoveModifier(final RunCircle runCircle,float runTime)
    {
        float startX=AuditionGameConfig.RUNCIRCLE_STARTX();
        float endX=AuditionGameConfig.RUNCIRCLE_ENDX();
        MoveXModifier moveXModifier=new MoveXModifier(runTime,startX,endX);

        IModifier.IModifierListener modifierListener=new IModifier.IModifierListener()
        {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem)
            {
            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem)
            {
                RunCircle thisRuncircle=(RunCircle) pItem;
                getScene().detachChild(thisRuncircle);
            }
        };
        moveXModifier.addModifierListener(modifierListener);
        moveXModifier.setAutoUnregisterWhenFinished(true);
        runCircle.registerEntityModifier(moveXModifier);
    }

    public void directionButtonTouchEvent(DirectionButton directionButton)
    {
        if(getModel().getGameStatus()==GameStatus.PLAYING&&!getModel().checkAnswerMax())
        {
            JsonArray questionJsonArray=getModel().getQuestionJsonArray();
            JsonArray answerJsonArray=getModel().getAnswerJsonArray();

            int direction = directionButton.getDirection();
            answerJsonArray.add(new JsonPrimitive(direction));
            int currentIndex = getModel().getAnswerJsonArray().size();
            int beginIndex = AuditionGameFunction.getBeginIndex(questionJsonArray.size());
            DirectionQuestion directionQuestion = getScene().getDirectionQuestion
                                                                        (currentIndex+beginIndex-1);

            int question = getModel().getQuestionJsonArray().get(currentIndex-1).getAsInt();
            int answer = AuditionGameFunction.getAnswerDirection(question);

            if (direction == answer)
            {
                directionQuestion.changeToAnswerDirection(true);
            } else
            {
                getModel().setGameStatus(GameStatus.RESPONSED);
                directionQuestion.changeToAnswerDirection(false);
                showTextPicture("miss");
                sendAnswer(-1f);

            }
        }

    }

    public void spaceButtonTouchEvent(SpaceButton spaceButton)
    {
        if(getModel().getGameStatus()==GameStatus.PLAYING)
        {
            JsonArray answerArr=getModel().getAnswerJsonArray();
            JsonArray questionArr=getModel().getQuestionJsonArray();
            float postionPercent=getScene().getRunCircle().getPostionInDownTimeBarPercent();
            if(AuditionGameFunction.checkAnswerVsQuestion(answerArr,questionArr))
            {
                if(66<=postionPercent&&postionPercent<=74)
                    showTextPicture("perfect");

                if((60<=postionPercent&&postionPercent<66)||(74<postionPercent&&postionPercent<=80))
                    showTextPicture("creat");

                if((50<=postionPercent&&postionPercent<60)||(80<postionPercent&&postionPercent<=90))
                    showTextPicture("cool");

                if(50>postionPercent||postionPercent>90)
                    showTextPicture("miss");
            }
            else
            {
                showTextPicture("miss");
            }

            sendAnswer(postionPercent);

            getModel().setGameStatus(GameStatus.RESPONSED);
        }
    }


    private void showLevelText(Integer level)
    {
        String text=AuditionGameFunction.getLevelText(level);
        getScene().getLevelText().changeText(text);
    }

    private void showTextPicture(String content)
    {
        TextPicture textPicture=TextPicture.createTextPicture(content,this);
        float width,height;
        width=AuditionGameConfig.TEXTPICTURE_WIDTH();
        height=AuditionGameConfig.TEXTPICTURE_HEIGHT();
        ScaleAtModifier scaleAtModifier=new ScaleAtModifier(1f,1.0f,1.2f,width/2,height/2);
        IModifier.IModifierListener modifierListener=new IModifier.IModifierListener()
        {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem)
            {

            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem)
            {
                TextPicture thisTextPicture=(TextPicture) pItem;
                getScene().detachChild(thisTextPicture);
            }
        };

        scaleAtModifier.addModifierListener(modifierListener);
        scaleAtModifier.setAutoUnregisterWhenFinished(true);
        textPicture.registerEntityModifier(scaleAtModifier);
        getScene().attachChild(textPicture);
    }


    private void sendAnswer(float postionPercent)
    {
        int index=getModel().getCurrentIndex();
        JsonArray answerArr=getModel().getAnswerJsonArray();
        getRequestFactory().sendGameAnswer(index,answerArr,postionPercent);
    }

    @Override
    public void finish()
    {
        getModel().setGameStatus(GameStatus.FINISH);
        AuditionGameResource.getMusic().pause();
    }

    @Override
    public AuditionGameScene getScene()
    {
        return (AuditionGameScene) engine.getScene();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return AuditionGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new AuditionGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new AuditionGameRF();
    }

    @Override
    public AuditionGameActivity getActivity()
    {
        return (AuditionGameActivity) activity;
    }

    @Override
    public AuditionGameModel getModel()
    {
        return (AuditionGameModel) model;
    }

    @Override
    public AuditionGameRF getRequestFactory()
    {
        return (AuditionGameRF) requestFactory;
    }
}
