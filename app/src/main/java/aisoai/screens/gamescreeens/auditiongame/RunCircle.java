package aisoai.screens.gamescreeens.auditiongame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class RunCircle extends TITSprite
{

    public RunCircle(float x, float y, float width, float height, TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x,y,width,height,textureRegion,inputControl);
    }

    @Override
    public TITGameControl getControl()
    {
        return (AuditionGameControl) control;
    }

    public static RunCircle createRunCircle(TITGameControl inputControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=AuditionGameResource.getGreenCircle();
        width=AuditionGameConfig.RUNCIRCLE_R()*2;
        height=width;
        x=AuditionGameConfig.RUNCIRCLE_STARTX();
        y=AuditionGameConfig.RUNCIRCLE_Y();

        RunCircle runCircle=new RunCircle(x,y,width,height,textureRegion,inputControl);
        return  runCircle;
    }

    public float getPostionInDownTimeBarPercent()
    {
        float x=getX();
        float xDelta=x-AuditionGameConfig.RUNCIRCLE_STARTX();
        return xDelta*100/AuditionGameConfig.DOWNTIMEBAR_WIDTH();
    }
}
