package aisoai.screens.gamescreeens.dinosaurjumpgame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class DinosaurJumpGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getDinosaurJumpGameControl();
    }

    @Override
    public DinosaurJumpGameControl getControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new DinosaurJumpGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new DinosaurJumpGameSence();
    }
}
