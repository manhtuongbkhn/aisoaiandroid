package aisoai.screens.gamescreeens.dinosaurjumpgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class DinosaurJumpGameResource extends TITGameResource
{
    private static TextureRegion dinosaur;
    private static TextureRegion cactus1;
    private static TextureRegion cactus2;
    private static TextureRegion cactus3;
    private static TextureRegion background;
    private static TextureRegion directionButton;
    private static TextureRegion playButton;
    private static TextureRegion replayButton;
    private static TextureRegion transparent;

    public DinosaurJumpGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        dinosaur=createTextureRegionFromAsset("dinosaurjumpgame/dinosaur.png",2);
        cactus1=createTextureRegionFromAsset("dinosaurjumpgame/cactus1.png",2);
        cactus2=createTextureRegionFromAsset("dinosaurjumpgame/cactus2.png",3);
        cactus3=createTextureRegionFromAsset("dinosaurjumpgame/cactus3.png",4);
        background=createTextureRegionFromAsset("dinosaurjumpgame/background.png",4);
        directionButton =createTextureRegionFromAsset("direction_button.png",2);
        playButton =createTextureRegionFromAsset("play.png",2);
        replayButton =createTextureRegionFromAsset("replay.png",2);
        transparent=createTextureRegionFromAsset("transparent.png",1);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    public static TextureRegion getDinosaur()
    {
        return dinosaur;
    }

    public static TextureRegion getCactus1()
    {
        return cactus1;
    }

    public static TextureRegion getCactus2()
    {
        return cactus2;
    }

    public static TextureRegion getCactus3()
    {
        return cactus3;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public static TextureRegion getDirectionButton()
    {
        return directionButton;
    }

    public static TextureRegion getPlayButton()
    {
        return playButton;
    }

    public static TextureRegion getReplayButton()
    {
        return replayButton;
    }

    public static TextureRegion getTransparent()
    {
        return transparent;
    }

    @Override
    protected void clearResource()
    {
        dinosaur=null;
        cactus1=null;
        cactus2=null;
        cactus3=null;
        background=null;
        directionButton =null;
        playButton=null;
        replayButton=null;
        transparent=null;
    }
}
