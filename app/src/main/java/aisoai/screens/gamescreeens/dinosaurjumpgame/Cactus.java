package aisoai.screens.gamescreeens.dinosaurjumpgame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class Cactus extends TITSprite
{
    private int index;

    public Cactus(float x,float y,float width,float height,TextureRegion textureRegion,
                                                                            TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public TITGameControl getControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    public static Cactus createCactus(int index,int type,TITGameControl iControl)
    {
        float x,y,width,height;
        width=DinosaurJumpGameConfig.CACTUS_WIDTH();
        height=DinosaurJumpGameConfig.CACTUS_HEIGHT(type);
        x=DinosaurJumpGameConfig.CACTUS_STARTX();
        y=DinosaurJumpGameConfig.CACTUS_STARTY(type);
        TextureRegion textureRegion=null;
        if(type==1)
            textureRegion=DinosaurJumpGameResource.getCactus1();
        if(type==2)
            textureRegion=DinosaurJumpGameResource.getCactus2();
        if(type==3)
            textureRegion=DinosaurJumpGameResource.getCactus3();
        Cactus cactus=new Cactus(x,y,width,height,textureRegion,iControl);
        cactus.setIndex(index);
        return cactus;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
