package aisoai.screens.gamescreeens.dinosaurjumpgame;

import org.andengine.entity.IEntity;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class DinosaurJumpGameSence extends TITScene
{
    private Dinosaur dinosaur;
    private ArrayList<Cactus> cactusArr=new ArrayList<Cactus>();
    private PlayButton playButton;

    @Override
    public void attachChild(IEntity iEntity)
    {
        if(iEntity instanceof Dinosaur)
        {
            Dinosaur iDinosaur =(Dinosaur) iEntity;
            this.dinosaur=iDinosaur;
        }
        if(iEntity instanceof Cactus)
        {
            Cactus iCactus=(Cactus) iEntity;
            cactusArr.add(iCactus);
        }
        if(iEntity instanceof PlayButton)
        {
            PlayButton iPlayButton=(PlayButton) iEntity;
            this.playButton=iPlayButton;
        }
        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        if (iEntity instanceof Dinosaur)
        {
            Dinosaur iDinosaur = (Dinosaur) iEntity;
            this.dinosaur = null;
        }

        if(iEntity instanceof Cactus)
        {
            Cactus iCactus=(Cactus) iEntity;
            cactusArr.remove(iCactus);
        }
        if(iEntity instanceof PlayButton)
        {
            PlayButton iPlayButton=(PlayButton) iEntity;
            this.playButton=null;
        }
        return super.detachChild(iEntity);
    }

    @Override
    protected DinosaurJumpGameControl getControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    public Dinosaur getDinosaur()
    {
        return dinosaur;
    }

    public ArrayList<Cactus> getCactusArr()
    {
        return cactusArr;
    }

    public PlayButton getPlayButton()
    {
        return playButton;
    }
}
