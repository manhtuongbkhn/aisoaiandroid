package aisoai.screens.gamescreeens.dinosaurjumpgame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class TouchEventListenBoard extends TITSprite
{
    public TouchEventListenBoard(float x, float y, float width, float height, TextureRegion textureRegion,
                                 TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
        }
    }

    @Override
    public DinosaurJumpGameControl getControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    public static TouchEventListenBoard createBoard(TITGameControl iControl)
    {
        TextureRegion textureRegion=(TextureRegion) DinosaurJumpGameResource.getTransparent();
        float x,y,width,height;
        x=0;
        y=0;
        width= TITGameConfig.GS_WIDTH();
        height=TITGameConfig.GS_HEIGHT();
        TouchEventListenBoard board =new TouchEventListenBoard(x,y,width,height,textureRegion,
                                                                                        iControl);
        return board;
    }
}
