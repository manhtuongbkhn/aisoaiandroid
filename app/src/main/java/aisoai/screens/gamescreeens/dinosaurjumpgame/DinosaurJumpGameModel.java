package aisoai.screens.gamescreeens.dinosaurjumpgame;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class DinosaurJumpGameModel extends TITGameModel
{
    private int dieCount=0;

    private GameStatus gameStatus=GameStatus.NULL;

    public GameStatus getGameStatus()
    {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus)
    {
        this.gameStatus = gameStatus;
    }

    public int getDieCount()
    {
        return dieCount;
    }

    public void setDieCount(int dieCount)
    {
        this.dieCount = dieCount;
    }

    public void increaseDieCount()
    {
        this.dieCount++;
    }
}
