package aisoai.screens.gamescreeens.dinosaurjumpgame;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class DirectionButton extends TITSprite
{
    private int direction;

    public DirectionButton(float x, float y, float width, float height, TextureRegion textureRegion,
                           TITGameControl iControl)
    {
        super(x, y, width, height, textureRegion, iControl);
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        MotionEvent motionEvent=event.getMotionEvent();
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN)
        {
            TouchEventHandlerThread thread=new TouchEventHandlerThread(x,y);
            thread.start();
        }
        return true;
    }

    class TouchEventHandlerThread extends Thread
    {
        private float x;
        private float y;

        public TouchEventHandlerThread(float iX,float iY)
        {
            x=iX;
            y=iY;
            this.setPriority(Thread.MAX_PRIORITY);
        }

        @Override
        public void run()
        {
            getControl().buttonEvent(direction);
        }
    }

    @Override
    public DinosaurJumpGameControl getControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    public static DirectionButton createDirectionButton(int direction,TITGameControl iControl)
    {
        float x,y,width,height;
        width=DinosaurJumpGameConfig.DIRECTION_BUTTON_WIDTH();
        height=DinosaurJumpGameConfig.DIRECTION_BUTTON_HEIGHT();
        x=DinosaurJumpGameConfig.DIRECTION_BUTTON_X(direction);
        y=DinosaurJumpGameConfig.DIRECTION_BUTTON_Y();
        TextureRegion textureRegion=DinosaurJumpGameResource.getDirectionButton();
        DirectionButton directionButton =new DirectionButton(x,y,width,height,textureRegion,iControl);
        directionButton.setDirection(direction);
        if(direction==4)
            directionButton.setRotation(180);
        return directionButton;
    }

    public int getDirection()
    {
        return direction;
    }

    public void setDirection(int direction)
    {
        this.direction = direction;
    }
}
