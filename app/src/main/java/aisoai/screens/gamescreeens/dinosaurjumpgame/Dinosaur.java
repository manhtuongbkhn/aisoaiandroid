package aisoai.screens.gamescreeens.dinosaurjumpgame;

import org.andengine.opengl.texture.region.TextureRegion;

import java.util.ArrayList;

import aisoai.screens.gamescreeens.titentities.TITCoordinate;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.physicextension.TITAcceleration;
import aisoai.screens.gamescreeens.titentities.physicextension.TITPhysicSprite;
import aisoai.screens.gamescreeens.titentities.physicextension.TITVector;

public class Dinosaur extends TITPhysicSprite
{

    public Dinosaur(float x,float y,float width,float height,TextureRegion textureRegion,
                                                            TITGameControl iControl, float weight)
    {
        super(x, y, width, height, textureRegion, iControl, weight);
    }

    @Override
    public DinosaurJumpGameControl getControl()
    {
        return (DinosaurJumpGameControl) control;
    }

    public static Dinosaur createDinosaur(TITGameControl iControl)
    {
        float x,y,width,height;
        TextureRegion textureRegion=DinosaurJumpGameResource.getDinosaur();
        x=DinosaurJumpGameConfig.DINOSAUR_STARTX();
        y=DinosaurJumpGameConfig.DINOSAUR_STARTY();
        width=DinosaurJumpGameConfig.DINOSAUR_WIDTH();
        height=DinosaurJumpGameConfig.DINOSAUR_HEIGHT();
        Dinosaur dinosaur=new Dinosaur(x,y,width,height,textureRegion,iControl,1);
        return dinosaur;
    }

    @Override
    public void setCoordinate(float newX, float newY)
    {
        setX(newX);
        if(newY>DinosaurJumpGameConfig.DINOSAUR_STARTY())
        {
            setY(DinosaurJumpGameConfig.DINOSAUR_STARTY());
        }
        else
        {
            setY(newY);
        }
    }

    @Override
    public void afterSetCoordinate()
    {
        float y=getY();
        TITAcceleration jet=new TITAcceleration
                        ("jet",new TITVector(new TITCoordinate(0,-DinosaurJumpGameConfig.G())));
        if(y>=DinosaurJumpGameConfig.DINOSAUR_STARTY())
        {
            getSpeed().getVector().getTip().setY(0);
            registerAcceleration(jet);
        }
        else
        {
            unregisterAcceleration(jet);
        }

        ArrayList<Cactus> cactusArr=getControl().getScene().getCactusArr();
        for(int i=0;i<cactusArr.size();i++)
        {
            Cactus cactus=cactusArr.get(i);
            if(collidesWith(cactus))
            {
                getControl().dinosaurCollidesWidthEvent();
                break;
            }
        }
    }
}
