package aisoai.screens.gamescreeens.dinosaurjumpgame;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class DinosaurJumpGameConfig
{
    public static float mScale;

    public static float G()
    {
        return 9.8f*M();
    }

    public static float M()
    {
        return mScale*TITGameConfig.GS_HEIGHT();
    }

    public static float LANE_Y()
    {
        return 310*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float DINOSAUR_WIDTH()
    {
        return 50* TITGameConfig.GS_WIDTH()/320;
    }

    public static float DINOSAUR_HEIGHT()
    {
        return 80*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DINOSAUR_STARTX()
    {
        return 30*TITGameConfig.GS_WIDTH()/320f;
    }

    public static float DINOSAUR_STARTY()
    {
        return LANE_Y() - DINOSAUR_HEIGHT();
    }

    public static float CACTUS_WIDTH()
    {
        return 15*TITGameConfig.GS_WIDTH()/320;
    }

    public static float CACTUS_HEIGHT(int type)
    {
        if(type==1)
            return 35*TITGameConfig.GS_HEIGHT()/390;
        if (type==2)
            return 70*TITGameConfig.GS_HEIGHT()/390;
        if(type==3)
            return 105*TITGameConfig.GS_HEIGHT()/390;
        return 0;
    }

    public static float CACTUS_STARTX()
    {
        return TITGameConfig.GS_HEIGHT()-CACTUS_WIDTH();
    }

    public static float CACTUS_FINISHX()
    {
        return -CACTUS_WIDTH();
    }

    public static float CACTUS_STARTY(int direction)
    {
        return LANE_Y()-CACTUS_HEIGHT(direction);
    }

    public static float DIRECTION_BUTTON_WIDTH()
    {
        return 70*TITGameConfig.GS_WIDTH()/320;
    }

    public static float DIRECTION_BUTTON_HEIGHT()
    {
        return DIRECTION_BUTTON_WIDTH();
    }

    public static float DIRECTION_BUTTON_X(int direction)
    {
        if(direction==1)
            return 80*TITGameConfig.GS_WIDTH()/320- DIRECTION_BUTTON_WIDTH()/2;
        if(direction==4)
            return 240*TITGameConfig.GS_WIDTH()/320- DIRECTION_BUTTON_WIDTH()/2;
        return 0;
    }

    public static float DIRECTION_BUTTON_Y()
    {
        return 350*TITGameConfig.GS_HEIGHT()/390- DIRECTION_BUTTON_HEIGHT()/2;
    }

    public static float PLAY_BUTTON_WIDTH()
    {
        return 60*TITGameConfig.GS_WIDTH()/320;
    }

    public static float PLAY_BUTTON_HEIGHT()
    {
        return PLAY_BUTTON_WIDTH();
    }

    public static float PLAY_BUTTON_X()
    {
        return TITGameConfig.GS_WIDTH()/2-PLAY_BUTTON_WIDTH()/2;
    }

    public static float PLAY_BUTTON_Y()
    {
        return TITGameConfig.GS_HEIGHT()/2-PLAY_BUTTON_HEIGHT()/2;
    }
}
