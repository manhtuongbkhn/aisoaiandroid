package aisoai.screens.gamescreeens.choicemodgame;

import org.andengine.opengl.font.Font;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITText;

public class AnswerText extends TITText
{
    public AnswerText(float x,float y,Font font,String text, int textMax,TITGameControl iControl)
    {
        super(x, y, font, text, textMax, iControl);
    }

    public static AnswerText createAnswerText(int direction,TITGameControl iControl)
    {
        Integer mod=direction-1;
        String text="Dư "+mod.toString();

        int textCharMax=7;
        Float x,y,width,height;

        Font font=ChoiceModGameResource.getAnswerFont();
        width=(float) font.getStringWidth(text);
        height=(float) font.getLineHeight();

        switch (direction)
        {
            case 1:
                x=TITGameConfig.GS_WIDTH()-width;
                y=TITGameConfig.GS_HEIGHT()/2-height/2;
                break;
            case 2:
                x=TITGameConfig.GS_WIDTH()-width;
                y=TITGameConfig.GS_HEIGHT()-height;
                break;
            case 3:
                x=TITGameConfig.GS_WIDTH()/2-width/2;
                y=TITGameConfig.GS_HEIGHT()-height;
                break;
            case 4:
                x=0f;
                y=TITGameConfig.GS_HEIGHT()-height;
                break;
            case 5:
                x=0f;
                y=TITGameConfig.GS_HEIGHT()/2-height/2;
                break;
            case 6:
                x=0f;
                y=0f;
                break;
            case 7:
                x=TITGameConfig.GS_WIDTH()/2-width/2;
                y=0f;
                break;
            case 8:
            default:
                x=TITGameConfig.GS_WIDTH()-width;
                y=0f;
                break;
        }
        AnswerText answerText=new AnswerText(x,y,font,text,textCharMax,iControl);
        answerText.setHorizontalAlign(TITGameConfig.HORIZONTAL_ALIGN());
        return answerText;
    }
}
