package aisoai.screens.gamescreeens.choicemodgame;

import android.graphics.Color;

import aisoai.config.ClientConfig;
import aisoai.config.UIDefine;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class ChoiceModGameConfig
{
    public static float MODQUESTION_WIDTH()
    {
        return 100*TITGameConfig.GS_WIDTH()/320;
    }

    public static float MODQUESTION_HEIGHT()
    {
        return MODQUESTION_WIDTH();
    }

    public static float MODQUESTION_X()
    {
        return 160*TITGameConfig.GS_WIDTH()/320-MODQUESTION_WIDTH()/2;
    }

    public static float MODQUESTION_Y()
    {
        return 195*TITGameConfig.GS_HEIGHT()/390-MODQUESTION_HEIGHT()/2;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static float QUESTIONTEXT_CENTERX()
    {
        return MODQUESTION_WIDTH()/2;
    }

    public static float QUESTIONTEXT_CENTERY()
    {
        return MODQUESTION_HEIGHT()/2;
    }

    public static int QUESTION_FONT_SIZE()
    {
        Float f= UIDefine.HUGETEXT_SIZE()*ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    public static int QUESTIONTEXT_COLOR= Color.BLUE;

    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static int ANSWERFONT_SIZE()
    {
        Float f= UIDefine.HUGETEXT_SIZE()*ClientConfig.SCREEN_WIDTH_PX/320;
        return f.intValue();
    }

    public static int ANSWERTEXT_COLOR= Color.BLUE;
}
