package aisoai.screens.gamescreeens.choicemodgame;

import org.andengine.entity.IEntity;

import aisoai.screens.gamescreeens.titentities.TITScene;

public class ChoiceModGameScene extends TITScene
{
    private ModQuestion modQuestion;

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();

        if(inputClass.equals(ModQuestion.class))
        {
            ModQuestion iModQuestion=(ModQuestion) iEntity;
            modQuestion=iModQuestion;
        }
        super.attachChild(iEntity);
    }


    public ModQuestion getModQuestion()
    {
        return modQuestion;
    }

    protected ChoiceModGameControl getControl()
    {
        return (ChoiceModGameControl) control;
    }
}
