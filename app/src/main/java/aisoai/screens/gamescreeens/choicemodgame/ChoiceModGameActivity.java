package aisoai.screens.gamescreeens.choicemodgame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITGameResource;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class ChoiceModGameActivity extends TITGameActivity
{

    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getChoiceModGameControl();
    }

    @Override
    public ChoiceModGameControl getControl()
    {
        return (ChoiceModGameControl) control;
    }

    @Override
    public TITGameResource initGameResource()
    {
        return new ChoiceModGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new ChoiceModGameScene();
    }
}
