package aisoai.screens.gamescreeens.choicemodgame;

import org.andengine.entity.IEntity;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;
import aisoai.screens.gamescreeens.titentities.TITText;

public class ModQuestion extends TITSprite
{
    private TITText questionText;

    public ModQuestion(float x,float y,float width, float height,TextureRegion textureRegion,
                                                                        TITGameControl inputControl)
    {
        super(x,y,width,height,textureRegion,inputControl);
    }

    @Override
    public ChoiceModGameControl getControl()
    {
        return (ChoiceModGameControl) control;
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();

        if(inputClass.equals(QuestionText.class))
        {
            QuestionText iQuestionText =(QuestionText) iEntity;
            this.questionText=iQuestionText;
        }

        super.attachChild(iEntity);
    }

    public static ModQuestion createModQuestion(TITGameControl iControl)
    {
        float x,y,width,height;
        width=ChoiceModGameConfig.MODQUESTION_WIDTH();
        height=ChoiceModGameConfig.MODQUESTION_HEIGHT();
        x=ChoiceModGameConfig.MODQUESTION_X();
        y=ChoiceModGameConfig.MODQUESTION_Y();
        TextureRegion textureRegion=ChoiceModGameResource.getButton();
        ModQuestion modQuestion=new ModQuestion(x,y,width,height,textureRegion,iControl);
        modQuestion.initText();
        return modQuestion;
    }

    private void initText()
    {
        QuestionText iQuestionText=QuestionText.createQuestionText(control);
        attachChild(iQuestionText);
    }

    public void setText(String newText)
    {
        questionText.changeText(newText);
    }

    public void titReset()
    {
        setX(ChoiceModGameConfig.MODQUESTION_X());
        setY(ChoiceModGameConfig.MODQUESTION_Y());
        setText("");
    }
}

class QuestionText extends TITText
{
    public QuestionText(float x, float y,Font font, String text,int textMax,TITGameControl iControl)
    {
        super(x, y,font, text,textMax,iControl);
    }

    public static QuestionText createQuestionText(TITGameControl iControl)
    {
        int textCharMax=7;
        Float x,y,width,height;

        Font font=ChoiceModGameResource.getQuestionFont();
        width=(float) font.getStringWidth("");
        height=(float) font.getLineHeight();
        x=ChoiceModGameConfig.QUESTIONTEXT_CENTERX()-width/2;
        y=ChoiceModGameConfig.QUESTIONTEXT_CENTERY()-height/2;
        QuestionText questionText=new QuestionText(x,y,font,"",textCharMax,iControl);
        questionText.setHorizontalAlign(TITGameConfig.HORIZONTAL_ALIGN());
        return questionText;
    }
}