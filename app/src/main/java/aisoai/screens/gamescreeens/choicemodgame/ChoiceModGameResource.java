package aisoai.screens.gamescreeens.choicemodgame;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class ChoiceModGameResource extends TITGameResource
{
    private static TextureRegion button;
    private static TextureRegion transparent;
    private static TextureRegion background;

    private static Font questionFont;
    private static Font answerFont;

    public ChoiceModGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    @Override
    protected void initFonts()
    {
        questionFont=createFontFromAsset(ChoiceModGameConfig.QUESTION_FONT_SIZE(),
                                        ChoiceModGameConfig.QUESTIONTEXT_COLOR,"times.ttf");
        answerFont=createFontFromAsset(ChoiceModGameConfig.ANSWERFONT_SIZE(),
                                        ChoiceModGameConfig.ANSWERTEXT_COLOR,"times.ttf");
    }

    @Override
    protected void initPictures()
    {
        button=createTextureRegionFromAsset("button.png",2);
        background=createTextureRegionFromAsset("operationgame/background.png",4);
        transparent=createTextureRegionFromAsset("transparent.png",1);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    @Override
    protected void clearResource()
    {

    }

    public static Font getQuestionFont()
    {
        return questionFont;
    }

    public static Font getAnswerFont()
    {
        return answerFont;
    }

    public static TextureRegion getButton()
    {
        return button;
    }

    public static TextureRegion getTransparent()
    {
        return transparent;
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

}
