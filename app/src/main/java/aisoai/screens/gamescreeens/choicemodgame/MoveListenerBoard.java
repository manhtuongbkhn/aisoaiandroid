package aisoai.screens.gamescreeens.choicemodgame;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITMoveEventListener;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class MoveListenerBoard extends TITSprite
{
    private TITMoveEventListener eventListener;

    public MoveListenerBoard(float x, float y, float width, float height, TextureRegion textureRegion
            , TITGameControl inputControl)
    {
        super(x, y, width, height, textureRegion, inputControl);
        eventListener=new TITMoveEventListener()
        {
            @Override
            protected void onMoved(float downX, float downY, float angle)
            {
                onAreaMoved(downX,downY,angle);
            }
        };
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        eventListener.handler(event.getMotionEvent());
        return true;
    }

    protected void onAreaMoved(float downX,float downY,float angle)
    {
        downY=downY-TITGameConfig.HUD_HEIGHT();
        ModQuestion modQuestion=getControl().getScene().getModQuestion();
        float x,y,width,height;
        x=modQuestion.getX();
        y=modQuestion.getY();
        width=modQuestion.getWidth();
        height=modQuestion.getHeight();
        boolean b1=x<=downX&&downX<=x+width;
        boolean b2=y<=downY&&downY<=y+height;
        if(b1&&b2)
            onModQuestionMoved(angle);
    }

    protected void onModQuestionMoved(float angle)
    {
        int direction=0;
        if((0<=angle&&angle<22.5)||(337.5<=angle&&angle<=360))
        {
            direction=1;
        }
        if(22.5<=angle&&angle<67.5)
        {
            direction=2;
        }
        if(67.5<=angle&&angle<112.5)
        {
            direction=3;
        }
        if(112.5<=angle&&angle<157.5)
        {
            direction=4;
        }
        if(157.5<=angle&&angle<202.5)
        {
            direction=5;
        }
        if(202.5<=angle&&angle<247.5)
        {
            direction=6;
        }
        if(247.5<=angle&&angle<292.5)
        {
            direction=7;
        }
        if(292.5<=angle&&angle<337.5)
        {
            direction=8;
        }
        if(direction!=0)
            getControl().modQuestionMovedEvent(direction);
    }

    public static MoveListenerBoard createBoard(TITGameControl iControl)
    {
        TextureRegion textureRegion=(TextureRegion)ChoiceModGameResource.getTransparent();
        float x,y,width,height;
        x=0;
        y=0;
        width= TITGameConfig.GS_WIDTH();
        height=TITGameConfig.GS_HEIGHT();
        MoveListenerBoard moveListenerBoard =new MoveListenerBoard(x,y,width,height,textureRegion,iControl);
        return moveListenerBoard;
    }

    @Override
    public ChoiceModGameControl getControl()
    {
        return (ChoiceModGameControl) control;
    }
}