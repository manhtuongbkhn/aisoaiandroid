package aisoai.screens.gamescreeens.choicemodgame;

import com.google.gson.JsonObject;

import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class ChoiceModGameControl extends TITGameControl
{

    @Override
    protected TextureRegion getBackground()
    {
        return ChoiceModGameResource.getBackground();
    }

    @Override
    protected void initTITScene()
    {
        MoveListenerBoard moveListenerBoard = MoveListenerBoard.createBoard(this);
        getScene().attachChild(moveListenerBoard);
        getScene().registerTouchArea(moveListenerBoard);

        for(int i=1;i<=8;i++)
        {
            AnswerText answerText=AnswerText.createAnswerText(i,this);
            getScene().attachChild(answerText);
        }

        ModQuestion modQuestion=ModQuestion.createModQuestion(this);
        getScene().attachChild(modQuestion);
    }

    @Override
    protected void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    protected void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                getModel().setGameStatus(GameStatus.SLEEPING);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;
            case "createNewQuestion":
                System.out.println("Create New Question");
                int number1=token.get(KJS.PARAM1).getAsInt();
                int number2=token.get(KJS.PARAM2).getAsInt();
                showQuestion(number1,number2);
                getModel().setCurrentQuestion(token);
                gameScriptReader.stop();
                break;
        }
    }

    private void showQuestion(Integer number1,Integer number2)
    {
        String text=number1.toString()+" : "+number2.toString();
        ModQuestion modQuestion=getScene().getModQuestion();
        modQuestion.setText(text);

        float centerX=modQuestion.getWidth()/2;
        float centerY=modQuestion.getHeight()/2;
        ScaleAtModifier scaleAtModifier=new ScaleAtModifier(0.5f,0f,1f,centerX,centerY);
        IModifier.IModifierListener modifierListener=new IModifier.IModifierListener()
        {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem)
            {

            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem)
            {
                getModel().setGameStatus(GameStatus.PLAYING);
            }
        };

        scaleAtModifier.addModifierListener(modifierListener);
        scaleAtModifier.setAutoUnregisterWhenFinished(true);
        modQuestion.registerEntityModifier(scaleAtModifier);
    }

    public void modQuestionMovedEvent(int direction)
    {
        if(getModel().getGameStatus()==GameStatus.PLAYING)
        {
            getModel().setGameStatus(GameStatus.SLEEPING);
            boolean isTrueAnswer = checkAnswer(direction);
            if (isTrueAnswer)
                playTrueSound();
            else
                playTrueSound();
            sendAnswer(direction);
            ModQuestion modQuestion = getScene().getModQuestion();
            moveModQuestion(direction);
        }
    }

    public void moveModQuestion(int direction)
    {
        ModQuestion modQuestion=getScene().getModQuestion();
        float width=modQuestion.getWidth();
        float height=modQuestion.getHeight();
        float gsWidth=TITGameConfig.GS_WIDTH();
        float gsHeight=TITGameConfig.GS_HEIGHT();
        float fromX,fromY,toX,toY;
        fromX=modQuestion.getX();
        fromY=modQuestion.getY();

        switch (direction)
        {
            case 1:
                toX=gsWidth;
                toY=fromY;
                break;
            case 2:
                toX=gsWidth;
                toY=gsHeight;
                break;
            case 3:
                toX=fromX;
                toY=gsHeight;
                break;
            case 4:
                toX=-width;
                toY=gsHeight;
                break;
            case 5:
                toX=-width;
                toY=fromY;
                break;
            case 6:
                toX=-width;
                toY=-height;
                break;
            case 7:
                toX=fromX;
                toY=-height;
                break;
            case 8:
            default:
                toX=gsWidth;
                toY=-height;
                break;
        }
        MoveModifier moveModifier=new MoveModifier(0.5f,fromX,toX,fromY,toY);
        IModifier.IModifierListener modifierListener=new IModifier.IModifierListener()
        {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem)
            {
            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem)
            {
                ModQuestion thisModQuestion=(ModQuestion) pItem;
                thisModQuestion.titReset();
                gameScriptReader.resume();
            }
        };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        modQuestion.registerEntityModifier(moveModifier);
    }

    private boolean checkAnswer(int direction)
    {
        int userMod=direction-1;
        JsonObject currentQuestion=getModel().getCurrentQuestion();
        int number1=currentQuestion.get(KJS.PARAM1).getAsInt();
        int number2=currentQuestion.get(KJS.PARAM2).getAsInt();

        int systemMod=number1%number2;

        return userMod==systemMod;
    }

    private void playTrueSound()
    {

    }

    private void playFalseSound()
    {

    }

    private void sendAnswer(int direction)
    {
        JsonObject currentQuestion=getModel().getCurrentQuestion();
        int index=currentQuestion.get(KJS.INDEX).getAsInt();
        getRequestFactory().sendGameAnswer(index,direction-1);
    }

    @Override
    public void finish()
    {

    }

    @Override
    public ChoiceModGameScene getScene()
    {
        return (ChoiceModGameScene) engine.getScene();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return ChoiceModGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new ChoiceModGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new ChoiceModGameRF();
    }

    @Override
    public ChoiceModGameActivity getActivity()
    {
        return (ChoiceModGameActivity) activity;
    }

    @Override
    public ChoiceModGameModel getModel()
    {
        return (ChoiceModGameModel) model;
    }

    @Override
    public ChoiceModGameRF getRequestFactory()
    {
        return (ChoiceModGameRF) requestFactory;
    }
}
