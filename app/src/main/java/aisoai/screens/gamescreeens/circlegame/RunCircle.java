package aisoai.screens.gamescreeens.circlegame;

import org.andengine.opengl.texture.region.TextureRegion;

import aisoai.screens.gamescreeens.titentities.TITCircleCoordinates;
import aisoai.screens.gamescreeens.titentities.TITCoordinate;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITSprite;

public class RunCircle extends TITSprite
{
    private int index;
    private int status;
    /*
        0:red
        1:blue
        2:green
    */
    private boolean run;;

    public RunCircle(float pX, float pY, float pTileWidth, float pTileHeight,
                                            TextureRegion textureRegion,TITGameControl inputControl)
    {
        super(pX,pY,pTileWidth,pTileHeight,textureRegion,inputControl);
    }

    public static RunCircle createRunCircle(int postion,int status,int index,
                                                                        TITGameControl inputControl)
    {
        float width=2*CircleGameConfig.CIRCLERUN_R();
        float height=width;
        float x,y;
        TextureRegion textureRegion;

        switch (postion)
        {
            case 1:
                x=CircleGameConfig.LEFT_LINE_X()-CircleGameConfig.CIRCLERUN_R();
                y=CircleGameConfig.START1_Y()-CircleGameConfig.CIRCLERUN_R();
                break;
            case 2:
                x=CircleGameConfig.RIGHT_LINE_X()-CircleGameConfig.CIRCLERUN_R();
                y=CircleGameConfig.START1_Y()-CircleGameConfig.CIRCLERUN_R();
                break;
            case 3:
                x=CircleGameConfig.RIGHT_LINE_X()-CircleGameConfig.CIRCLERUN_R();
                y=CircleGameConfig.START2_Y()-CircleGameConfig.CIRCLERUN_R();
                break;
            case 4:
            default:
                x=CircleGameConfig.LEFT_LINE_X()-CircleGameConfig.CIRCLERUN_R();
                y=CircleGameConfig.START2_Y()-CircleGameConfig.CIRCLERUN_R();
                break;
        }

        switch (status)
        {
            case 0:
                textureRegion=CircleGameResource.getRedCircle();
                break;
            case 1:
                textureRegion=CircleGameResource.getBlueCircle();
                break;
            case 2:
            default:
                textureRegion=CircleGameResource.getGreenCircle();
                break;
        }

        RunCircle circle=new RunCircle(x,y,width,height,textureRegion,inputControl);
        circle.setIndex(index);
        circle.setStatus(status);
        circle.setRun(true);
        circle.setZIndex(CircleGameConfig.CIRCLE_RUN_LAYER);
        return circle;
    }

    public TITCircleCoordinates getCircleCoordinates()
    {
        float xPostion=getX();
        float yPostion=getY();
        float width=getWidth();
        float height=getHeight();

        float xCenter=xPostion+width/2;
        float yCenter=yPostion+height/2;
        TITSprite titSprite;

        TITCoordinate center=new TITCoordinate(xCenter,yCenter);

        return new TITCircleCoordinates(center,width/2);
    }


    @Override
    public CircleGameControl getControl()
    {
        return (CircleGameControl) control;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public boolean isRun()
    {
        return run;
    }

    public void setRun(boolean run)
    {
        this.run = run;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
