package aisoai.screens.gamescreeens.circlegame;

import aisoai.screens.gamescreeens.titentities.TITGameModel;

public class CircleGameModel extends TITGameModel
{
    private boolean enbledGreenCircle;

    public CircleGameModel()
    {
        enbledGreenCircle=false;
    }

    public boolean isEnbleGreenCircle()
    {
        return enbledGreenCircle;
    }

    public void setEnbledGreenCircle(boolean enbledGreenCircle)
    {
        this.enbledGreenCircle = enbledGreenCircle;
    }
}
