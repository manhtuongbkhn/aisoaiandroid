package aisoai.screens.gamescreeens.circlegame;

import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameResource;

public class CircleGameResource extends TITGameResource
{

    public CircleGameResource(TITGameActivity titGameActivity)
    {
        super(titGameActivity);
    }

    private static TextureRegion blueCircle;
    private static TextureRegion redCircle;
    private static TextureRegion greenCircle;
    private static TiledTextureRegion centerCircle;
    private static TextureRegion background;

    @Override
    protected void initFonts()
    {

    }

    @Override
    protected void initPictures()
    {
        centerCircle=createTiledTextureRegionFromAsset("circlegame/center-circle.png",3,1,3);
        blueCircle=createTextureRegionFromAsset("circlegame/blue-circle.png",1);
        redCircle=createTextureRegionFromAsset("circlegame/red-circle.png",1);
        greenCircle=createTextureRegionFromAsset("circlegame/green-circle.png",1);
        background =createTextureRegionFromAsset("circlegame/background.png",3);
    }

    @Override
    protected void initSounds()
    {

    }

    @Override
    protected void initMusics()
    {

    }

    public static TextureRegion getBlueCircle()
    {
        return blueCircle;
    }

    public static TextureRegion getRedCircle()
    {
        return redCircle;
    }

    public static TextureRegion getGreenCircle()
    {
        return greenCircle;
    }

    public static TiledTextureRegion getCenterCircle()
    {
        return centerCircle.deepCopy();
    }

    public static TextureRegion getBackground()
    {
        return background;
    }

    public void clearResource()
    {
        blueCircle=null;
        redCircle=null;
        centerCircle=null;
        greenCircle=null;
        background =null;
    }
}
