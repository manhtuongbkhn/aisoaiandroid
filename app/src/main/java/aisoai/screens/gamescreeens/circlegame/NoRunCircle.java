package aisoai.screens.gamescreeens.circlegame;

import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import aisoai.screens.gamescreeens.titentities.TITAnimatedSprite;
import aisoai.screens.gamescreeens.titentities.TITCircleCoordinates;
import aisoai.screens.gamescreeens.titentities.TITCoordinate;
import aisoai.screens.gamescreeens.titentities.TITGameControl;

public class NoRunCircle extends TITAnimatedSprite
{
    /*
        0:red
        1:blue
        2:green
    */

    public NoRunCircle(float x,float y,float width,float height,TiledTextureRegion tiledTextureRegion
                                               ,TITGameControl inputControl)
    {
        super(x,y,width,height,tiledTextureRegion,inputControl);
    }

    public static NoRunCircle createNoRunCircle(int postion,int status,TITGameControl control)
    {
        float width=2*CircleGameConfig.CIRCLENORUN_R();
        float height=width;
        float x,y;

        if(postion==0)
        {
            x=CircleGameConfig.LEFT_LINE_X()-CircleGameConfig.CIRCLENORUN_R();
        }
        else
        {
            x=CircleGameConfig.RIGHT_LINE_X()-CircleGameConfig.CIRCLENORUN_R();
        }
        y=CircleGameConfig.CENTER_Y()-CircleGameConfig.CIRCLENORUN_R();
        NoRunCircle circle=new NoRunCircle(x,y,width,height,CircleGameResource.getCenterCircle(),
                                                                                            control);
        circle.setStatus(status);
        circle.setZIndex(CircleGameConfig.CIRCLE_NO_RUN_LAYER);
        return circle;
    }

    @Override
    public boolean onAreaTouched(TouchEvent event,float x,float y)
    {
        if(event.getAction()== TouchEvent.ACTION_UP)
        {
            getControl().centerCircleTouchEvent(this);
        }
        return true;
    }

    public TITCircleCoordinates getCircleCoordinates()
    {
        float xPostion=getX();
        float yPostion=getY();
        float width=getWidth();
        float height=getHeight();

        float xCenter=xPostion+width/2;
        float yCenter=yPostion+height/2;

        TITCoordinate center=new TITCoordinate(xCenter,yCenter);

        return new TITCircleCoordinates(center,width/2);
    }

    public int getStatus()
    {
        return getCurrentTileIndex();
    }

    public void setStatus(int status)
    {
        setCurrentTileIndex(status);
    }

    @Override
    public CircleGameControl getControl()
    {
        return (CircleGameControl) control;
    }
}
