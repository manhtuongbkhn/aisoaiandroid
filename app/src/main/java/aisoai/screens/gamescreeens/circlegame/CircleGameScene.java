package aisoai.screens.gamescreeens.circlegame;

import org.andengine.entity.IEntity;

import java.util.ArrayList;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class CircleGameScene extends TITScene
{
    private ArrayList<RunCircle> listCircleRun;
    private ArrayList<NoRunCircle> listCircleNoRun;

    public CircleGameScene()
    {
        super();
        listCircleNoRun=new ArrayList<NoRunCircle>();
        listCircleRun=new ArrayList<RunCircle>();
    }

    @Override
    public void attachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();

        if(inputClass.equals(NoRunCircle.class))
        {
            NoRunCircle noRunCircle =(NoRunCircle) iEntity;
            listCircleNoRun.add(noRunCircle);
        }

        if(inputClass.equals(RunCircle.class))
        {
            RunCircle runCircle = (RunCircle) iEntity;
            listCircleRun.add(runCircle);
        }
        super.attachChild(iEntity);
    }

    @Override
    public boolean detachChild(IEntity iEntity)
    {
        Class inputClass=iEntity.getClass();
        if(inputClass.equals(RunCircle.class))
        {
            RunCircle runCircle =(RunCircle) iEntity;
            listCircleRun.remove(runCircle);
        }
        return super.detachChild(iEntity);

    }

    public NoRunCircle getLCircleNoRun()
    {
        return listCircleNoRun.get(0);
    }

    public NoRunCircle getRCircleNoRun()
    {
        return listCircleNoRun.get(1);
    }

    public RunCircle getOldestCircleRun()
    {
        if(listCircleRun.size()>0)
            return listCircleRun.get(0);
        else
            return null;
    }

    protected CircleGameControl getControl()
    {
        return (CircleGameControl) control;
    }
}

