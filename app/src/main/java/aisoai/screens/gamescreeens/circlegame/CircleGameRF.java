package aisoai.screens.gamescreeens.circlegame;

import com.smartfoxserver.v2.entities.data.SFSObject;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;
import aisoai.config.CMDRQ;
import aisoai.config.KJS;
import aisoai.screens.titentities.TITRequestFactory;

public class CircleGameRF extends TITRequestFactory
{
    public void sendGameAnswer(int index,int lStatus,int rStatus)
    {
        Room room=getSFSClient().getLastJoinedRoom();
        SFSObject toServerData=new SFSObject();
        toServerData.putInt(KJS.INDEX,index);
        toServerData.putInt(KJS.PARAM1,lStatus);
        toServerData.putInt(KJS.PARAM2,rStatus);

        ExtensionRequest request=new ExtensionRequest(CMDRQ.GAMEANSWER_RQ,toServerData,room);
        sendRequest(request);
    }
}
