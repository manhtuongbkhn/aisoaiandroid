package aisoai.screens.gamescreeens.circlegame;
import aisoai.screens.gamescreeens.titentities.TITGameConfig;

public class CircleGameConfig
{
    public static float CLEAR_HANDLER_TIME=0.3f;

    public static float LEFT_LINE_X()
    {
        return 80f*TITGameConfig.GS_WIDTH()/320;
    }

    public static float RIGHT_LINE_X()
    {
        return 240*TITGameConfig.GS_WIDTH()/320;
    }

    public static float CENTER_Y()
    {
        return 200*TITGameConfig.GS_HEIGHT()/390;
    }

    public static float CIRCLENORUN_R()
    {
        return 30*TITGameConfig.GS_WIDTH()/320;
    }

    public static float CIRCLERUN_R()
    {
        return 30*TITGameConfig.GS_WIDTH()/320;
    }

    public static float START1_Y()
    {
        return 0;
    }

    public static float START2_Y()
    {
        return TITGameConfig.GS_HEIGHT();
    }

    public final static int CIRCLE_NO_RUN_LAYER=10;
    public final static int CIRCLE_RUN_LAYER=8;

}
