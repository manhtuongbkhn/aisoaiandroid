package aisoai.screens.gamescreeens.circlegame;

import aisoai.TITApplication;
import aisoai.screens.gamescreeens.titentities.TITGameActivity;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.gamescreeens.titentities.TITScene;

public class CircleGameActivity extends TITGameActivity
{
    @Override
    protected TITGameControl linkControl()
    {
        return TITApplication.getScreenControlManager().getCircleGameControl();
    }

    @Override
    public TITGameControl getControl()
    {
        return (CircleGameControl) control;
    }

    @Override
    public CircleGameResource initGameResource()
    {
        return new CircleGameResource(this);
    }

    @Override
    public TITScene initScene()
    {
        return new CircleGameScene();
    }
}
