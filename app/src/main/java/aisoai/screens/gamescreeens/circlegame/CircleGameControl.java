package aisoai.screens.gamescreeens.circlegame;

import com.google.gson.JsonObject;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.modifier.IModifier;

import aisoai.config.KJS;
import aisoai.screens.gamescreeens.titentities.TITGameControl;
import aisoai.screens.gamescreeens.titentities.TITGameModel;
import aisoai.screens.titentities.TITRequestFactory;
import aisoai.screens.titentities.activity.ITITActivity;

public class CircleGameControl extends TITGameControl
{

    @Override
    protected TextureRegion getBackground()
    {
        return CircleGameResource.getBackground();
    }

    @Override
    public void initTITScene()
    {
        NoRunCircle leftCircle=NoRunCircle.createNoRunCircle(0,0,this);
        NoRunCircle rightCircle=NoRunCircle.createNoRunCircle(1,1,this);

        getScene().attachChild(leftCircle);
        getScene().attachChild(rightCircle);
        getScene().registerTouchArea(leftCircle);
        getScene().registerTouchArea(rightCircle);
    }

    @Override
    public void initUpdateHandler()
    {
        gameScriptReader.start();
    }

    @Override
    public void readToken(JsonObject token)
    {
        String methodName=token.get(KJS.METHOD_NAME).getAsString();
        switch (methodName)
        {
            case "init":
                Float gameOverTime=token.get(KJS.PARAM1).getAsFloat();
                getModel().setTime(gameOverTime);
                break;
            case "createNewRunCircle":
                int postion=token.get(KJS.PARAM1).getAsInt();
                int status=token.get(KJS.PARAM2).getAsInt();
                float runTime=token.get(KJS.PARAM3).getAsFloat();
                int index=token.get(KJS.INDEX).getAsInt();
                RunCircle runCircle =RunCircle.createRunCircle(postion,status,index,this);
                setFinishMoveEvent(runCircle,runTime);
                getScene().attachChild(runCircle);
                break;
            case "enbleGreenCircle":
                getModel().setEnbledGreenCircle(true);
                break;
            case "sleep":
                int sleepTime=token.get(KJS.PARAM1).getAsInt();
                try {Thread.sleep(sleepTime);} catch (InterruptedException e) {}
                break;

        }
    }

    public void centerCircleTouchEvent(NoRunCircle circle)
    {
        switch(circle.getStatus())
        {
            case 0:
                circle.setStatus(1);
                break;
            case 1:
                if(getModel().isEnbleGreenCircle())
                    circle.setStatus(2);
                else
                    circle.setStatus(0);
                break;
            case 2:
                circle.setStatus(0);
                break;
        }
    }

    public void setFinishMoveEvent(final RunCircle circle,float runTime)
    {
        float finishY=CircleGameConfig.CENTER_Y()-CircleGameConfig.CIRCLERUN_R();
        MoveYModifier moveModifier=new MoveYModifier(runTime,circle.getY(),finishY);

        IModifier.IModifierListener<IEntity> modifierListener=
                                                        new IModifier.IModifierListener<IEntity>()
        {
            @Override
            public void onModifierStarted(IModifier<IEntity> iModifier,IEntity iEntity)
            {

            }

            @Override
            public void onModifierFinished(IModifier<IEntity> iModifier, IEntity iEntity)
            {
                MoveYModifier thisMoveModifier=(MoveYModifier) iModifier;
                RunCircle thisRunCircle=(RunCircle) iEntity;
                thisRunCircle.setRun(false);
                sendAnswer(thisRunCircle.getIndex());

                getScene().detachChild(thisRunCircle);
            }
        };
        moveModifier.addModifierListener(modifierListener);
        moveModifier.setAutoUnregisterWhenFinished(true);
        circle.registerEntityModifier(moveModifier);
    }

    public void sendAnswer(int index)
    {
        int lStatus=getScene().getLCircleNoRun().getStatus();
        int rStatus=getScene().getRCircleNoRun().getStatus();
        getRequestFactory().sendGameAnswer(index,lStatus,rStatus);
    }

    @Override
    public void finish()
    {

    }

    @Override
    public CircleGameScene getScene()
    {
        return (CircleGameScene) engine.getScene();
    }

    @Override
    public Class<? extends ITITActivity> initActivity()
    {
        return CircleGameActivity.class;
    }

    @Override
    protected TITGameModel initModel()
    {
        return new CircleGameModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new CircleGameRF();
    }

    @Override
    public CircleGameModel getModel()
    {
        return (CircleGameModel) model;
    }

    @Override
    public CircleGameActivity getActivity()
    {
        return (CircleGameActivity) activity;
    }

    @Override
    public CircleGameRF getRequestFactory()
    {
        return (CircleGameRF) requestFactory;
    }
}
