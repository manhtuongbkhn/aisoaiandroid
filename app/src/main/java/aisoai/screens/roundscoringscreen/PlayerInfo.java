package aisoai.screens.roundscoringscreen;

import android.graphics.Bitmap;

import com.google.gson.JsonObject;

import aisoai.config.KJS;
import aisoai.loadnetimage.TITRequestImage;

abstract public class PlayerInfo
{
    private String fullName;
    private String avatarUrl;
    private int trueCount;
    private int falseCount;
    private int point;
    private int covertPoint;
    private int rank;
    private Bitmap avatarBitmap;

    public PlayerInfo(JsonObject jsonObject)
    {
        fullName=jsonObject.get(KJS.FULL_NAME).getAsString();
        avatarUrl=jsonObject.get(KJS.AVATAR_URL).getAsString();
        trueCount=jsonObject.get(KJS.TRUE_COUNT).getAsInt();
        falseCount=jsonObject.get(KJS.FALSE_COUNT).getAsInt();
        point=jsonObject.get(KJS.POINT).getAsInt();
        covertPoint=jsonObject.get(KJS.CONVERT_POINT).getAsInt();
        rank=jsonObject.get(KJS.RANK).getAsInt();
        new TITRequestImage(avatarUrl,6)
        {
            @Override
            protected void setImage(Bitmap bitmap)
            {
                avatarBitmap=bitmap;
                reloadImage(bitmap);
            }
        };
    }

    public String getFullName()
    {
        return fullName;
    }

    public String getAvatarUrl()
    {
        return avatarUrl;
    }

    public int getTrueCount()
    {
        return trueCount;
    }

    public int getFalseCount()
    {
        return falseCount;
    }

    public int getPoint()
    {
        return point;
    }

    public int getCovertPoint()
    {
        return covertPoint;
    }

    public int getRank()
    {
        return rank;
    }

    public Bitmap getAvatarBitmap()
    {
        return avatarBitmap;
    }

    abstract protected void reloadImage(Bitmap bitmap);
}
