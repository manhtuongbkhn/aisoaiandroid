package aisoai.screens.roundscoringscreen;

import java.util.ArrayList;

import aisoai.screens.titentities.model.TITModel;

public class RoundScoringModel extends TITModel
{
    private ArrayList<PlayerInfo> playerArr=new ArrayList<PlayerInfo>();

    public ArrayList<PlayerInfo> getPlayerArr()
    {
        return playerArr;
    }

    public void setPlayerArr(ArrayList<PlayerInfo> playerArr)
    {
        this.playerArr = playerArr;
    }
}
