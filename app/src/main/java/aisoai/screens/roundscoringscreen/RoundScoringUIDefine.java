package aisoai.screens.roundscoringscreen;

import aisoai.config.ClientConfig;

public class RoundScoringUIDefine
{
    public static int GAMEICONIV_WIDTH()
    {
        Float f=50f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int GAMEICONIV_HEIGHT()
    {
        return GAMEICONIV_WIDTH();
    }

    public static int PLAYERITEM_WIDTH()
    {
        Float f=ClientConfig.SCREEN_WIDTH_PX;
        return f.intValue();
    }

    public static int PLAYERITEM_HEIGHT()
    {
        Float f=80f*ClientConfig.SCREEN_HEIGHT_PX/480f;
        return f.intValue();
    }

    public static int PLAYERAVATARIV_WIDTH()
    {
        Float f=50f*ClientConfig.SCREEN_WIDTH_PX/320f;
        return f.intValue();
    }

    public static int PLAYERAVATARIV_HEIGHT()
    {
        return PLAYERAVATARIV_WIDTH();
    }
}
