package aisoai.screens.roundscoringscreen;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import aisoai.TITApplication;
import aisoai.config.UIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;
import aisoai.R;
import aisoai.screens.titentities.control.TITNormalControl;

public class RoundScoringActivity extends TITNormalActivity
{
    private TextView tvRound;
    private ImageView ivGameIcon;
    private TextView tvGameName;
    private TextView tvGameHardLevel;
    private ListView lvPlayer;

    @Override
    protected void linkToLayout()
    {
        setContentView(R.layout.round_scoring);

        tvRound=(TextView) findViewById(R.id.tvRound);
        ivGameIcon=(ImageView) findViewById(R.id.ivGameIcon);
        tvGameName=(TextView) findViewById(R.id.tvGameName);
        tvGameHardLevel=(TextView) findViewById(R.id.tvGameHardLevel);
        lvPlayer=(ListView) findViewById(R.id.lvPlayer);
    }

    @Override
    protected void scaleView()
    {
        ivGameIcon.requestLayout();
        ivGameIcon.getLayoutParams().width= RoundScoringUIDefine.GAMEICONIV_WIDTH();
        ivGameIcon.getLayoutParams().height= RoundScoringUIDefine.GAMEICONIV_HEIGHT();

        tvRound.setTextSize(UIDefine.BIGTEXT_SIZE());
        tvGameName.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvGameHardLevel.setTextSize(UIDefine.SMALLTEXT_SIZE());
    }

    @Override
    protected TITNormalControl linkControl()
    {
        return TITApplication.getScreenControlManager().getRoundScoringControl();
    }

    public TextView getTvRound()
    {
        return tvRound;
    }

    public TextView getTvGameName()
    {
        return tvGameName;
    }

    public ImageView getIvGameIcon()
    {
        return ivGameIcon;
    }

    public TextView getTvGameHardLevel()
    {
        return tvGameHardLevel;
    }

    public ListView getLvPlayer()
    {
        return lvPlayer;
    }

    @Override
    public RoundScoringControl getControl()
    {
        return (RoundScoringControl) control;
    }

}
