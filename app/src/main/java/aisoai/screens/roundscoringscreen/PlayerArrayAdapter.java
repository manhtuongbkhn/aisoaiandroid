package aisoai.screens.roundscoringscreen;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.andengine.entity.text.Text;

import java.util.ArrayList;

import aisoai.config.UIDefine;
import aisoai.screens.titentities.activity.TITNormalActivity;
import aisoai.R;

public class PlayerArrayAdapter extends ArrayAdapter<PlayerInfo>
{
    private TITNormalActivity titActivity;
    private ArrayList<PlayerInfo> playerArr;

    private View item;
    private ImageView ivPlayerAvatar;
    private TextView tvPlayerName;
    private TextView tvTrueCount;
    private TextView tvFalseCount;
    private TextView tvPlayerPoint;
    private TextView tvPlayerConvertPoint;
    private TextView tvPlayerRank;

    public PlayerArrayAdapter(TITNormalActivity titActivity,ArrayList<PlayerInfo> playerArr)
    {
        super(titActivity, R.layout.roundscoring_player_item,playerArr);
        this.titActivity=titActivity;
        this.playerArr=playerArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater=titActivity.getLayoutInflater();
        item=inflater.inflate(R.layout.roundscoring_player_item,parent,false);
        linkToLayout();
        scaleView();
        setContentView(position);
        return item;
    }

    public void linkToLayout()
    {
        ivPlayerAvatar=(ImageView) item.findViewById(R.id.tvPlayerAvatar);
        tvPlayerName=(TextView) item.findViewById(R.id.tvPlayerName);
        tvTrueCount=(TextView) item.findViewById(R.id.tvTrueCount);
        tvFalseCount=(TextView) item.findViewById(R.id.tvFalseCount);
        tvPlayerPoint=(TextView) item.findViewById(R.id.tvPlayerPoint);
        tvPlayerConvertPoint=(TextView) item.findViewById(R.id.tvPlayerConvertPoint);
        tvPlayerRank=(TextView) item.findViewById(R.id.tvPlayerRank);

        tvPlayerName.setTextSize(UIDefine.TINYTEXT_SIZE());
        tvFalseCount.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvTrueCount.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvPlayerPoint.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvPlayerConvertPoint.setTextSize(UIDefine.SMALLTEXT_SIZE());
        tvPlayerRank.setTextSize(UIDefine.SMALLTEXT_SIZE());
    }

    public void scaleView()
    {
        item.requestLayout();
        item.getLayoutParams().width= RoundScoringUIDefine.PLAYERITEM_WIDTH();
        item.getLayoutParams().height= RoundScoringUIDefine.PLAYERITEM_HEIGHT();

        ivPlayerAvatar.requestLayout();
        ivPlayerAvatar.getLayoutParams().width= RoundScoringUIDefine.PLAYERAVATARIV_WIDTH();
        ivPlayerAvatar.getLayoutParams().height= RoundScoringUIDefine.PLAYERAVATARIV_HEIGHT();
    }

    public void setContentView(int postion)
    {
        PlayerInfo playerInfo=playerArr.get(postion);
        Bitmap avatarBitmap=playerInfo.getAvatarBitmap();
        String playerName=playerInfo.getFullName();
        Integer trueCount=playerInfo.getTrueCount();
        Integer falseCount=playerInfo.getFalseCount();
        Integer point=playerInfo.getPoint();
        Integer convertPoint=playerInfo.getCovertPoint();
        Integer rank=playerInfo.getRank();

        ivPlayerAvatar.setImageBitmap(avatarBitmap);
        tvPlayerName.setText(playerName);
        tvTrueCount.setText(trueCount.toString());
        tvFalseCount.setText(falseCount.toString());
        tvPlayerPoint.setText(point.toString());
        tvPlayerConvertPoint.setText(convertPoint.toString());
        tvPlayerRank.setText(rank.toString());
    }
}
