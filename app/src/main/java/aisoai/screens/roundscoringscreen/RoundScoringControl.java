package aisoai.screens.roundscoringscreen;

import android.graphics.Bitmap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import aisoai.TITApplication;
import aisoai.config.KJS;
import aisoai.screens.choicegamescreen.ChoiceGameControl;
import aisoai.screens.gamescreeens.TITGameRouter;
import aisoai.screens.totalscoringscreen.TotalScoringControl;
import aisoai.loadnetimage.TITRequestImage;
import aisoai.screens.titentities.control.TITNormalControl;
import aisoai.screens.titentities.model.TITModel;
import aisoai.screens.titentities.TITRequestFactory;

public class RoundScoringControl extends TITNormalControl
{

    @Override
    public void init()
    {
        PlayerArrayAdapter arrayAdapter=new PlayerArrayAdapter
                                                        (getActivity(),getModel().getPlayerArr());
        getActivity().getLvPlayer().setAdapter(arrayAdapter);
    }

    @Override
    public void finish()
    {

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void roundScoringInfoNotify(JsonObject fromServerData)
    {
        Integer round=fromServerData.get(KJS.ROOM_ROUND).getAsInt();
        String gameName=fromServerData.get(KJS.GAME_NAME).getAsString();
        Integer gameId=fromServerData.get(KJS.GAME_ID).getAsInt();
        Integer hardLevel=fromServerData.get(KJS.HARD_LEVEL).getAsInt();

        getActivity().getTvRound().setText("Round "+round.toString());
        getActivity().getIvGameIcon().setImageResource(TITGameRouter.routerGameIcon(gameId));
        getActivity().getTvGameName().setText(gameName);
        switch (hardLevel)
        {
            case 1:
                getActivity().getTvGameHardLevel().setText("Mức:Dễ");
                break;
        }
    }


    public void roundScoringUserInfoNotify(JsonObject fromServerData)
    {
        JsonArray userInfoJsonArr=fromServerData.get(KJS.ARRAY).getAsJsonArray();
        for(int i=0;i<userInfoJsonArr.size();i++)
        {
            JsonObject jsonObject=userInfoJsonArr.get(i).getAsJsonObject();
            PlayerInfo playerInfo=new PlayerInfo(jsonObject)
            {
                @Override
                protected void reloadImage(Bitmap bitmap)
                {
                    getActivity().getLvPlayer().invalidateViews();
                }
            };
            getModel().getPlayerArr().add(playerInfo);
        }
        getActivity().getLvPlayer().invalidateViews();
    }

    public void roundScoringDownTimeNotify(JsonObject fromServerData)
    {

    }

    public void startTotalScoringNotify(JsonObject fromServerData)
    {
        TITApplication.getScreenControlManager().changeScreen(new TotalScoringControl());
    }

    public void startChoiceGameNotify(JsonObject fromServerData)
    {
        TITApplication.getScreenControlManager().changeScreen(new ChoiceGameControl());
    }

    @Override
    public Class<RoundScoringActivity> initActivity()
    {
        return RoundScoringActivity.class;
    }

    @Override
    protected TITModel initModel()
    {
        return new RoundScoringModel();
    }

    @Override
    protected TITRequestFactory initRequestFactory()
    {
        return new RoundScoringRF();
    }

    @Override
    public RoundScoringActivity getActivity()
    {
        return (RoundScoringActivity) activity;
    }

    @Override
    public RoundScoringModel getModel()
    {
        return (RoundScoringModel) model;
    }

    @Override
    public RoundScoringRF getRequestFactory()
    {
        return (RoundScoringRF) requestFactory;
    }
}
