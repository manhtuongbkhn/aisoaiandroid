package aisoai.config;

public class CMDNF
{
    public final static String USERJOINZONE_NF="user_join_zone_nf";
    public final static String USERJOINROOM_NF="user_join_room_nf";
    public final static String USEREXITROOM_NF="user_exit_room_nf";
    public final static String CHALLENGE_NF="challenge_nf";
    public static final String RELOADTOPPLAYER_NF = "reload_top_player_nf";

    public final static String RELOADFRIENDLIST_NF="reload_friend_list_nf";
    public final static String RELOADROOMLIST_NF="reload_room_list_nf";
    public final static String ROOMINFO_NF="room_info_nf";
    public final static String ALLPLAYERINFO_NF="all_player_info_nf";
    public final static String USERWASKICKED_NF="user_was_kicked_user_nf";
    public final static String NEWMESSAGE_NF="new_message_nf";
    public final static String RELOADINVITATIONFRIENDLIST_NF="reload_invitation_friend_list_nf";
    public final static String WASINVITED_NF="was_invited_nf";
    public final static String BEGINGAMEDOWNTIME_NF="begin_game_down_time_nf";
    public final static String READYSTARTGAME_NF="ready_start_game_nf";
    public final static String STARTGAMEDOWNTIME_NF="start_game_down_time_nf";

    public final static String STARTCHOICEGAME_NF="start_choice_game_nf";
    public final static String CHOICEGAMEINFO_NF="choice_game_info_nf";
    public final static String CHOICEGAMEUSERINFO_NF="choice_game_user_info_nf";
    public final static String CHOICEGAMEDOWNTIME_NF="choice_game_down_time_nf";
    public final static String CHOICEDGAMEINFO_NF="choiced_game_info_nf";

    public final static String STARTGUIDEGAME_NF="start_guide_game_nf";
    public final static String GAMEGUIDEINFO_NF="game_guide_nf";
    public final static String GUIDEGAMEUSERINFO_NF="guide_game_user_info_nf";
    public final static String GAMESCRIPT_NF="game_script_nf";
    public final static String GUIDEGAMEDOWNTIME_NF="guide_game_down_time_nf";

    public final static String STARTPLAYINGGAME_NF="start_playing_game_nf";
    public final static String PLAYINGGAMEUSERINFO_NF="playing_game_user_info_nf";
    public final static String PLAYINGGAMEDOWNTIME_NF="playing_game_down_time_nf";
    public final static String RELOADPLAYINGPOINT_NF="reload_playing_point_nf";

    public final static String STARTROUNDSCORING_NF="start_round_scoring_nf";
    public final static String ROUNDSCORINGINFO_NF="round_scoring_info_nf";
    public final static String ROUNDSCORINGUSERINFO_NF="round_scoring_user_info_nf";
    public final static String ROUNDSCORINGDOWNTIME_NF="round_scoring_down_time_nf";

    public final static String STARTTOTALSCORING_NF="start_total_scoring_nf";
    public final static String TOTALSCORINGUSERINFO_NF="total_scoring_user_info_nf";
    public final static String TOTALSCORINGDOWNTIME_NF="total_scoring_down_time_nf";
    public final static String WASKICKROOMBYSYSTEM_NF="was_kickroom_by_system_nf";
}
