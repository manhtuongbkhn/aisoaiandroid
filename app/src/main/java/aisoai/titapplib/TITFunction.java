package aisoai.titapplib;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;


import java.util.ArrayList;
import java.util.Random;

import aisoai.config.KJS;

public class TITFunction
{
    public static JsonObject checkMessage(String message)
    {
        JsonObject jsonObject=new JsonObject();

        if(message==null)
            jsonObject.addProperty(KJS.SUCESS,false);
        else
        {
            jsonObject.addProperty(KJS.SUCESS,true);

        }

        return jsonObject;
    }

    public static int randInt(int max)
    {
        Random random=new Random();
        int d=random.nextInt(max);
        return d+1;
    }

    public static int covertTime(Float fTime)
    {
        int iTime=fTime.intValue();
        if(fTime-iTime>=0.00001)
        {
            iTime=iTime+1;
        }
        return iTime;
    }

    public static JsonObject covertToJsonObject(SFSObject sfsObject)
    {
        Gson gson=new Gson();
        String jsonStr=sfsObject.toJson();
        JsonObject jsonObject=gson.fromJson(jsonStr, JsonObject.class);
        return jsonObject;
    }

    public static SFSObject covertToSFSObject(JsonObject jsonObject)
    {
        String jsonStr=jsonObject.toString();
        SFSObject sfsObject=(SFSObject)SFSObject.newFromJsonData(jsonStr);
        return sfsObject;
    }

    public static JsonArray covertToJsonArray(SFSArray sfsArray)
    {
        Gson gson=new Gson();
        String jsonStr=sfsArray.toJson();
        JsonArray jsonArray=gson.fromJson(jsonStr, JsonArray.class);
        return jsonArray;
    }

    public static SFSArray covertToSFSArray(JsonArray jsonArray)
    {
        String jsonStr=jsonArray.toString();
        SFSArray sfsArray=SFSArray.newFromJsonData(jsonStr);
        return sfsArray;
    }

    public static ArrayList<Integer> covertToIntArrayList(JsonArray jsonArray)
    {
        ArrayList<Integer> arrayList=new ArrayList<Integer>();
        for(int i=0;i<jsonArray.size();i++)
        {
            int number=jsonArray.get(i).getAsInt();
            arrayList.add(number);
        }
        return arrayList;
    }

    public static JsonArray covertToIntJsonArray(ArrayList<Integer> arrayList)
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<arrayList.size();i++)
        {
            int number=arrayList.get(i);
            jsonArray.add(new JsonPrimitive(number));
        }
        return jsonArray;
    }

    public static SFSArray covertToIntSFSArray(ArrayList<Integer> arrayList)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<arrayList.size();i++)
        {
            int number=arrayList.get(i);
            sfsArray.addInt(number);
        }
        return sfsArray;
    }

    public static ArrayList<Float> covertToFloatArrayList(JsonArray jsonArray)
    {
        ArrayList<Float> arrayList=new ArrayList<Float>();
        for(int i=0;i<jsonArray.size();i++)
        {
            Float f=jsonArray.get(i).getAsFloat();
            arrayList.add(f);
        }
        return arrayList;
    }

    public static JsonArray covertToFloatJsonArray(ArrayList<Float> arrayList)
    {
        JsonArray jsonArray=new JsonArray();
        for(int i=0;i<arrayList.size();i++)
        {
            Float f=arrayList.get(i);
            jsonArray.add(new JsonPrimitive(f));
        }
        return jsonArray;
    }

    public static SFSArray covertToFloatSFSArray(JsonArray jsonArray)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<jsonArray.size();i++)
        {
            Float f=jsonArray.get(i).getAsFloat();
            sfsArray.addFloat(f);
        }
        return sfsArray;
    }

    public static SFSArray covertToFloatSFSArray(ArrayList<Float> arrayList)
    {
        SFSArray sfsArray=new SFSArray();
        for(int i=0;i<arrayList.size();i++)
        {
            Float f=arrayList.get(i);
            sfsArray.addFloat(f);
        }
        return sfsArray;
    }

    public static int stringWidth(String str,Paint paint)
    {
        Rect rect=new Rect();
        paint.getTextBounds(str,0,str.length(),rect);
        return rect.width();
    }

    public static int stringHeight(String str,Paint paint)
    {
        Rect rect=new Rect();
        paint.getTextBounds(str,0,str.length(),rect);
        return rect.height();
    }

    public static Bitmap overlay(Bitmap bitmap1, Bitmap bitmap2)
    {
        Bitmap bmOverlay = Bitmap.createBitmap(bitmap1.getWidth(), bitmap1.getHeight(),bitmap2.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bitmap1,new Matrix(),null);
        canvas.drawBitmap(bitmap2, 0, 0, null);
        return bmOverlay;
    }

    public static Bitmap getRoundedBitmap(Bitmap bitmap)
    {
        int width=bitmap.getWidth();
        int height=bitmap.getHeight();
        final Bitmap output = Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = org.andengine.util.color.Color.ARGB_PACKED_BLUE_CLEAR;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0,0,width,height);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        paint.setStrokeWidth(5);
        canvas.drawOval(rectF,paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        bitmap.recycle();

        return output;
    }

    public static Bitmap createCompetenceBitmap(ArrayList<Integer> competenceArr,
                                        Integer width,Integer height,float radiusScale,int textSize)
    {
        Bitmap bufferBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas bitmapCanvas = new Canvas(bufferBitmap);
        float speed = competenceArr.get(0).floatValue()/100f;
        float observation = competenceArr.get(1).floatValue()/100f;
        float calculate = competenceArr.get(2).floatValue()/100f;
        float memory = competenceArr.get(3).floatValue()/100f;
        float logic = competenceArr.get(4).floatValue()/100f;
        float english = competenceArr.get(5).floatValue()/100f;

        float radius = height.floatValue() * radiusScale;
        float cos30 = 0.866f;
        float cos60 = 0.5f;

        float X1 = width / 2;
        float Y1 = height / 2 - radius;

        float X2 = width / 2 + cos30 * radius;
        float Y2 = height / 2 - cos60 * radius;

        float X3 = width / 2 + cos30 * radius;
        float Y3 = height / 2 + cos60 * radius;

        float X4 = width / 2;
        float Y4 = height / 2 + radius;

        float X5 = width / 2 - cos30 * radius;
        float Y5 = height / 2 + cos60 * radius;

        float X6 = width / 2 - cos30 * radius;
        float Y6 = height / 2 - cos60 * radius;

        float x1 = width / 2;
        float y1 = height / 2 - speed * radius;

        float x2 = width / 2 + cos30 * observation * radius;
        float y2 = height / 2 - cos60 * observation * radius;

        float x3 = width / 2 + cos30 * calculate * radius;
        float y3 = height / 2 + cos60 * calculate * radius;

        float x4 = width / 2;
        float y4 = height / 2 + memory * radius;

        float x5 = width / 2 - cos30 * logic * radius;
        float y5 = height / 2 + cos60 * logic * radius;

        float x6 = width / 2 - cos30 * english * radius;
        float y6 = height / 2 - cos60 * english * radius;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(textSize);
        paint.setColor(Color.GRAY);
        bitmapCanvas.drawARGB(255, 230, 230, 230);
        paint.setColor(Color.GREEN);
        bitmapCanvas.drawCircle(width / 2, height / 2, radius, paint);

        paint.setColor(Color.DKGRAY);
        bitmapCanvas.drawLine(width / 2, height / 2, X1, Y1, paint);
        bitmapCanvas.drawLine(width / 2, height / 2, X2, Y2, paint);
        bitmapCanvas.drawLine(width / 2, height / 2, X3, Y3, paint);
        bitmapCanvas.drawLine(width / 2, height / 2, X4, Y4, paint);
        bitmapCanvas.drawLine(width / 2, height / 2, X5, Y5, paint);
        bitmapCanvas.drawLine(width / 2, height / 2, X6, Y6, paint);
        paint.setColor(Color.BLUE);
        int strWidth,strHeight;
        strWidth = TITFunction.stringWidth("Tốc độ", paint);
        strHeight = TITFunction.stringHeight("Tốc độ", paint);
        bitmapCanvas.drawText("Tốc độ", X1 - strWidth / 2, Y1, paint);

        strWidth = TITFunction.stringWidth("Quan sát", paint);
        strHeight = TITFunction.stringHeight("Quan sát", paint);
        bitmapCanvas.drawText("Quan sát", X2, Y2, paint);

        strWidth = TITFunction.stringWidth("Tính toán", paint);
        strHeight = TITFunction.stringHeight("Tính toán", paint);
        bitmapCanvas.drawText("Tính toán", X3, Y3 + strHeight, paint);

        strWidth = TITFunction.stringWidth("Ghi nhớ", paint);
        strHeight = TITFunction.stringHeight("Ghi nhớ", paint);
        bitmapCanvas.drawText("Ghi nhớ", X4 - strWidth / 2, Y4 + strHeight, paint);

        strWidth = TITFunction.stringWidth("Logic", paint);
        strHeight = TITFunction.stringHeight("Logic", paint);
        bitmapCanvas.drawText("Logic", X5 - strWidth, Y5 + strHeight, paint);

        strWidth = TITFunction.stringWidth("Phản xạ", paint);
        strHeight = TITFunction.stringHeight("English", paint);
        bitmapCanvas.drawText("Phản xạ", X6 - strWidth, Y6, paint);

        paint.setColor(Color.BLUE);
        bitmapCanvas.drawLine(x1, y1, x2, y2, paint);
        bitmapCanvas.drawLine(x2, y2, x3, y3, paint);
        bitmapCanvas.drawLine(x3, y3, x4, y4, paint);
        bitmapCanvas.drawLine(x4, y4, x5, y5, paint);
        bitmapCanvas.drawLine(x5, y5, x6, y6, paint);
        bitmapCanvas.drawLine(x6, y6, x1, y1, paint);
        return bufferBitmap;
    }

    public static int convertMinute(int time)
    {
        int minute=time/60;
        return minute;
    }

    public static int convertSecond(int time)
    {
        int minute=time/60;
        return time-minute*60;
    }

    public static Uri resourceToUri (Context context,int resID)
    {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                context.getResources().getResourcePackageName(resID) + '/' +
                context.getResources().getResourceTypeName(resID) + '/' +
                context.getResources().getResourceEntryName(resID) );
    }
}
